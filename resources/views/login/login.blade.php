@extends('common.login_layout')
@section('title', 'Login | SignUp')
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<link rel="stylesheet" href="{!! asset('theme_includes/css/allcss/allcss.css') !!}">
<script src="{!! asset('js/login.js') !!}" type="text/javascript"></script>
<link rel="stylesheet" href="{!! asset('theme_includes/css/login/login.css') !!}">
<script>
	
</script>
<script>
	// for active label
	

	var Password_required = "{{ __('messages.Password_required') }}";
	var Password_valid = "{{ __('messages.Password_valid') }}";
	var Username_required = "{{ __('messages.Username_required') }}";
	var Username_contain = "{{ __('messages.Username_contain') }}";
	var Error = "{{ __('messages.Error') }}";
	var pls_chk_api_url = "{{ __('messages.pls_chk_api_url') }}";
	var Invalid_Credentials = "{{ __('messages.Invalid_Credentials') }}";
	var Error = "{{ __('messages.Error') }}";
	var Something_Went_Wrong_Exception_Occured = "{{ __('messages.Something_Went_Wrong_Exception_Occured') }}";
	var Ok = "{{ __('messages.Ok') }}";
	var Success = "{{ __('messages.Success') }}";
    //const togglePassword = document.querySelector('#togglePassword');
    const togglePassword = document.getElementById('togglePassword');
    const password = document.getElementById('UserForm-pass');
    function pageRedirect() {
        // window.navigate("htmlcode.html");
    }     
</script>

    <style>
     
        
    </style>
    <body class="bg-theme bg-theme1">

<!-- start loader -->
   <div id="pageloader-overlay" class="visible incoming"><div class="loader-wrapper-outer"><div class="loader-wrapper-inner" ><div class="loader"></div></div></div></div>
   <!-- end loader -->

<!-- Start wrapper-->
 <div id="wrapper">

 <div class="loader-wrapper"><div class="lds-ring"></div></div>
 <div class="height-100v d-flex align-items-center justify-content-center">
	<div class="card background_color_white box_shadow card_marg card-authentication1 my-5">
		<div class="card-body">
		 <div class="card-content p-2">
		 	<div class="text-center">
		 		<img src="{!! asset('theme_includes/images/logo-icon.png') !!}" src="assets/images/logo-icon.png" alt="logo icon">
		 	</div>
		  <div class="card-title text-uppercase text-center py-3">{{ __('messages.Login') }}</div>
		    <form action="login" method="POST">
			  <div class="form-group">
			  
			   <div class="position-relative has-icon-right">
				  <input type="text" id="userName" name="username" class="form-control input-shadow">
				  <div class="form-control-position">
					  <i class="icon-user"></i>
				  </div>
				  <label for="username" class="form-label" style="padding-left:2px;">{{ __('messages.User_Name') }}</label>
			   </div>
				<span id="username-validation" class="validation"></span>
			  </div>
             
			  <div class="form-group">
			  
			   <div class="position-relative has-icon-right">
				  <input type="password" id="password" name="password" class="form-control input-shadow" >
				  <div class="form-control-position">
					  <i class="icon-lock"></i>
				  </div>
				  <label for="exampleInputPassword" class="form-label">{{ __('messages.Your_Password') }}</label>
			   </div>
         <span id="password-validation" class="validation"></span>
			  </div>
              
			<div class="form-row">
			 <div class="form-group col-6">
			   <div class="icheck-material-white">
                <!-- <input type="checkbox"  id="remember_me"  checked="" /> -->
                <input type="checkbox" id="user-checkbox" checked="" />
                <label for="user-checkbox">{{ __('messages.Remember_Me') }}</label>
			  </div>
			 </div>
			 <div class="form-group col-6 text-right" style="margin-top:1.5%">
			  <a href="{{url('/forgetPassword') }}">Forget Password</a>
			 </div>
			</div>
			 <button type="button" id="btnLogin" class="btn btn-light btn-block btn_login">Sign In</button>
			 
		
			 
			 </form>
		   </div>
		  </div>
		  <div class="card-footer text-center py-3">
		    <p class="text-warning mb-0">Do not have an account? <a  href="{{url('/signup') }}"> Sign Up here</a></p>
		  </div>
	     </div>
 </div>
    
     <!--Start Back To Top Button-->
    <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
    <!--End Back To Top Button-->
	
	
	
	</div><!--wrapper-->
  

 
    </body>
