<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8"/>
  <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
  <meta name="description" content=""/>
  <meta name="author" content=""/>
  <title>Framework_UI</title>
  <!-- datatable css cdn -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/dataTables.bootstrap4.min.css">

  <!-- loader-->
  <link rel="stylesheet" href="{!! asset('theme_includes/css/pace.min.css') !!}">
  
  <!-- all css -->
  <link rel="stylesheet" href="{!! asset('theme_includes/css/allcss/allcss.css') !!}">
  <!--favicon-->
  <link rel="icon" href="assets/images/favicon.ico" type="image/x-icon">
  <!-- Vector CSS -->
  <!-- <link rel="stylesheet" href="{!! asset('theme_includes/plugins/vectormap/jquery-jvectormap-2.0.2.css') !!}"> -->
 
  <!-- simplebar CSS-->
  <link rel="stylesheet" href="{!! asset('theme_includes/plugins/simplebar/css/simplebar.css') !!}">
  
  <!-- Bootstrap core CSS-->
  <link rel="stylesheet" href="{!! asset('theme_includes/css/bootstrap.min.css') !!}">
 
  <!-- animate CSS-->
  <link rel="stylesheet" href="{!! asset('theme_includes/css/animate.css') !!}">
  
  <!-- Icons CSS-->
  <link rel="stylesheet" href="{!! asset('theme_includes/css/icons.css') !!}">
  
  <!-- Sidebar CSS-->
  <link rel="stylesheet" href="{!! asset('theme_includes/css/sidebar-menu.css') !!}">
 
  <!-- Custom Style-->
  <link rel="stylesheet" href="{!! asset('theme_includes/css/app-style.css') !!}">

 <!-- REQUIRED SCRIPTS -->

  <!-- Bootstrap core JavaScript-->
  <script src="{!! asset('theme_includes/js/jquery.min.js') !!}" type="text/javascript"></script>
  <!-- <script type="text/javascript">
  var jQuery_2_2_4 = $.noConflict(true);
  </script> -->
  <!-- datatable js cdn -->
  <script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js" type="text/javascript"></script>
  <script src="https://cdn.datatables.net/1.10.24/js/dataTables.bootstrap4.min.js" type="text/javascript"></script>

  <script src="{!! asset('theme_includes/js/popper.min.js') !!}" type="text/javascript"></script>
  <script src="{!! asset('theme_includes/js/bootstrap.min.js') !!}" type="text/javascript"></script>
  
	
 <!-- simplebar js -->
 <script src="{!! asset('theme_includes/plugins/simplebar/js/simplebar.js') !!}" type="text/javascript"></script>
  
  <!-- sidebar-menu js -->
  <script src="{!! asset('theme_includes/js/sidebar-menu.js') !!}" type="text/javascript"></script>
  
  <!-- loader scripts -->
  <!-- <script src="{!! asset('theme_includes/js/jquery.loading-indicator.js') !!}" type="text/javascript"></script> -->
  
  <!-- Custom scripts -->
  <script src="{!! asset('theme_includes/js/app-script.js') !!}" type="text/javascript"></script>
  
  <!-- Chart js -->
  <script src="{!! asset('theme_includes/plugins/Chart.js/Chart.min.js') !!}" type="text/javascript"></script>
  
  <!-- Index js -->
  <script src="{!! asset('theme_includes/js/index.js') !!}" type="text/javascript"></script>
  
  <!-- loader-->
  <script src="{!! asset('theme_includes/js/pace.min.js') !!}" type="text/javascript"></script>
  
</head>
<body>
<!--Start topbar header-->
<header class="topbar-nav">
 <nav class="navbar navbar-expand fixed-top">
  <ul class="navbar-nav mr-auto align-items-center">
    <li class="nav-item">
      <a class="nav-link toggle-menu" href="javascript:void();">
       <i class="icon-menu menu-icon"></i>
     </a>
    </li>
    <li class="nav-item">
      <form class="search-bar">
        <input type="text" class="form-control" placeholder="Enter keywords">
         <a href="javascript:void();"><i class="icon-magnifier"></i></a>
      </form>
    </li>
  </ul>
     
  <ul class="navbar-nav align-items-center right-nav-link">
    <!-- <li class="nav-item dropdown-lg">
      <a class="nav-link dropdown-toggle dropdown-toggle-nocaret waves-effect" data-toggle="dropdown" href="javascript:void();">
      <i class="fa fa-envelope-open-o"></i></a>
    </li>
    <li class="nav-item dropdown-lg">
      <a class="nav-link dropdown-toggle dropdown-toggle-nocaret waves-effect" data-toggle="dropdown" href="javascript:void();">
      <i class="fa fa-bell-o"></i></a>
    </li> -->
    <li class="nav-item language">
      <a class="nav-link dropdown-toggle dropdown-toggle-nocaret waves-effect" data-toggle="dropdown" href="javascript:void();"><i class="fa fa-flag"></i></a>
      <ul class="dropdown-menu dropdown-menu-right">
          <li class="dropdown-item"> <i class="flag-icon flag-icon-gb mr-2"></i> English</li>
          <li class="dropdown-item"> <i class="flag-icon flag-icon-fr mr-2"></i> French</li>
          <li class="dropdown-item"> <i class="flag-icon flag-icon-cn mr-2"></i> Chinese</li>
          <li class="dropdown-item"> <i class="flag-icon flag-icon-de mr-2"></i> German</li>
        </ul>
    </li>
    <li class="nav-item">
      <a class="nav-link dropdown-toggle dropdown-toggle-nocaret" data-toggle="dropdown" href="#">
        <span class="user-profile"><img src="https://via.placeholder.com/110x110" class="img-circle" alt="user avatar"></span>
      </a>
      <ul class="dropdown-menu dropdown-menu-right">
       <li class="dropdown-item user-details">
        <a href="javaScript:void();">
           <div class="media">
             <div class="avatar"><img class="align-self-start mr-3" src="https://via.placeholder.com/110x110" alt="user avatar"></div>
            <div class="media-body">
            <h6 class="mt-2 user-title">Sarajhon Mccoy</h6>
            <p class="user-subtitle">mccoy@example.com</p>
            </div>
           </div>
          </a>
        </li>
        <li class="dropdown-divider"></li>
        <li class="dropdown-item"><i class="icon-envelope mr-2"></i> Inbox</li>
        <li class="dropdown-divider"></li>
        <li class="dropdown-item"><i class="icon-wallet mr-2"></i> Account</li>
        <li class="dropdown-divider"></li>
        <li class="dropdown-item"><i class="icon-settings mr-2"></i> Setting</li>
        <li class="dropdown-divider"></li>
        <li class="dropdown-item"><i class="icon-power mr-2"></i> Logout</li>
      </ul>
    </li>
  </ul>
</nav>
</header>
<div class="right-sidebar">
  <div class="switcher-icon">
    <i class="zmdi zmdi-settings zmdi-hc-spin"></i>
  </div>
  <div class="right-sidebar-content">
    <p class="mb-0">Gaussion Texture</p>
    <hr>
    <ul class="switcher">
      <a href="#" onclick="changeCSS('{!! asset('theme_includes/css/app-style.css') !!}', 0);"> <li id="theme1"></li></a>
      <a href="#" onclick="changeCSS('{!! asset('theme_includes/css/app-style.css') !!}', 0);"><li id="theme2"></li></a>
      <a href="#" onclick="changeCSS('{!! asset('theme_includes/css/app-style.css') !!}', 0);"><li id="theme3"></li></a>
      <a href="#" onclick="changeCSS('{!! asset('theme_includes/css/app-style.css') !!}', 0);"><li id="theme4"></li></a>
      <a href="#" onclick="changeCSS('{!! asset('theme_includes/css/app-style.css') !!}', 0);"><li id="theme5"></li></a>
      <a href="#" onclick="changeCSS('{!! asset('theme_includes/css/app-style.css') !!}', 0);"><li id="theme6"></li></a>
    </ul>
    <p class="mb-0">Gradient Background</p>
    <hr>
    <ul class="switcher">
      <a href="#" onclick="changeCSS('{!! asset('theme_includes/css/app-style.css') !!}', 0);">  <li id="theme7"></li></a>
      <a href="#" onclick="changeCSS('{!! asset('theme_includes/css/app-style.css') !!}', 0);"><li id="theme8"></li></a>
      <a href="#" onclick="changeCSS('{!! asset('theme_includes/css/app-style.css') !!}', 0);"><li id="theme9"></li></a>
      <a href="#" onclick="changeCSS('{!! asset('theme_includes/css/app-style.css') !!}', 0);"><li id="theme10"></li></a>
      <a href="#" onclick="changeCSS('{!! asset('theme_includes/css/app-style.css') !!}', 0);"><li id="theme12"></li></a>
      <a href="#" onclick="changeCSS('{!! asset('theme_includes/css/app-style.css') !!}', 0);"><li id="theme11"></li></a>
      <a href="#" onclick="changeCSS('{!! asset('theme_includes/css/app-style.css') !!}', 0);"><li id="theme13"></li></a>
      <a href="#" onclick="changeCSS('{!! asset('theme_includes/css/app-style.css') !!}', 0);"><li id="theme14"></li></a>
      <a href="#" onclick="changeCSS('{!! asset('theme_includes/css/app-style.css') !!}', 0);"><li id="theme15"></li></a>
      <a href="#" onclick="changeCSS('{!! asset('theme_includes/css/app-style16.css') !!}', 0);"><li id="theme16"></li></a>
      <a href="#" onclick="changeCSS('{!! asset('theme_includes/css/app-style17.css') !!}', 0);"><li id="theme17"></li></a>
      <a href="#" onclick="changeCSS('{!! asset('theme_includes/css/app-style18.css') !!}', 0);"><li id="theme18"></li></a>
      <a href="#" onclick="changeCSS('{!! asset('theme_includes/css/app-style19.css') !!}', 0);"><li id="theme19"></li></a>
      <a href="#" onclick="changeCSS('{!! asset('theme_includes/css/app-style20.css') !!}', 0);"><li id="theme20"></li></a>
    </ul>
  </div>
</div>
@include('common.sidebar') 
@include('common.footer')
<!--End topbar header-->
</body>
</html>
<script>
function changeCSS(cssFile, cssLinkIndex) {

    var oldlink = document.getElementsByTagName("link").item(cssLinkIndex);

    var newlink = document.createElement("link");
    newlink.setAttribute("rel", "stylesheet");
    newlink.setAttribute("type", "text/css");
    newlink.setAttribute("href", cssFile);

    document.getElementsByTagName("head").item(cssLinkIndex).replaceChild(newlink, oldlink);
}
</script>
