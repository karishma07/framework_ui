<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <meta name="description" content=""/>
    <meta name="author" content=""/>
    <title></title>
    <!-- loader-->
    <link rel="stylesheet" href="{!! asset('theme_includes/css/pace.min.css') !!}">
    <!--favicon-->
    <link rel="stylesheet" href="{!! asset('theme_includes/images/favicon.ico') !!}">
    <!-- Bootstrap core CSS-->
    <link rel="stylesheet" href="{!! asset('theme_includes/css/bootstrap.min.css') !!}">
    <!-- animate CSS-->
    <link rel="stylesheet" href="{!! asset('theme_includes/css/animate.css') !!}">
    <!-- Icons CSS-->
    <link rel="stylesheet" href="{!! asset('theme_includes/css/icons.css') !!}">
    <!-- Custom Style-->
    <link rel="stylesheet" href="{!! asset('theme_includes/css/app-style.css') !!}">
    <!-- Bootstrap core JavaScript-->
    <script src="{!! asset('theme_includes/js/jquery.min.js') !!}" type="text/javascript"></script>
    <script src="{!! asset('theme_includes/js/popper.min.js') !!}" type="text/javascript"></script>
    <script src=" {!! asset('theme_includes/js/bootstrap.min.js') !!}" type="text/javascript"></script>
    <!-- sidebar-menu js -->
    <script src="{!! asset('theme_includes/js/sidebar-menu.js') !!}" type="text/javascript"></script>
    <!-- Custom scripts -->
    <script src="{!! asset('theme_includes/js/app-script.js') !!}" type="text/javascript"></script>
    <script src="{!! asset('theme_includes/js/pace.min.js') !!}" type="text/javascript"></script>
  </head>
  <body>

  	<!--start color switcher-->
    <div class="right-sidebar">
    <div class="switcher-icon">
      <i class="zmdi zmdi-settings zmdi-hc-spin"></i>
    </div>
    <div class="right-sidebar-content">

      <p class="mb-0">Gaussion Texture</p>
      <hr>
      
      <ul class="switcher">
      <a href="#" onclick="changeCSS('{!! asset('theme_includes/css/app-style.css') !!}', 0);"> <li id="theme1"></li></a>
      <a href="#" onclick="changeCSS('{!! asset('theme_includes/css/app-style.css') !!}', 0);"><li id="theme2"></li></a>
      <a href="#" onclick="changeCSS('{!! asset('theme_includes/css/app-style.css') !!}', 0);"><li id="theme3"></li></a>
      <a href="#" onclick="changeCSS('{!! asset('theme_includes/css/app-style.css') !!}', 0);"><li id="theme4"></li></a>
      <a href="#" onclick="changeCSS('{!! asset('theme_includes/css/app-style.css') !!}', 0);"><li id="theme5"></li></a>
      <a href="#" onclick="changeCSS('{!! asset('theme_includes/css/app-style.css') !!}', 0);"><li id="theme6"></li></a>
    </ul>
    <p class="mb-0">Gradient Background</p>
    <hr>
    <ul class="switcher">
      <a href="#" onclick="changeCSS('{!! asset('theme_includes/css/app-style.css') !!}', 0);">  <li id="theme7"></li></a>
      <a href="#" onclick="changeCSS('{!! asset('theme_includes/css/app-style.css') !!}', 0);"><li id="theme8"></li></a>
      <a href="#" onclick="changeCSS('{!! asset('theme_includes/css/app-style.css') !!}', 0);"><li id="theme9"></li></a>
      <a href="#" onclick="changeCSS('{!! asset('theme_includes/css/app-style.css') !!}', 0);"><li id="theme10"></li></a>
      <a href="#" onclick="changeCSS('{!! asset('theme_includes/css/app-style.css') !!}', 0);"><li id="theme12"></li></a>
      <a href="#" onclick="changeCSS('{!! asset('theme_includes/css/app-style.css') !!}', 0);"><li id="theme11"></li></a>
      <a href="#" onclick="changeCSS('{!! asset('theme_includes/css/app-style.css') !!}', 0);"><li id="theme13"></li></a>
      <a href="#" onclick="changeCSS('{!! asset('theme_includes/css/app-style.css') !!}', 0);"><li id="theme14"></li></a>
      <a href="#" onclick="changeCSS('{!! asset('theme_includes/css/app-style.css') !!}', 0);"><li id="theme15"></li></a>
      <a href="#" onclick="changeCSS('{!! asset('theme_includes/css/app-style16.css') !!}', 0);"><li id="theme16"></li></a>
      <a href="#" onclick="changeCSS('{!! asset('theme_includes/css/app-style17.css') !!}', 0);"><li id="theme17"></li></a>
      <a href="#" onclick="changeCSS('{!! asset('theme_includes/css/app-style18.css') !!}', 0);"><li id="theme18"></li></a>
      <a href="#" onclick="changeCSS('{!! asset('theme_includes/css/app-style19.css') !!}', 0);"><li id="theme19"></li></a>
      <a href="#" onclick="changeCSS('{!! asset('theme_includes/css/app-style20.css') !!}', 0);"><li id="theme20"></li></a>
    </ul>
      
     </div>
   </div>
  <!--end color switcher-->

  <script>
    function changeCSS(cssFile, cssLinkIndex) {
      var oldlink = document.getElementsByTagName("link").item(cssLinkIndex);
      var newlink = document.createElement("link");
      newlink.setAttribute("rel", "stylesheet");
      newlink.setAttribute("type", "text/css");
      newlink.setAttribute("href", cssFile);
      document.getElementsByTagName("head").item(cssLinkIndex).replaceChild(newlink, oldlink);
    }
  </script>

  </body>
</html>
