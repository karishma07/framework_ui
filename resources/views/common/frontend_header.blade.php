<style>
h1.framework-app-name {
    margin-left: 8px;
    text-align: center !important;
}

.framework-container {
    display: flex;
    flex-direction: column;
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
}

.b-color {
    max-height: 60px;
    background-color: rgb(6, 111, 197);
    color: white !important;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}
.field-error {
    color: #FF0000;
    font-size:85%;
}
</style>
<!-- <link rel="stylesheet" href="{!! asset('theme_includes/css/fronend_header/fronend_header.css') !!}"> -->
<div class="framework-container">
        <nav class="navbar navbar-expand-lg navbar-dark b-color">
            <!-- <a class="nav-link waves-effect waves-light"> <i class="fas fa-bars"></i></a> -->
            <!-- Navbar brand -->
            
            <!-- <span class="toggl_bar_new1" style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776;</span> -->
            <!-- <div style="width:100%;">
                <a class="navbar-brand" href="#">{{ __('messages.Framework') }}</a>
            </div>
             -->
            <div class="collapse navbar-collapse" id="navbarTogglerDemo02" style="float:right !important;width:15%;">
                <ul type="none" class="ul_align">
                <li class="nav-item dropdown margin_top_5 " style="  margin-right: 5px;background: #fff;border-radius:5px;width:100%;text-align:center;">
                    <a class=" dropdown-toggle nav-link" href="#" id="navbardrop" data-toggle="dropdown">
                        @if(app()->getLocale() =='en')         
                       <img src="{{asset('theme_includes/img/EN.png')}}">&nbsp;English      
                       @else
                       <img src="{{asset('theme_includes/img/TH.png')}}">&nbsp;{{ __('messages.Thai') }}
                    @endif
                    <span class="caret"></span>
                    </a>
                    
                     <div class="dropdown-menu">
                            <a class="nav-link " href="../../locale/en"> <img src="{{asset('theme_includes/img/EN.png')}}">&nbsp;English</a>
                            <a class="nav-link " href="../../locale/th">  <img src="{{asset('theme_includes/img/TH.png')}}">&nbsp;{{ __('messages.Thai') }}</a>
                     </div>
                </li>
                </ul>
               </div>
            
            <!--<a id="lnkLogout">Logout</a>-->
        </nav>
    </div>