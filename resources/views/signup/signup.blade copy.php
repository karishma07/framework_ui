@extends('common.login_layout')
@section('title', 'SignUp | SignUp')
@section('content')
<style>
  .not-allowed {
      cursor: not-allowed !important;
      pointer-events:none;
      
   }
</style>
<link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js" type="text/javascript"></script>
<script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js" type="text/javascript"></script>
<link rel="stylesheet" href="{!! asset('theme_includes/css/mdb.min.css') !!}">
<link rel="stylesheet" type="text/css" href="http://www.shieldui.com/shared/components/latest/css/light/all.min.css" />
<script type="text/javascript" src="http://www.shieldui.com/shared/components/latest/js/shieldui-all.min.js"></script>
<script type="text/javascript" src="http://www.shieldui.com/shared/components/latest/js/jszip.min.js"></script>

<script src="{!! asset('js/register.js') !!}" type="text/javascript"></script>
<script src="{!! asset('js/register-validator.js') !!}" type="text/javascript"></script>
<link rel="stylesheet" href="{!! asset('theme_includes/css/signup/signup.css') !!}">
    <div class="container">
        <div class="d-flex justify-content-center align-items-center" style="height: 100vh;">
            <div class="card-body signup-details">
                <form class="validatedForm" id="registerFormID">
                <!--Header-->
                <div class="form-header">
                    <h3 class="login-header-style">{{ __('messages.Sign_Up') }}</h3>
                </div>

                <!--Body-->
                <div class="form-outline mb-4 position1">
                    <i class="fas fa-user  position2"></i>
                    <input type="text" id="userName" name="userName" class="form-control">
                    <label for="UserFormName" class="form-label"> {{ __('messages.User_name') }}</label>
                </div>
 		        <div class='field-error' id="username_error"></div>
                <div class="form-outline mb-4 position1">
                    <i class="fas fa-eye-slash  position2 classList" id="togglePassword"></i>
                    <input type="password" id="password" name="password"class="form-control">
                    <label for="UserFormPass" class="form-label"> {{ __('messages.Password') }}</label>
                </div>
 		        <div class='field-error' id="password_error"></div>
                <!--<div class="form-outline mb-4 position1">
                    <i class="fas fa-eye-slash  position2 classList2" id="togglePassword2"></i>
                    <input type="password" id="confirmpass"name="confirmpass" class="form-control ">
                    <label for="UserFormConfirmpass" class="form-label"> {{ __('messages.Confirm_password') }}</label>
                </div>
		        <div class='validation' id="confirmPass-validation"></div>-->
                <div class="form-outline mb-4 position1">
                    <i class="fas fa-mobile  position2" id="togglePassword"></i>
                    <input type="text" id="mobile" name="mobile" class="form-control" maxlength="10">
                    <label for="mobile" class="form-label">{{ __('messages.Mobile') }}</label>
                </div>
 		        <div class='field-error' id="mobile_error"></div>
         <div class="otp_div">
		<div class="form-outline mb-4 position1">
                    <i class="fas fa-mobile  position2" id="togglePassword"></i>
                    <input type="text" id="otp" name="otp"class="form-control" maxlength="6">
                    <label for="otp" class="form-label">{{ __('messages.OTP') }}</label>
                </div>
                <span id="otp_error" class="field-error"></span>
                <!-- <div id="new_chq"></div>
                <input type="hidden" value="1" id="total_chq"> -->
                <div class="input_fields_wrap">
                   
                </div>
                </div>
              <div class="text-center">
                    <button id="btnRegister" name="btnSubmit" type="button" class="signup-button"><i
                        class="fas fa-user-plus "></i>&nbsp;Create a New Account</button>
                        <!--otp verification button initially hide-->
                        <button class="otp_verify" id="verifyOTP" type="button">{{ __('messages.Verify_OTP') }}</button>

                    <a href="{{ url('/')}}"><button class="back-button"><i
                            class="fas fa-reply "></i>&nbsp;{{ __('messages.Back') }}</button></a>
                </div>
                
                </form>
            </div>
        </div>
    </div>
    <!-- End your project here-->
@stop
