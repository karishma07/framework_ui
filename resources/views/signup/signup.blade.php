@extends('common.frontend_layout')
@section('title', 'SignUp | SignUp')
@section('content')
<style>


</style>
<script src="{!! asset('js/register.js') !!}" type="text/javascript"></script>
<script src="{!! asset('js/register-validator.js') !!}" type="text/javascript"></script>
<link rel="stylesheet" href="{!! asset('theme_includes/css/signup/signup.css') !!}">
    <div class="container">
        <div class="d-flex justify-content-center align-items-center" style="height: 100vh;">
            <div class="card-body signup-details">
                <form class="validatedForm" name="validatedForm" id="registerFormID">
                <!--Header-->
                <div class="form-header">
                    <h3 class="login-header-style">{{ __('messages.Sign_Up') }}</h3>
                </div>

                <!--Body-->
                <div class="form-outline mb-4 position1">
                    <i class="fas fa-user  position2"></i>
                    <input type="text" id="userName" name="username" class="form-control">
                    <label for="UserFormName" class="form-label ">{{ __('messages.User_name') }}</label>
                </div>
 		        <div class='field-error validation' id="username_error"></div>
                <div class="form-outline mb-4 position1">
                    <i class="fas fa-eye-slash  position2 classList " id="togglePassword"></i>
                    <input type="password" id="password" name="password"class="form-control">
                    <label for="UserFormPass" class="form-label">{{ __('messages.Password') }}</label>
                </div>
 		        <div class='field-error validation' id="password_error"></div>
                <div class="form-outline mb-4 position1">
                    <i class="fas fa-eye-slash  position2 classList" id="togglePassword1"></i>
                    <input type="password" id="confirm_password" name="confirm_password"class="form-control">
                    <label for="confirm_password" class="form-label">{{ __('messages.Confirm_Password') }}</label>
                </div>
                 <div class='field-error validation' id="confirm_password_error"></div>
                 
                <div class="form-outline mb-4 position1">
                    <i class="fas fa-mobile  position2"></i>
                    <input type="text" id="mobile" name="mobile" class="form-control" maxlength="10">
                    <label for="mobile" class="form-label">{{ __('messages.Mobile') }}</label>
                </div>
 		        <div class='field-error validation' id="mobile_error"></div>
         <div class="otp_div">
		    <div class="form-outline mb-4 position1">
                    <i class="fas fa-mobile  position2" ></i>
                    <input type="text" name="otp" id="otp" class="form-control" maxlength="6">
                    <label for="otp" class="form-label">{{ __('messages.OTP') }}</label>
                </div>
                <div class='field-error validation' id="otp_error"></div>
                <div class='validation' id="otp-validation"></div>
                <!-- <div id="new_chq"></div>
                <input type="hidden" value="1" id="total_chq"> -->
                <div class="input_fields_wrap">
                   
                </div>
                </div>
              <div class="text-center">
                    <button id="btnRegister" name="btnSubmit" type="button" class="signup-button"><i
                        class="fas fa-user-plus "></i>&nbsp;Create a New Account</button>
                        <!--otp verification button initially hide-->
                        <button class="otp_verify" id="verifyOTP" type="button">{{ __('messages.Verify_OTP') }}P</button>

                    <a href="{{ url('/')}}"><button class="back-button" id="btnBack" type="button"><i
                            class="fas fa-reply "></i>&nbsp;{{ __('messages.Back') }}</button></a>
                </div>
                
                </form>
            </div>
        </div>
    </div>
    <!-- End your project here-->
@stop