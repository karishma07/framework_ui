@extends('common.login_layout')
@section('title', 'Forget password')
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>

<script src="{!! asset('js/forgot_password.js') !!}" type="text/javascript"></script> 
<link rel="stylesheet" href="{!! asset('theme_includes/css/allcss/allcss.css') !!}">
<link rel="stylesheet" href="{!! asset('theme_includes/css/forget_password/forget_password.css') !!}">
<script>
var Email_is_required = "{{ __('messages.Email_is_required') }}";
var Email_contain = "{{ __('messages.Email_contain') }}";
var Error = "{{ __('messages.Error') }}";
var Email_invalid = "{{ __('messages.Email_invalid') }}";
var pls_chk_api_url = "{{ __('messages.pls_chk_api_url') }}";
var Ok = "{{ __('messages.Ok') }}";
var Password_link_sent_successfully = "{{ __('messages.Password_link_sent_successfully') }}";
var Something_Went_Wrong_Exception_Occured = "{{ __('messages.Something_Went_Wrong_Exception_Occured') }}";
var Success = "{{ __('messages.Success') }}";
</script>
 <style>
 /* css for label active start */




/* end label css */
 </style>
<body class="bg-theme bg-theme1">

<!-- Start wrapper-->
 <div id="wrapper">

 <div class="height-100v d-flex align-items-center justify-content-center">
	<div class="card background_color_white box_shadow card_marg card-authentication1 mb-0">
		<div class="card-body">
		 <div class="card-content p-2">
     <div class="text-center">
            <img src="{!! asset('theme_includes/images/logo-icon.png') !!}" src="assets/images/logo-icon.png" alt="logo icon">
          </div>
		  <div class="card-title text-uppercase text-center py-3">{{ __('messages.Forget_Password') }}</div>
		    <p class="pb-2" style="padding-left:2px;">Please enter your email address. You will receive a link to create a new password via email.</p>
		    <form>
			  <div class="form-group">
			  
			   <div class="position-relative has-icon-right">
				  <input type="text" id="username" name="username" class="form-control input-shadow">

				  <div class="form-control-position">
					  <i class="icon-envelope-open"></i>
				  </div>
          <label for="username" class="form-label" style="padding-left:2px;">{{ __('messages.Email') }}</label>
			   </div>
            <span id="username-validation" class="validation"></span>
			  </div>
             
			 
			  <button type="button" id="btnForgotPass" class="btn btn-light btn-block mt-3 btn_send">{{ __('messages.Send') }}</button>
			 </form>
		   </div>
		  </div>
		   <div class="card-footer text-center py-3">
		    <p class="text-warning mb-0">Return to the <a href="{{url('/') }}"> Sign In</a></p>
		  </div>
	     </div>
	     </div>
    
     <!--Start Back To Top Button-->
    <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
    <!--End Back To Top Button-->
	
	<!--start color switcher-->
   <div class="right-sidebar">
    <div class="switcher-icon">
      <i class="zmdi zmdi-settings zmdi-hc-spin"></i>
    </div>
    <div class="right-sidebar-content">

      <p class="mb-0">Gaussion Texture</p>
      <hr>
      
      <ul class="switcher">
        <li id="theme1"></li>
        <li id="theme2"></li>
        <li id="theme3"></li>
        <li id="theme4"></li>
        <li id="theme5"></li>
        <li id="theme6"></li>
      </ul>

      <p class="mb-0">Gradient Background</p>
      <hr>
      
      <ul class="switcher">
        <li id="theme7"></li>
        <li id="theme8"></li>
        <li id="theme9"></li>
        <li id="theme10"></li>
        <li id="theme11"></li>
        <li id="theme12"></li>
		<li id="theme13"></li>
        <li id="theme14"></li>
        <li id="theme15"></li>
      </ul>
      
     </div>
   </div>
  <!--end color switcher-->
	
	</div><!--wrapper-->
	
</body>
</html>
 

