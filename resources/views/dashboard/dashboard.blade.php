@section('title', 'Dashboard')
@section('scripts')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/dataTables.bootstrap4.min.css">
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<style>
  .bargrp_div{
    background: #fff;
    margin-left: 0px !important;
    margin-right: 2.3% !important;
    border-radius: 5px;
  }
</style>
<body class="bg-theme bg-theme1">
  <div class="clearfix"></div>
  <div id="wrapper">
    @include('common.header') 
    <div class="content-wrapper">
      <div class="container-fluid">
        <div class="card mt-3">
          <div class="card-content">
            <div class="card-body" style="display: flex;padding-left: 0px;padding-right: 0px;">
              <div class="row row_width">
                <div class="col-lg-4 margin_bottom_div">
                  <div class="div_style">
                    <h5 class="text_black mb-0" style="text-align: center;font-size: 4em;"> <i class="zmdi zmdi-assignment-o"></i></h5>
                    <hr class="hr_back">
                    <p class="text_black mb-0  small-font">Approved Requisitions<span class="float-right">2</span></p>
                  </div>
                </div>
                <div  class="col-lg-4 margin_bottom_div">
                  <div class="div_style">
                    <h5 class="text_black mb-0" style="text-align: center;font-size: 4em;"> <i class="fa fa-graduation-cap" ></i></h5>
                    <hr class="hr_back">
                    <p class="text_black mb-0 small-font">Approved Jobs <span class="float-right">5</span></p>
                  </div>
                </div>
                <div  class="col-lg-4 div_padding_right ">
                  <div class="div_style">
                    <h5 class="text_black mb-0" style="text-align: center;font-size: 4em;"> <i class="zmdi zmdi-accounts"></i></h5>
                    <hr class="hr_back">
                    <p class="mb-0 text_black small-font">Open Positions <span class="float-right">4</span></p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>  
        <div class="card mt-3 ">
		      <div class="card-body ipad_pro_margin_top">
		    	  <div class="row">
				      <div class="col-lg-8" style="margin-right: -0.4%;">
                <div class="row bargrp_div">
                  <div class="col-sm-9 overflow_div">
                    <div id="barchart" class="barchart_div"></div>
                  </div>
                  <div class="col-sm-3 bar_chart_text">
                    <p style="color:#000;">No Of Jobs</p>
                  </div>
                </div>
				      </div>
              <div class="col-lg-4 padding_left">
                <div class="piechart_div">
					      <div id="donutchart" style="width: 320px; height: 300px;"></div>
                </div>
				      </div>
			      </div>
		      </div>
        </div>
        <br>
        <div class="row table_div">
          <div class="col-sm-12 table_div1">
          <div class="table-responsive">
            <table id="example" class="table table-striped table-bordered" style="width:100%">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Position</th>
                  <th>Office</th>
                  <th>Age</th>
                  <th>Start date</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Tiger</td>
                  <td>System Architect</td>
                  <td>india</td>
                  <td>25</td>
                  <td>2011/04/25</td>
                </tr>
                <tr>
                  <td>Garrett</td>
                  <td>Accountant</td>
                  <td>Tokyo</td>
                  <td>27</td>
                  <td>2011/07/25</td>
                </tr>
              </tbody>
            </table>
</div>
          </div>
        </div>
      </div>
      <div class="overlay toggle-menu"></div>
    </div>
  </div>
</body>
<script type = "text/javascript">
// function checkCookie() {
//   debugger;
//   var username = getCookie("username");
//   if (username != "") {
//    alert("Welcome again " + username);
//   } else {
//     username = prompt("Please enter your name:", "");
//     if (username != "" && username != null) {
//       setCookie("username", username, 365);
//     }
//   }
// }



  google.charts.load('current', {packages: ['corechart']});     
</script>
<script>
  google.charts.load('current', {'packages':['bar']});
  google.charts.setOnLoadCallback(drawChart);

  function drawChart() {
		// Define the chart to be drawn.
		var options = {
      legend: 'none',
			chart: {
				title: '  ',
				subtitle: '',
			  },
		 bars: 'vertical' // Required for Material Bar Charts.
	
	}; 
  var data = google.visualization.arrayToDataTable([
    ['Year', 'No Of Jobs'],
    ['2015',  15],
    ['2016',  10],
    ['2017',  8],
    ['2018',  5],
    ['2019',  10],
    ['2020',  3]
  ]);
		// Instantiate and draw the chart.
  var chart = new google.visualization.ColumnChart(document.getElementById('barchart'));
		chart.draw(data,options);
  }
  google.charts.setOnLoadCallback(drawChart);
</script>
<script>
  google.charts.load("current", {packages:["corechart"]});
  google.charts.setOnLoadCallback(drawChart);
  function drawChart() {
    var data = google.visualization.arrayToDataTable([
      ['Task', 'Hours per Day'],
      ['PHP',     25],
      ['.NET',      40],
      ['Angular',  15],
      ['UI', 12],
      ['Testing', 8]
          
    ]);

    var options = {
      title: 'Total Employees',
      pieHole: 0.4,
    };

    var chart = new google.visualization.PieChart(document.getElementById('donutchart'));
    chart.draw(data, options);
  }
</script>
<script>
  $(document).ready(function() {
    $('#example').DataTable();
} );
</script>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.24/js/dataTables.bootstrap4.min.js"></script>

<script>
function changeCSS(cssFile, cssLinkIndex) {

    var oldlink = document.getElementsByTagName("link").item(cssLinkIndex);

    var newlink = document.createElement("link");
    newlink.setAttribute("rel", "stylesheet");
    newlink.setAttribute("type", "text/css");
    newlink.setAttribute("href", cssFile);

    document.getElementsByTagName("head").item(cssLinkIndex).replaceChild(newlink, oldlink);
}
</script>