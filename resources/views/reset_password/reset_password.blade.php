@extends('common.login_layout')
@section('title', 'Reset password')

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script src="{!! asset('js/reset_password.js') !!}" type="text/javascript"></script>
<link rel="stylesheet" href="{!! asset('theme_includes/css/allcss/allcss.css') !!}">
<link rel="stylesheet" href="{!! asset('theme_includes/css/reset_password/reset_password.css') !!}">
<script>
var Password_required = "{{ __('messages.Password_required') }}";
var Password_valid = "{{ __('messages.Password_valid') }}";
var Confirm_Password_required = "{{ __('messages.Confirm_Password_required') }}";
var Confirm_Password_valid = "{{ __('messages.Confirm_Password_valid') }}";
var Password_and_confirm_password_must_match = "{{ __('messages.Password_and_confirm_password_must_match') }}";
var Error = "{{ __('messages.Error') }}";
var Email_invalid = "{{ __('messages.Email_invalid') }}";
var pls_chk_api_url = "{{ __('messages.pls_chk_api_url') }}";
var Ok = "{{ __('messages.Ok') }}";
var Success = "{{ __('messages.Success') }}";
var Password_reset_successfully = "{{ __('messages.Password_reset_successfully') }}";
var Invalid_Credentials = "{{ __('messages.Invalid_Credentials') }}";
var Something_Went_Wrong_Exception_Occured = "{{ __('messages.Something_Went_Wrong_Exception_Occured') }}";

</script>

<body class="bg-theme bg-theme1">

  <!-- start loader -->
  <div id="pageloader-overlay" class="visible incoming">
    <div class="loader-wrapper-outer">
      <div class="loader-wrapper-inner">
        <div class="loader"></div>
      </div>
    </div>
  </div>
  <!-- end loader -->

  <!-- Start wrapper-->
  <div id="wrapper">
  <div class="height-100v d-flex align-items-center justify-content-center">
    <div class="card card-authentication1 mx-auto my-4">
      <div class="card-body">
        <div class="card-content p-2">
          <div class="text-center">
            <img src="{!! asset('theme_includes/images/logo-icon.png') !!}" src="assets/images/logo-icon.png" alt="logo icon">
          </div>
          <div class="card-title text-uppercase text-center py-3">{{ __('messages.Reset_Password') }}</div>
          <form>

            <div class="form-group">
              <label for="exampleInputPassword" class="sr-only">{{ __('messages.Password') }}</label>
              <div class="position-relative has-icon-right">
                <input type="password" id="newPassword" name="newPassword" class="form-control input-shadow" placeholder="Enter Password">
                <div class="form-control-position">
                  <i class="icon-lock"></i>
                </div>
              </div>
              <span id="newPassword-validation" class="validation"></span>
            </div>
           

            <div class="form-group">
              <label for="UserFormPass" class="sr-only">{{ __('messages.Confirm_Password') }}</label>
              <div class="position-relative has-icon-right">
                <input type="password" id="confirm_password" name="confirm_password" class="form-control input-shadow" placeholder="Enter Confirm Password">
                <div class="form-control-position">
                  <i class="icon-lock"></i>
                </div>
              </div>
              <span id="newPassword-validation" class="validation"></span>
            </div>
            <span id="confirm_password-validation" class="validation"></span>

            
            <button type="button" type="button"  id="btnResetPass" class="btn btn-light btn-block waves-effect waves-light btn_send">{{ __('messages.Send') }}</button>
            


          </form>
          <div class="card-footer text-center py-3">
		    <p class="text-warning mb-0">Return to the <a href="{{url('/') }}"> Sign In</a></p>
		  </div>
        </div>
      </div>
      
    </div>
  </div>

    <!--Start Back To Top Button-->
    <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
    <!--End Back To Top Button-->

    <!--start color switcher-->
    <div class="right-sidebar">
      <div class="switcher-icon">
        <i class="zmdi zmdi-settings zmdi-hc-spin"></i>
      </div>
      <div class="right-sidebar-content">

        <p class="mb-0">Gaussion Texture</p>
        <hr>

        <ul class="switcher">
          <li id="theme1"></li>
          <li id="theme2"></li>
          <li id="theme3"></li>
          <li id="theme4"></li>
          <li id="theme5"></li>
          <li id="theme6"></li>
        </ul>

        <p class="mb-0">Gradient Background</p>
        <hr>

        <ul class="switcher">
          <li id="theme7"></li>
          <li id="theme8"></li>
          <li id="theme9"></li>
          <li id="theme10"></li>
          <li id="theme11"></li>
          <li id="theme12"></li>
          <li id="theme13"></li>
          <li id="theme14"></li>
          <li id="theme15"></li>
        </ul>

      </div>
    </div>
    <!--end color switcher-->

  </div>
  <!--wrapper-->



</body>

 