@extends('common.frontend_layout')
@section('title', 'Employee List')
@section('scripts')

<link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
<!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js" type="text/javascript"></script> -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>


<script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js" type="text/javascript"></script>
<link rel="stylesheet" href="{!! asset('theme_includes/css/employee/employee.css') !!}">
<!-- <link rel="stylesheet" href="{!! asset('theme_includes/css/mdb.min.css') !!}"> -->
<link rel="stylesheet" type="text/css" href="http://www.shieldui.com/shared/components/latest/css/light/all.min.css" />
<script type="text/javascript" src="http://www.shieldui.com/shared/components/latest/js/shieldui-all.min.js"></script>
<script type="text/javascript" src="http://www.shieldui.com/shared/components/latest/js/jszip.min.js"></script>
<script src="{!! asset('js/dashboard-validator.js') !!}" type="text/javascript"></script>
<script src="{!! asset('js/utils.js') !!}" type="text/javascript"></script>
<script src="{!! asset('js/employee.js') !!}" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.2/jspdf.min.js"></script>
<script src="https://cdn.rawgit.com/rainabba/jquery-table2excel/1.1.0/dist/jquery.table2excel.min.js"></script>


<body class="bg-theme bg-theme1">
    <div class="clearfix"></div>
    <div id="wrapper">
        @include('common.header')

        <div class="content-wrapper">
            <div class="container-fluid">



                <div id="main" class="mainDiv">
                    <div class="margindiv" id="data-grid-container">
                        <div class="datatbl">
                            <div class="btn_e_i_d">
                                <button type="button" class="btn btn-primary addbutton ripple-surface" id="btnShowEmployeeModal">
                                    <!-- data-toggle="modal" data-target="#exampleModal" -->
                                    <i class="fas fa-plus-circle "></i>
                                </button>

                                <a href="/import"><button id="" class="btn btn-primary btninport"><i class="fas fa-cloud-upload-alt"></i></button></a>
                                <!-- //excel -->
                                <a href="x" download="down.xls" id="exportTable"><button class="btn btn-primary btnexport"><i class="fas fa-file-export"></i></button></a>
                                <!-- pdf -->
                                <!-- <button id="" onclick="run();" class="btn btn-primary btnexport"><i class="fas fa-file-export"></i></button> -->
                            </div>
                            <div class="table-responsive" id="dvData">
                                <table id="table" class="display select" cellspacing="0" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th><input name="select_all" value="1" type="checkbox"></th>
                                            <th>{{ __('messages.Name') }}</th>
                                            <th>{{ __('messages.Designation') }}</th>
                                            <th>{{ __('messages.Experience') }}</th>
                                            <th>{{ __('messages.Contact') }}</th>
                                            <th>{{ __('messages.Upload') }}</th>
                                            <th>{{ __('messages.Edit') }}</th>
                                            <th>{{ __('messages.Remove') }}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <!-- @if(isset($employeeArray))
                        @foreach ($employeeArray as $empObj) -->
                                        <tr id="1">
                                            <td></td>
                                            <!-- <td>{{ $empObj->name}}</td>
                            <td>{{ $empObj->designation}}</td>
                            <td>{{ $empObj->experience}}</td>
                            <td>{{ $empObj->contact}}</td> -->
                                            <td>ABC</td>
                                            <td>Developer</td>
                                            <td>10 Year</td>
                                            <td>11111-11111</td>
                                            <td><a href="#"><button id="" class="btn btn-primary btnupload"><i class="fas fa-cloud-upload-alt"></i></button></a></td>
                                            <td><button type="button" class="btn btn-primary btnpencil editEmp btnEdit" id="btnEdit{{ $loop->index }}" record-id="{{ $empObj->id }}"><i class="fas fa-pen"></i></button></td> <!-- data-toggle="modal" -->
                                            <td><button id="button" class="btn btn-primary btntrasht btnRemoveEmployee" data-toggle="tooltip" title="Delete Employee" employee-id="{{ $empObj->id }}"><i class="fas fa-trash"></i></button></td>
                                        </tr>
                                        <!-- @endforeach
                        @endif -->
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <!-- end of main div -->
                @stop

            </div>
            <!-- End container-fluid-->

        </div>
        <!--End content-wrapper-->

        <!--start color switcher-->
        <div class="right-sidebar">
            <div class="switcher-icon">
                <i class="zmdi zmdi-settings zmdi-hc-spin"></i>
            </div>
            <div class="right-sidebar-content">

                <p class="mb-0">Gaussion Texture</p>
                <hr>

                <ul class="switcher">
                    <li id="theme1"></li>
                    <li id="theme2"></li>
                    <li id="theme3"></li>
                    <li id="theme4"></li>
                    <li id="theme5"></li>
                    <li id="theme6"></li>
                </ul>

                <p class="mb-0">Gradient Background</p>
                <hr>

                <ul class="switcher">
                    <li id="theme7"></li>
                    <li id="theme8"></li>
                    <li id="theme9"></li>
                    <li id="theme10"></li>
                    <li id="theme11"></li>
                    <li id="theme12"></li>
                    <li id="theme13"></li>
                    <li id="theme14"></li>
                    <li id="theme15"></li>
                </ul>

            </div>
        </div>
        <!--end color switcher-->

    </div><!--  end of wrapper -->
</body>