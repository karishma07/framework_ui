@extends('common.frontend_layout')
@section('title', 'Upload_Document')
@section('scripts')
<link rel="stylesheet" href="{!! asset('theme_includes/css/allcss/allcss.css') !!}">
<link rel="stylesheet" href="{!! asset('theme_includes/css/employee/upload_document.css') !!}">
<!-- Font Awesome CSS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<script type="text/javascript">
    function readUrl(input) {
  
  if (input.files && input.files[0]) {
    let reader = new FileReader();
    reader.onload = (e) => {
      let imgData = e.target.result;
      let imgName = input.files[0].name;
      input.setAttribute("data-title", imgName);
      console.log(e.target.result);
    }
    reader.readAsDataURL(input.files[0]);
  }

}
</script>
<style>

</style>
@stop
@section('content')
<body class="bg-theme bg-theme1">
<div class="clearfix"></div>
<div id="wrapper">
@include('common.header') 
<div class="content-wrapper">
      <div class="container-fluid">
          <div id="main"class="mainDiv">
    
        <div class="row">
            <div class="col-sm-12">
            <a href ="/upload"><button type="button" class="btn btn-light btn_back"><i class="fa fa-arrow-left" aria-hidden="true"></i></button></a>
            </div>
        </div>
        <div class="row margin_top_2" >
            <div class="col-sm-12" >
            <div class="bulkimport_container">
            
                <div class="form-group inputDnD">
                <button type="button" class="btn btn-default btn-block txt_color" onclick="document.getElementById('inputFile').click()">{{ __('messages.Bulk_Import_Excel_Sheet') }}</button>
                    <label class="sr-only" for="inputFile">{{ __('messages.File_Upload') }}File Upload <i class="fas fa-cloud-upload-alt"></i></label>
                    <input type="file" class="form-control-file txt_color font-weight-bold ip_bg_color" id="inputFile" accept=".doc,.pdf,.rtf,.docx" onchange="readUrl(this)" data-title="Drag and drop a file">
                </div>

                </div>
            </div>
        </div>

    
</div>
<!-- End your project here-->

</div>
      <!-- End container-fluid-->
      
      </div><!--End content-wrapper-->

      <!--start color switcher-->
   <div class="right-sidebar">
    <div class="switcher-icon">
      <i class="zmdi zmdi-settings zmdi-hc-spin"></i>
    </div>
    <div class="right-sidebar-content">

      <p class="mb-0">Gaussion Texture</p>
      <hr>
      
      <ul class="switcher">
        <li id="theme1"></li>
        <li id="theme2"></li>
        <li id="theme3"></li>
        <li id="theme4"></li>
        <li id="theme5"></li>
        <li id="theme6"></li>
      </ul>

      <p class="mb-0">Gradient Background</p>
      <hr>
      
      <ul class="switcher">
        <li id="theme7"></li>
        <li id="theme8"></li>
        <li id="theme9"></li>
        <li id="theme10"></li>
        <li id="theme11"></li>
        <li id="theme12"></li>
		<li id="theme13"></li>
        <li id="theme14"></li>
        <li id="theme15"></li>
      </ul>
      
     </div>
   </div> <!--end color switcher-->
   
   </div><!--  end of wrapper -->
   </body>@stop