@extends('common.frontend_layout')
@section('title', 'Employee List')
@section('scripts')
<!-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script> -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<link rel="stylesheet" href="{!! asset('theme_includes/css/allcss/allcss.css') !!}">
<link rel="stylesheet" href="{!! asset('theme_includes/css/employee/employee.css') !!}">
<link rel="stylesheet" type="text/css" href="http://www.shieldui.com/shared/components/latest/css/light/all.min.css" />
<script type="text/javascript" src="http://www.shieldui.com/shared/components/latest/js/shieldui-all.min.js"></script>
<script type="text/javascript" src="http://www.shieldui.com/shared/components/latest/js/jszip.min.js"></script>
<script src="{!! asset('js/dashboard-validator.js') !!}" type="text/javascript"></script>
<script src="{!! asset('js/utils.js') !!}" type="text/javascript"></script>
<script src="{!! asset('js/employee.js') !!}" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.2/jspdf.min.js"></script>
<script src="https://cdn.rawgit.com/rainabba/jquery-table2excel/1.1.0/dist/jquery.table2excel.min.js"></script>


@section('content')

<body class="bg-theme bg-theme1">
    <div class="clearfix"></div>
    <div id="wrapper">
        @include('common.header')
        <div class="content-wrapper">
            <div class="container-fluid">
                <div id="main" class="mainDiv">
                    <div class="margindiv" id="data-grid-container">
                        <div class="datatbl">
                            <div class="btn_e_i_d">
                                <button type="button" class="btn btn-light addbutton ripple-surface" id="btnShowEmployeeModal" title="Add">
                                    <!-- data-toggle="modal" data-target="#exampleModal" -->
                                    <i class="fas fa-plus-circle "></i>
                                </button>

                                <a href="/import"><button id="" class="btn btn-light btninport " title="Import"><i class="fas fa-cloud-upload-alt"></i></button></a>
                                <!-- //excel -->
                                <a href="x" download="down.xls" id="exportTable"><button class="btn btn-light btnexport" title="Export"><i class="fas fa-file-export"></i></button></a>
                                <!-- pdf -->
                                <!-- <button id="" onclick="run();" class="btn btn-primary btnexport"><i class="fas fa-file-export"></i></button> -->
                            </div>
                            <div class="table-responsive" id="dvData">
                                <table id="table" class="display select table" cellspacing="0" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th><input name="select_all" value="1" type="checkbox"></th>
                                            <th>{{ __('messages.Name') }}</th>
                                            <th>{{ __('messages.Designation') }}</th>
                                            <th>{{ __('messages.Experience') }}</th>
                                            <th>{{ __('messages.Contact') }}</th>
                                            <th>{{ __('messages.Upload') }}</th>
                                            <th>{{ __('messages.Edit') }}</th>
                                            <th>{{ __('messages.Remove') }}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(isset($employeeArray))
                                        @foreach ($employeeArray as $empObj)
                                        <tr id="1">
                                            <td></td>
                                            <td>{{ $empObj->name}}</td>
                                            <td>{{ $empObj->designation}}</td>
                                            <td>{{ $empObj->experience}}</td>
                                            <td>{{ $empObj->contact}}</td>

                                            <td><a href="#"><button id="" class="btn btn-primary btnupload"><i class="fas fa-cloud-upload-alt"></i></button></a></td>
                                            <td><button type="button" class="btn btn-primary btnpencil editEmp btnEdit" id="btnEdit{{ $loop->index }}" record-id="{{ $empObj->id }}"><i class="fas fa-pen"></i></button></td> <!-- data-toggle="modal" -->
                                            <td><button id="button" class="btn btn-primary btntrasht btnRemoveEmployee" data-toggle="tooltip" title="Delete Employee" employee-id="{{ $empObj->id }}"><i class="fas fa-trash"></i></button></td>
                                        </tr>
                                        @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade " id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="EmployeeDetailsModel" aria-hidden="true">
                        <div class="modal-dialog  modal-xl" role="document">
                            <div class="modal-content">
                                <div class="modal-body ">
                                    <div class="firstblock">
                                        <h5 class="bcolor" id="EmployeeDetailsModel">Employee Details</h5>
                                        <div class="card">
                                            <div class="card-body form_div_new">
                                                <form method="post" name="employeeForm" id="employeeFormID" style="margin-top:1%;">
                                                    <div class="row md-12 ">
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="hidden" name="id" id="id">

                                                                <input type="text" id="empName" name="name" class="form-control" />
                                                                <label class="form-label" for="empName">{{ __('messages.Name') }}</label>
                                                                <span id="name_error" class="field-error"></span>
                                                            </div>
                                                           
                                                        </div>
                                                        <div class=" col-md-3">
                                                            <div class="form-group">

                                                                <input type="text" id="empDesign" name="designation" class="form-control" />
                                                                <label class="form-label" for="empDesign">{{ __('messages.Designation') }}</label>
                                                                <span id="designation_error" class="field-error"></span>
                                                            </div>
                                                           
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">

                                                                <input type="text" id="empExp" name="experience" class="form-control" />
                                                                <label class="form-label" for="empExp">{{ __('messages.Experience') }}</label>
                                                                <span id="experience_error" class="field-error"></span>
                                                            </div>
                                                           
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">

                                                                <input type="text" id="empContact" name="contact" class="form-control" />
                                                                <label class="form-label" for="empContact">{{ __('messages.Contact') }}</label>
                                                                <span id="contact_error" class="field-error"></span>
                                                            </div>
                                                          
                                                        </div>
                                                    </div>
                                                    <div id="employeeProjectDetails">
                                                    </div>
                                                    <div id="employeePerformanceDetails">
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- Tabs navs -->
                                <ul class="nav nav-tabs nav-tabs-primary top-icon nav-justified nav_tab_top" >
                                    <li class="nav-item col-sm-3">
                                        <a href="javascript:void();" class="nav-link active" data-target="#ProjectDetails" data-toggle="pill">{{ __('messages.ProjectDetails') }}</a>
                                    </li>
                                    <li class="nav-item col-sm-3">
                                        <a href="javascript:void();" class="nav-link" data-target="#PerformanceDetails" data-toggle="pill">{{ __('messages.PerformanceDetails') }}</a>
                                    </li>
                                </ul>

                                <!-- Tabs navs -->

                                <!-- Tabs content -->
                                <div class="tab-content p-3">
                                    <div class="tab-pane active" id="ProjectDetails">
                                        <div class="table-responsive">
                                            <div class="btn2_e2_i2_d2">
                                                <button type="button" class="btn btn-light addbutton11 addBtnForm" data-toggle="modal" id="btnShowAddProjectModal" title="Add">
                                                    <i class="fas fa-plus-circle "></i>
                                                </button>
                                                <a href="x" download="down.xls" id="exportTableProject"><button class="btn btn-light btnexport11 " title="Export"><i class="fas fa-file-export"></i></button></a>
                                            </div>
                                            <div class="table-responsive" id="dvDataProject">
                                                <table id="projectDetailsTb" class="display select table" cellspacing="0" style="width:100%">
                                                    <thead>
                                                        <tr>
                                                            <th><input name="select_all" value="1" type="checkbox"></th>
                                                            <th>{{ __('messages.ProjectCode') }}</th>
                                                            <th>{{ __('messages.ProjectName') }}</th>
                                                            <th>{{ __('messages.Technology') }}</th>
                                                            <th>{{ __('messages.Duration') }}</th>
                                                            <th>{{ __('messages.Status') }}</th>
                                                            <th>{{ __('messages.Edit') }}</th>
                                                            <th>{{ __('messages.Delete') }}</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="tab-pane " id="PerformanceDetails">
                                        <div class="table-responsive">
                                            <div class="btn3_e3_i3_d3">
                                                <button type="button" class="btn btn-light addbutton11 addBtnForm" id="btnShowPerformanceModal" title="Add" data-dismiss="modal">
                                                    <!-- data-toggle="modal" data-target="#exampleModal3" -->
                                                    <i class="fas fa-plus-circle "></i>
                                                </button>
                                                <a href="x" download="down.xls" id="exportTablePerformance"><button class="btn btn-light btnexport11" title="Export"><i class="fas fa-file-export"></i></button></a>
                                            </div>
                                            <div class="table-responsive" id="dvDataPerformance">
                                                <table id="performanaceDetailsTb" class="display select" cellspacing="0" style="width:100%">
                                                    <thead>
                                                        <tr>
                                                            <th><input name="select_all" value="1" type="checkbox"></th>
                                                            <th>{{ __('messages.Year') }}</th>
                                                            <th>{{ __('messages.Grade') }}</th>
                                                            <th>{{ __('messages.Comments') }}</th>
                                                            <th>{{ __('messages.Edit') }}</th>
                                                            <th>{{ __('messages.Delete') }}</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="secondblock">
                                        <!-- Modal -->
                                        <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="EmployeeDetailsModel" aria-hidden="true">
                                            <div class="modal-dialog  modal-lg" role="document">
                                                <div class="modal-content ipad_height">
                                                    <div class="modal-header">
                                                        <h5 class="bcolor">{{ __('messages.ProjectDetails') }}</h5>
                                                        <button type="button" class="close cancleBtnproject" data-dismiss="modal" aria-label="Close" id="btnCloseProjectDtl">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="card">
                                                            <div class="card-body form_div">
                                                                <form method="post" name="addProjectForm" id="addProjectFormId">
                                                                    <div class="row md-12">
                                                                        <div class="col-md-6">

                                                                            <div class="form-group">
                                                                                <input type="hidden" name="id" id="projectId">

                                                                                <select class="form-control select_form" id="projectCode" name="project_code">
                                                                                    @if(isset($projectArray))
                                                                                    <option value=""></option>
                                                                                    @foreach($projectArray as $project)
                                                                                    <option value="{{$project->project_code}}">{{ $project->project_code}}</option>
                                                                                    @endforeach

                                                                                    @endif
                                                                                </select>
                                                                                <label class="form-label" for="projectCode">{{ __('messages.Project_Code') }}*</label>
                                                                                <span id="project_code_error" class="field-error"></span>
                                                                            </div>
                                                                            
                                                                        </div>
                                                                        <div class="col-md-6">

                                                                            <div class="form-group">

                                                                                <select class="form-control select_form" id="projectName" name="project_name">
                                                                                    @if(isset($projectArray))
                                                                                    <option value="">{{ __('messages.Select') }}...</option>
                                                                                    @foreach($projectArray as $project)
                                                                                    <option value="{{ $project->project_name}}">{{ $project->project_name}}</option>
                                                                                    @endforeach

                                                                                    @endif
                                                                                </select>
                                                                                <label class="form-label" for="projectName">{{ __('messages.Project_Name') }}*</label>
                                                                                <span id="project_name_error" class="field-error"></span>
                                                                            </div>
                                                                            
                                                                        </div>
                                                                    </div>
                                                                    <div class="row md-12 md_space">
                                                                        <div class="col-md-6">

                                                                            <div class="form-group">

                                                                                <input type="text" id="technology" name="technology" class="form-control" />
                                                                                <label class="form-label" for="technology">{{ __('messages.Technology') }}*</label>
                                                                                <span id="technology_error" class="field-error"></span>
                                                                            </div>
                                                                           
                                                                        </div>
                                                                        <div class="col-md-6">

                                                                            <div class="form-group">

                                                                                <input type="text" id="duration" name="duration" class="form-control" />
                                                                                <label class="form-label" for="duration">{{ __('messages.Duration') }}*</label>
                                                                                <span id="duration_error" class="field-error"></span>
                                                                            </div>
                                                                          
                                                                        </div>
                                                                    </div>
                                                                    <div class="row mb-12 md_space">
                                                                        <div class="col-md-6">

                                                                            <div class="form-group">

                                                                                <select class="form-control  select_form" id="status" name="status">
                                                                                    <option value=""></option>
                                                                                    <option value="In-Progress">{{ __('messages.In_Progress') }}</option>
                                                                                    <option value="Completed">{{ __('messages.Completed') }}</option>
                                                                                </select>
                                                                                <label class="form-label" for="status">Select Status*</label>
                                                                                <span id="status_error" class="field-error"></span>
                                                                            </div>
                                                                           
                                                                        </div>
                                                                    </div>
                                                                    <div id="projectTblRows">
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-success" id="btnAddProject">Save</button>
                                                        <button type="button" class="btn btn-secondary cancleBtnproject" id="btnCancel">Cancle</button> <!-- data-dismiss="modal" -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="modal fade" id="exampleModal3" tabindex="-1" role="dialog" aria-labelledby="EmployeeDetailsModel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content ipad_height">
                                                    <div class="modal-header">
                                                        <h5 class="bcolor">{{ __('messages.PerformancetDetails') }}</h5>
                                                        <button type="button" class="close BtnCanclePer" id="btnClosePerformanceModal" aria-label="Close" data-dismiss="modal">
                                                            <!-- data-dismiss="modal" -->
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">

                                                        <div class="card">
                                                            <div class="card-body form_div1">
                                                                <form name="performanceDetailsForm" id="performanceDetailsFormId">
                                                                    <div class="modal-body">
                                                                        <div class="row mb-12">
                                                                            <div class="col-sm-6">

                                                                                <div class="form-group">
                                                                                    <input type="hidden" name="id" id="performanceId">

                                                                                    <input type="text" id="year" name="year" class="form-control" />
                                                                                    <label class="form-label " for="year">{{ __('messages.Year') }}*</label>
                                                                                    <span id="year_error" class="field-error"></span>
                                                                                </div>
                                                                                
                                                                            </div>
                                                                            <div class="col-sm-6">

                                                                                <div class="form-group">

                                                                                    <select class="form-control select_form" id="grade" name="grade">
                                                                                        <option value=""></option>
                                                                                        <option value="Outstanding">{{ __('messages.Outstanding') }}</option>
                                                                                        <option value="Commendable">{{ __('messages.Commendable') }}</option>
                                                                                    </select>
                                                                                    <label class="form-label" for="grade">Select Grade*</label>
                                                                                    <span id="grade_error" class="field-error"></span>
                                                                                </div>
                                                                               
                                                                            </div>
                                                                        </div>
                                                                        <div class="row mb-12 md_space">
                                                                            <div class="col-sm-6">

                                                                                <div class="form-group">

                                                                                    <input type="text" id="comments" name="comments" class="form-control" />
                                                                                    <label class="form-label" for="comments">{{ __('messages.Comments') }}*</label>
                                                                                    <span id="comments_error" class="field-error"></span>
                                                                                </div>
                                                                                
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-success" id="btnAddPerformanceDetails">Save</button>
                                                        <button type="button" class="btn btn-secondary BtnCanclePer" id="btnCancelPerformanceDetails">Cancle</button> <!-- data-dismiss="modal" -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <!-- Tabs content -->
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary cancleBtnMain" id="btnCloseEmployeeModal" data-dismiss="modal">
                                        <i class="fas fa-times-circle"></i>&nbsp;{{ __('messages.Cancle') }}
                                    </button>
                                    <div id="saveButtonsDiv">
                                        <button type="button" class="btn btn-success btn_save" id="btnSaveAndClose"><i class="fas fa-check-circle"></i>&nbsp;{{ __('messages.Save_and_Close') }}</button>
                                        <button type="button" class="btn btn-success btn_save" id="btnSaveAndCreateNew"><i class="fas fa-plus-circle"></i>&nbsp;{{ __('messages.Save_and_New') }}</button>
                                    </div>
                                    <div id="updateButtonsDiv">
                                        <button type="button" class="btn btn-success btn_update" id="btnUpdateEmployee"><i class="fas fa-check-circle"></i>&nbsp;{{ __('messages.Update') }}</button>
                                    </div>
                                </div>
                            </div>
                            <!-- end of modal content -->
                        </div>
                        <!-- end of modal dilog -->
                    </div>
                    <!-- end of main modal -->
                </div>
                <!-- end of main div -->
                @stop

            </div>
            <!-- End container-fluid-->

        </div>
        <!--End content-wrapper-->

        <!--start color switcher-->
        <div class="right-sidebar">
            <div class="switcher-icon">
                <i class="zmdi zmdi-settings zmdi-hc-spin"></i>
            </div>
            <div class="right-sidebar-content">

                <p class="mb-0">Gaussion Texture</p>
                <hr>

                <ul class="switcher">
                    <li id="theme1"></li>
                    <li id="theme2"></li>
                    <li id="theme3"></li>
                    <li id="theme4"></li>
                    <li id="theme5"></li>
                    <li id="theme6"></li>
                </ul>

                <p class="mb-0">Gradient Background</p>
                <hr>

                <ul class="switcher">
                    <li id="theme7"></li>
                    <li id="theme8"></li>
                    <li id="theme9"></li>
                    <li id="theme10"></li>
                    <li id="theme11"></li>
                    <li id="theme12"></li>
                    <li id="theme13"></li>
                    <li id="theme14"></li>
                    <li id="theme15"></li>
                </ul>

            </div>
        </div>
        <!--end color switcher-->

    </div><!--  end of wrapper -->
</body>
@stop