<?php
namespace App\Http\Controllers;
use App;
class ERPFrameworkController extends Controller
{
    //
    protected $data;
    public function __construct()
    {
        $this->data['success_message'] = '';
        $this->data['error_message'] = '';
        $this->data['exception_message'] = '';
        $this->data['message'] = '';
    }
}
