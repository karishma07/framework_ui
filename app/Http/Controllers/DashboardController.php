<?php 
namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Exception;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\ERPFrameworkController;
use Illuminate\Support\Facades\View;
use Carbon\Carbon;
use App\Models\Employee;

class DashboardController extends ERPFrameworkController
{

    
    public function getAllSessionData() {
        $sessionVal = Session::all();
        Log::info(json_encode($sessionVal));
        return response()->json(json_decode(json_encode(Session::all())));
    }
    /**
     * This method is used to show dashboard
     */
    public function dashboard()
    {
        return View::make('dashboard.dashboard');

    }

    public function pass()
    {
        return View::make('dashboard.casual_visitor_pass');

    }


   
    
}