<?php 
namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Exception;
use App\Http\Controllers\ERPFrameworkController;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\View;
use Carbon\Carbon;


class LoginController extends ERPFrameworkController
{

    /**
     * This method is used to show login/Register Form
     */
    public function index()
    {
        return View::make('login.login');

    }

    /**
     * This method is used to login the registered user
     */
    public function login(Request $request)  {
        try {
           //$payload = $request->all();
           $username = $request->input("username");
           $password = $request->input("password");
          
           $headers = array('Accept' => 'application/json', 'Content-Type' => 'application/json');
           //$dataArr = array("username" => $username, "password" => $password);

           $response = callAPI("POST", "/api/auth/login", 
               array("username" => $username, "password" => $password), $headers);
            //$response = postWithJsonBody("/api/login", $dataArr, $headers);
            
                    
            Log::info("response==>:  ". json_encode($response));
            if ($response->getStatusCode() == 200) {
              
                Log::info("Session:  ". json_encode(Session::all()));
                Log:info("Body:: " . json_encode($response->getData()));
                $body = $response->getData();
                Log::info('userID'. Session(['loggedInUserId' =>  $body->data->user->id]));
              
                return response()->json(array(
                    "success" => true,
                    "message" => "Login successfully!",
                    "data" => $body->data->user,
                    "access_token" => $body->data->access_token,
                    "token_type" => $body->data->token_type,
                ), 200);
              
               
                //return redirect("/dashboard");
            } else {
                return response()->json(array(
                    "success" => false,
                     "message" => $response,
                ), 400);
              
            }

        } catch (\Exception $e) {
			Log::info("Exception: " . $e->getMessage());
			Log::error($e);
			return response()->json(
				array(
					"message" => "Something went wrong while login!",
					"exception" => $e->getMessage(),
				),
				500,
				array("Content-Type" => "application/json", "Accept" => "application/json")
			);
        }
       
    }

      
    /**
     * This method is used to close the cusrrent user session
     */
    public function logout() {
        Log::info("Logout successfully!");
        Session::flush();
        return redirect('/');
    }

    public function signup()
    {
        return View::make('signup.signup', $this->data);

    }

    public function getOTP(Request $request) {
        try{
            $username = $request->input('userName');
            $password = $request->input('password');
            $mobile = $request->input("mobile");

            $headers = array('Accept' => 'application/json', 'Content-Type' => 'application/json');
            $roleId = 1;
            $response = callAPI("POST","/api/auth/otp", array("mobile" => $mobile), $headers);

            if ($response->getStatusCode() == 200) {
                $body = $response->getData();
                return response()->json(array(
                    "success" => true,
                    "message" => "OTP sent successfully!",
                    "key" => $body->data->otp
                ), 200);
            } else {
                return response()->json(array(
                    "success" => false,
                    "message" => $response,
                ), 400);
            }

        } catch (\Exception $e) {
            Log::info("Exception: " . $e->getMessage());
            Log::error($e);
            return response()->json(
                array(
                    "message" => "Something went wrong while sending OTP!",
                    "exception" => $e->getMessage(),
                ), 500, array("Content-Type" => "application/json", "Accept" => "application/json")
            );
        }
    }

    public function register(Request $request){
        try{
            $username = $request->input('userName');
            $password = $request->input('password');
            $mobile = $request->input("mobile");

            $headers = array('Accept' => 'application/json', 'Content-Type' => 'application/json');
            $roleId = 1;
            $response = callAPI("POST","/api/auth/register", 
                array("username" => $username, "password" => $password, "mobile" => $mobile,
                 "roleId" => $roleId), $headers);

                if ($response->getStatusCode() == 200) {
                    $body = $response->getData();
                    return response()->json(array(
                        "success" => true,
                        "message" => "Registered successfully!",
                        "data" => $body->data->user,
                        //"key" => $body->data->otp
                    ), 200);
                   
               
                } else {
                    return response()->json(array(
                        "success" => false,
                        "message" => $response,
                    ), 400);
                
                }

        } catch (\Exception $e) {
			Log::info("Exception: " . $e->getMessage());
			Log::error($e);
			return response()->json(
				array(
					"message" => "Something went wrong while register!",
					"exception" => $e->getMessage(),
				),
				500,
				array("Content-Type" => "application/json", "Accept" => "application/json")
			);
        }
    }
}
