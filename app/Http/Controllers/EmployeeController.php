<?php 
namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Exception;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\ERPFrameworkController;
use Illuminate\Support\Facades\View;
use Carbon\Carbon;
use App\DAppModel;
use App\Models\Employee;

class EmployeeController extends ERPFrameworkController
{
    public function saveEmployee(Request $request)  {
        try {
            $employeeData = $request->all();
            $headers = array('Accept' => 'application/json', 'Content-Type' => 'application/json');
            $roleId = 1;
            $response = callAPI("POST","/api/auth/save/employee",$employeeData, $headers);
            Log::info("response==>".json_encode($response));
           
            if ($response->getStatusCode() == 200) {
                $body = $response->getData();
                Log::info("res: " . json_encode($body));
                return response()->json(array(
                    "success" => true,
                    "message" => "Employe Record Created Successfully!",
                    "projectDetails" => $body->data->projectDetails,
                    "performanceDetails" => $body->data->performanceDetails
                ), 200);
            } else {
                return response()->json(array(
                    "success" => true,
                    "message" => $response->body->message
                ), 400);
            }
        } catch (Exception $e) {
            Log::error($e);
            return response()->json(array(
                "message" => "Something went wrong while creating employee record!",
                "exception" => $e->getMessage()
            ), 500);
        }
       
    }
    public function updateEmployee(Request $request, $empId) {
        try {
            $headers = array('Accept' => 'application/json', 'Content-Type' => 'application/json');
            $response = callAPI("POST","/api/auth/update/employee/" . $empId, $request->all(),$headers);
            
            if ($response->getStatusCode() == 200) {
                $body = $response->getData();
                Log::info("res: " . json_encode($body));
                return response()->json(array(
                    "success" => true,
                    "message" => "Employe Record Updated Successfully!",
                    "employee" => $body->data,
                    "projectDetails" => $body->data->projectDetails,
                    "performanceDetails" => $body->data->performanceDetails
                ), 200);
            }
        } catch(\Exception $e) {
            Log::info("Something went wrong while updating contract!");
            Log::error($e);
            return response()->json(array('message' => "Something went wrong while updating contract", 
                "exception" => $e->getMessage()), 500);
        }
    }

    public function fetchEmployeeDetails(Request $request, $empId) {
        try {
            $headers = array('Accept' => 'application/json', 'Content-Type' => 'application/json');
            $response = callAPI("GET","/api/auth/employee/" . $empId,'',$headers);
            Log::info("response==>".json_encode($response));
            if($response->getStatusCode() == 200) {
                $body = $response->getData()->data;
                return response()->json(array(
                    "success" => true,
                    "message" => "Employe Record Return Successfully!",
                    "data" => $body->data
                ), 200);
            }
        } catch (\Exception $e) {
            Log::info("Something went wrong! Exception: " . $e->getMessage());
            Log::error($e);
            return response()->json(array(
                "message" => "Something went wrong!",
                "exception" => $e->getMessage()
            ), 500);
        }
        
    }
    

    public function deleteEmployee(Request $request, $empId) {
        try {
            $headers = array('Accept' => 'application/json', 'Content-Type' => 'application/json');
            $response = callAPI("DELETE","/api/auth/delete/employee/" . $empId,'',$headers);
            Log::info("response==>".json_encode($response));
            if($response->getStatusCode() == 200){
                $body = $response->getData();
                Log::info("res: " . json_encode($body));
                return response()->json(array(
                    "success" => true,
                    "message" => "Employe Record Deleted Successfully!",
                    "is_deleted" => $body->data
                   
                ), 200);
            }
        } catch (\Exception $e) {
            Log::info("Something went wrong! Exception: " . $e->getMessage());
            Log::error($e);
            return response()->json(array(
                "message" => "Something went wrong!",
                "exception" => $e->getMessage()
            ), 500);
        }
        
    }

    /**
     * This method is used to show employee
     */
    public function index(Request $request)
    {
        // try {
        //     $headers = array('Accept' => 'application/json', 'Content-Type' => 'application/json');
        //     $response = callAPI("GET","/api/auth/all/employee/",'',$headers);
        //     //$response = callAPI("GET","/api/auth/all/employee/",'',$headers);
        //     Log::info("response==>".json_encode($response));
        //     if($response->getStatusCode() == 200){
        //         $body = $response->getData();
        //         Log::info("res: " . json_encode($body));
        //         $this->data['employeeArray'] = $body->data->employee;
        //         Log::info("employeArr====>". json_encode($this->data['employeeArray']));
        //         //return View::make('dashboard.dashboard',$this->data);
        //     }else{
        //         $this->data['employeeArray'] = array();
        //     }
        //     $response1 = callAPI("GET","/api/auth/get-projects/",'',$headers);
        //     if($response1->getStatusCode() == 200){
        //         $body = $response1->getData();
        //         Log::info("res: " . json_encode($body));
        //         $this->data['projectArray'] = $body->data->project;
        //         Log::info("projectArray====>". json_encode($this->data['projectArray']));
               
        //     }else{
        //         $this->data['projectArray'] = array();
        //     }
        //     return View::make('employee.employee',$this->data);
        // } catch (\Exception $e) {
        //     Log::info("Something went wrong! Exception: " . $e->getMessage());
        //     Log::error($e);
        //     $this->data['employeeArray'] = array();
        //     return View::make('employee.employee',$this->data);
        // }
        return View::make('employee.employee');
    }
    public function EditEmployee($id){
        return View::make('employee.employee_edit');
    }
   

    public function getAllSessionData() {
        $sessionVal = Session::all();
        Log::info(json_encode($sessionVal));
        return response()->json(json_decode(json_encode(Session::all())));
    }
    

    /**
     * This method is used to show import page
     */
    public function import()
    {
        return View::make('employee.import');

    }
    public function uploadFile(Request $request){

        if ($request->input('submit') != null ){
    
          $file = $request->file('file');
    
          // File Details 
          $filename = $file->getClientOriginalName();
          $extension = $file->getClientOriginalExtension();
          $tempPath = $file->getRealPath();
          $fileSize = $file->getSize();
          $mimeType = $file->getMimeType();
    
          // Valid File Extensions
          $valid_extension = array("csv");
    
          // 2MB in Bytes
          $maxFileSize = 2097152; 
    
          // Check file extension
          if(in_array(strtolower($extension),$valid_extension)){
    
            // Check file size
            if($fileSize <= $maxFileSize){
    
              // File upload location
              $location = 'uploads';
    
              // Upload file
              $file->move($location,$filename);
    
              // Import CSV to Database
              $filepath = public_path($location."/".$filename);
    
              // Reading file
              $file = fopen($filepath,"r");
    
              $importData_arr = array();
              $i = 0;
    
              while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
                 $num = count($filedata );
                 
                 // Skip first row (Remove below comment if you want to skip the first row)
                 /*if($i == 0){
                    $i++;
                    continue; 
                 }*/
                 for ($c=0; $c < $num; $c++) {
                    $importData_arr[$i][] = $filedata [$c];
                 }
                 $i++;
              }
              fclose($file);
    
              // Insert to MySQL database
              foreach($importData_arr as $importData){
    
                $insertData = array(
                   "name"=>$importData[1],
                   "designation"=>$importData[2],
                   "experience"=>$importData[3],
                   "contact"=>$importData[4]);
                   Employee::insertData($insertData);
    
              }
              return redirect('/dashboard');
              Session::flash('message','Import Successful.');
            }else{
              Session::flash('message','File too large. File must be less than 2MB.');
            }
    
          }else{
             Session::flash('message','Invalid File Extension.');
          }
    
        }
    
        // Redirect to index
        return View::make('employee.import');
        // return redirect()->action('DashboardController@import');
      }
    
     /**
     * This method is used to show upload page
     */
    public function upload()
    {
        return View::make('employee.upload');

    }
        /**
     * This method is used to show upload document
     */
    public function uploadDocument()
    {
        return View::make('employee.upload_document');

    }

    /**
     * This method is used to show upload image
     */
    public function uploadImage()
    {
        return View::make('employee.upload_image');

    }
   
  
    
    
}
