<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class ResetPasswordController extends Controller
{
    public function index($id)
    {
        return View::make('reset_password.reset_password');

    }
}
