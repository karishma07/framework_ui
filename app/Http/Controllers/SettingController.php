<?php 
namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Exception;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\ERPFrameworkController;
use Illuminate\Support\Facades\View;
use Carbon\Carbon;

class SettingController extends ERPFrameworkController{
       /**
     * This method is used to show dashboard
     */
    public function setting()
    {
        return View::make('setting.setting');

    }
    
    
}
