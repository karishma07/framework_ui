<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Exception;
use App\Http\Controllers\ERPFrameworkController;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\View;
use Carbon\Carbon;;


class ForgetpasswordController extends Controller
{
    public function index()
    {
        return View::make('forget_password1.forget_password');

    }

  /*
*This function used for call api for sending reset password link on user email id
*/
    public function sendResetLink(Request $request)  {
        try {
           //$payload = $request->all();
           $username = $request->input("username");        
           $headers = array('Accept' => 'application/json', 'Content-Type' => 'application/json');
           //$dataArr = array("username" => $username, "password" => $password);

           $response = callAPI("POST", "/sendResetLink/send", 
               array("username" => $username), $headers);
            //$response = postWithJsonBody("/api/login", $dataArr, $headers);
            
                    
            Log::info("response==>:  ". json_encode($response));
            if ($response->getStatusCode() == 200) {
                Log::info("Session:  ". json_encode(Session::all()));
                Log:info("Body:: " . json_encode($response->getData()));
                $body = $response->getData();
                return response()->json(array(
                    "success" => true,
                    "message" => "Link sent  successfully!",
                ), 200);
                //return redirect("/dashboard");
            } else {
                return response()->json(array(
                    "success" => false,
                     "message" => $response,
                ), 400);
              
            }

        } catch (\Exception $e) {
			Log::info("Exception: " . $e->getMessage());
			Log::error($e);
			return response()->json(
				array(
					"message" => "Something went wrong while sending  reset link!",
					"exception" => $e->getMessage(),
				),
				500,
				array("Content-Type" => "application/json", "Accept" => "application/json")
			);
        }
       
    }
/*
*This function used for reset password with encrypted unique user id
*/
    public function resetForgotPassword(Request $request,$uniqueURLIdentifier) {
        try {
           //$payload = $request->all();
           $newPassword = $request->input("newPassword");        
           $headers = array('Accept' => 'application/json', 'Content-Type' => 'application/json');
           //$dataArr = array("username" => $username, "password" => $password);

           $response = callAPI("POST", "/resetForgotPassword/". $uniqueURLIdentifier, 
               array("newPassword" => $newPassword), $headers);
            //$response = postWithJsonBody("/api/login", $dataArr, $headers);
            
                    
            Log::info("response==>:  ". json_encode($response));
            if ($response->getStatusCode() == 200) {
                Log::info("Session:  ". json_encode(Session::all()));
                Log:info("Body:: " . json_encode($response->getData()));
                $body = $response->getData();
                return response()->json(array(
                    "success" => true,
                    "message" => "Password reset successfully!",
                ), 200);
                //return redirect("/dashboard");
            } else {
                return response()->json(array(
                    "success" => false,
                     "message" => $response,
                ), 400);
              
            }

        } catch (\Exception $e) {
			Log::info("Exception: " . $e->getMessage());
			Log::error($e);
			return response()->json(
				array(
					"message" => "Something went wrong while updating password!",
					"exception" => $e->getMessage(),
				),
				500,
				array("Content-Type" => "application/json", "Accept" => "application/json")
			);
        }
       
    }

}
