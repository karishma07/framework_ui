<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Exception;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\ERPFrameworkController;
use Illuminate\Support\Facades\View;
use Carbon\Carbon;

class GenericController extends ERPFrameworkController {

    public function __construct() {
        parent::__construct();
    }

    public function executeAPI( Request $request ) {

        // Get request method
        $method = $request->method();

        // Get the endpoint from the url
        $ep = str_replace( 'api/r', '/api', $request->path() );

        // Get the url without query string
        $url = $request->url();

        // Get the full url with query string
        $fullPath = $request->fullUrl();
        //$query = $request->query();

        // Create a query string
        $qryStr = str_replace( $url, '', $fullPath );

        // Get bearer token
        $token = $request->bearerToken();

        // Initialize response variable
        $response = null;

        Log::info("Method Name: " . $request->method());

        $apiEp = $ep . $qryStr;

        $headers = array(
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $token
        );

        return callAPI($request->method(), $apiEp, $request->all(), $headers);

        /*if ( $request->isMethod( 'post' ) ) {
            Log::info( 'Executing POST method' );
            $apiEp = $ep . $qryStr;
            $response = postWithJsonBody( $apiEp, $request->all(), $headers = array(
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer ' . $token
            ) );
        } else if ( $request->isMethod( 'get' ) ) {
            Log::info( 'Executing GET method' );
            $apiEp = $ep . $qryStr;
            $response = callGetAPI( $apiEp, $request->all(), $headers = array(
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer ' . $token
            ) );
        } else if ( $request->isMethod( 'put' ) ) {
            Log::info( 'Executing PUT method' );
            $apiEp = $ep . $qryStr;
            $response = putWithJsonBody( $apiEp, $request->all(), $headers = array(
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer ' . $token
            ) );
        } else if ( $request->isMethod( 'delete' ) ) {
            Log::info( 'Executing DELETE method' );
            $apiEp = $ep . $qryStr;
            $response = callDeleteAPI( $apiEp, $request->all(), $headers = array(
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer ' . $token
            ) );
        }

        if ( $response != null ) {
            if ( $response->code >= 200 && $response->code <= 299 ) {
                return response()->json(
                    array(
                        'success' => true,
                        'message' => isset( $response->body->message ) ? $response->body->message : 'Request processed successfully!',
                        'data' => $response->body
                    ), 200 );
            } else {
                return response()->json(
                    array(
                        'success' => false,
                        'message' => isset($response->body->message) ? $response->body->message : 'Request processed successfully!'
                    ),
                    $response->code
                );
            }
        }*/
    }
}
