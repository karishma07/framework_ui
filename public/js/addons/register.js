$(document).ready(function () {
    var loggedInUserID = localStorage.getItem("loggedInUserId");
    //console.log("UserID==>" + loggedInUserID);
    
    $(".otp_div").hide();
    $(".otp_verify").hide();
    $("#btnRegister").click(function () {
          
    $(".otp_div").show();
    $(".otp_verify").show();
    $("#btnRegister").hide();
        var username = $("#userName").val();
        var password = $("#password").val();
        var mobile = $("#mobile").val();
        var roleId = 1;
        var reqData = {
            userName: username,
            password: password,
            mobile: mobile,
            roleId: roleId,
        };
        $.ajax({
            url: `${baseURL}/api/register`,
            method: "POST",
            data: reqData,
            type: "json",
            success: function (response, textStatus, jqXHR) {
                console.log("response==>" + JSON.stringify(response));
                if (response != null) {
                    if (jqXHR.status == 200) {
                        window.location.href = `${baseURL}`;
                    }
                }
            },
            error: function (xhr, textStatus, err) { 
                console.log("xhr: ", xhr);
                console.log("Error while fetching the preview data: ", err);
                if (xhr.status == 404) {
                    Swal.fire({
                        title: "Error!",
                        text: "Please check API URL!",
                        icon: "error",
                        confirmButtonText: "OK",
                    });
                }
            },
        });
    });

    $("#userName").keyup(function () {
        var username = $("#userName").val();
        const userNamePattern = /^[A-Za-z]+.*$/;
        this.userNameValidationMsg = "";
        if (username.length == 0) {
            //this.userNameValidationMsg = "UserName is required !";
            $("#username-validation").html("UserName is required");
            this.isUserNameValid = false;
        } else {
            if (userNamePattern.test(username)) {
                $("#username-validation").html("");
                this.isUserNameValid = true;
            } else {
                this.isUserNameValid = false;
                //this.userNameValidationMsg = "UserName should start with Alphabets !";
                $("#username-validation").html("UserName should start with Alphabets !");
            }
        }
    });

    // $("#password").keyup(function () {
    //     const passwordPattern = /^(?=.*[A-Z])(?=.*[a-z])(?=.*\d)(?=.*[^A-Za-z0-9]).{8,}$/;
    //     var confirmPassword = $("#confirmpass").val();
    //     var password = $("#password").val();

    //     if (password.length == 0) {
    //         this.passwordValidationMsg = "Password is required !";
    //         $("#password-validation").html("Password is required !");
    //     } else {
    //         if (passwordPattern.test(password)) {
    //             this.isPasswordValid = true;
    //         } else {
    //             $("#password-validation").html("Password is not valid !");
    //         }
    //     }
    //     if (confirmPassword != null && confirmPassword.length > 0) {
    //         this.validateConfirmPassword();
    //     }
    // });
    $("#mobile").change(function (event) {
        let val = event.target.value;
        console.log("val==>", val);
        let mobile = $("#mobile").val();
        const mobilePattern = /^[0-9]+$/;
        if (!mobilePattern.test(val)) {
            mobile = mobile.substring(0, mobile.length - 1);
            event.target.value = mobile;
        }

        if (mobile.length == 0) {
            $("#mobile-validation").html("Mobile number is required !");
        } else {
            if (mobilePattern.test(mobile)) {
                $("#mobile-validation").html("");
                return true;
            } else {
                $("#mobile-validation").html("Mobile number is required !");
                return false;
            }
        }
    });
});

/* validateMobile(event) : void {
        let val = event.target.value;
        const mobilePattern = /^[0-9]+$/;
        if(!mobilePattern.test(val)){
          this.mobile = this.mobile.substring(0,(this.mobile.length-1));
          event.target.value = this.mobile;
        } 
        this.mobileValidationMsg = '';
        if(this.mobile.length == 0){
          this.mobileValidationMsg = "Mobile number is required !";
          this.isMobileValid = false;
        }
        else{
          if(mobilePattern.test(this.mobile)){
            this.isMobileValid = true;
          }else{
            this.isMobileValid = false;
            this.mobileValidationMsg = 'Mobile number is not valid !';
          }
        }
      }*/

function validateConfirmPassword() {
    var password = $("#password").val();
    var confirmPassword = $("#confirmPassword").val();
    ///this.confirmPasswordValidationMsg = '';
    if (password == confirmPassword) {
        $("#confirmPass-validation").html("");
        return true;
    } else {
        $("#confirmPass-validation").html("Password does not match !");
        return false;
    }
}
