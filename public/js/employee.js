$(document).ready(function() {
    //for label active

    $(".form-group .form-control").blur(function() {
        if ($(this).val() != "") {
            $(this).siblings(".form-label").addClass("active");
        } else {
            $(this).siblings(".form-label").removeClass("active");
        }
    });


    //emplopyee table
    $("#exportTable").click(function(e) {
        $(this).attr({
            'download': "Employee_Data.xls",
            'href': 'data:application/csv;charset=utf-8,' + encodeURIComponent($('#dvData').html())
        })

    });
    //project table
    $("#exportTableProject").click(function(e) {
        $(this).attr({
            'download': "EmployeeProject_Data.xls",
            'href': 'data:application/csv;charset=utf-8,' + encodeURIComponent($('#dvDataProject').html())
        })

    });
    //performance table
    $("#exportTablePerformance").click(function(e) {
        $(this).attr({
            'download': "EmployeePerformance_Data.xls",
            'href': 'data:application/csv;charset=utf-8,' + encodeURIComponent($('#dvDataPerformance').html())
        })

    });

    $("#table").DataTable({
        ajax: "",
        columnDefs: [{
            targets: 0,
            searchable: true,
            orderable: false,
            width: "1%",
            className: "dt-body-center",
            render: function(data, type, full, meta) {
                return '<input type="checkbox">';
            },
        }, ],
        order: [1, "asc"],
        rowCallback: function(row, data, dataIndex) {
            // Get row ID
            var rowId = data[0];
            // If row ID is in the list of selected row IDs
            /*if ($.inArray(rowId, rows_selected) !== -1) {
                $(row).find('input[type="checkbox"]').prop("checked", true);
                $(row).addClass("selected");
            }*/
        },
    });

    createProjectDetailsDatatable();
    createPerformanceDetailsDatatable();

    $("#exampleModal").modal("hide");
    $("#exampleModal2").modal("hide");
    $("#exampleModal3").modal("hide");
    $("#btnShowAddProjectModal").click(function() {
        $("#exampleModal2").modal("show");
    });
    $("#btnShowEmployeeModal").click(function() {
        $("#updateButtonsDiv").hide();
        $("#exampleModal").modal("show");
    });
    $("#btnCloseEmployeeModal").click(function() {
        resetEmployeeForm();
        resetProjectDetailsForm();
        resetPerformanceDetailsForm();
        resetAllDataTables();

        $("#saveButtonsDiv").show();
        $("#updateButtonsDiv").hide();
        $("#exampleModal").modal("hide");
    });
    $("#btnCancel").click(function() {
        $("#exampleModal2").modal("hide");
        resetProjectDetailsForm();
        $("#main").addClass("modal-open");
    });
    $("#btnCloseProjectDtl").click(function() {
        $("#exampleModal2").modal("hide");
        resetProjectDetailsForm();
        $("#main").addClass("modal-open");
    });
    $("#btnAddProject").click(function() {
        if (
            isValidForm("addProjectFormId", validatorObjects["addProjectForm"])
        ) {
            let idx = 0;
            projectDetailsTable.destroy(false);
            if (!isProjectEdit) {
                idx = projectRows;
                $("#projectDetailsTb tbody").append(`
                    <tr id="projectRow${idx}">
                        <td><input type='checkbox'></td>
                        <td>${$("#projectCode option:selected").text()}</td>
                        <td>${$("#projectName option:selected").text()}</td>
                        <td>${$("#technology").val()}</td>
                        <td>${$("#duration").val()}</td>
                        <td>${$("#status").val()}</td>
                        <td><button type="button" class="btn btn-primary btnpencil editEmp btnEditProject" id="btnEditProject${idx}" record-id="" projectRowId="${idx}"><i class="fas fa-pen"></i></button></td>
                        <td><button id="button" class="btn btn-primary btntrasht btnRemoveProject" data-toggle="tooltip" title="Delete Project" project-id="" projectRowId="${idx}"><i class="fas fa-trash"></i></button></td>
                    </tr>
                `);

                let objectAdded = { rowIndex: idx };
                $("#addProjectFormId")
                    .serializeArray()
                    .forEach((p) => {
                        objectAdded[p.name] = p.value;
                    });

                if (!employeeDetails.projectDetails) {
                    employeeDetails.projectDetails = [];
                }
                if (employeeDetails.projectDetails) {
                    employeeDetails.projectDetails.push(objectAdded);
                }
                editProject();
                deleteProject();
                createProjectDetailsDatatable();
                //resetProjectDetailsForm();
            } else {
                idx = selectedProjectRow;
                let objectEdited = {};
                $("#addProjectFormId")
                    .serializeArray()
                    .forEach((p) => {
                        objectEdited[p.name] = p.value;
                    });

                let pdetails = [...employeeDetails.projectDetails].map((pd) => {
                    if (pd.id == objectEdited.id) {
                        return {...pd, ...objectEdited };
                    } else {
                        return pd;
                    }
                });
                employeeDetails.projectDetails = pdetails;

                $(`#projectRow${selectedProjectRow}`).html("");
                $(`#projectRow${selectedProjectRow}`).append(`
                    <td><input type='checkbox'></td>
                    <td>${$("#projectCode option:selected").text()}</td>
                    <td>${$("#projectName option:selected").text()}</td>
                    <td>${$("#technology").val()}</td>
                    <td>${$("#duration").val()}</td>
                    <td>${$("#status").val()}</td>
                    <td><button type="button" class="btn btn-primary btnpencil editEmp btnEditProject" id="btnEditProject${idx}" record-id="${$(
                    "#projectId"
                ).val()}" projectRowId="${idx}"><i class="fas fa-pen"></i></button></td>
                    <td><button id="button" class="btn btn-primary btntrasht btnRemoveProject" data-toggle="tooltip" title="Delete Project" projectRowId="${idx}"><i class="fas fa-trash"></i></button></td>
                `);
                editProject();
                $(`epdDiv${idx}`).remove();
                isProjectEdit = false;
            }

            $(`#employeeProjectDetails`).append(`
                    <div id='epdDiv${idx}'>
                        <input type='hidden' name='project_code[]' id='projectCode${idx}' obj-key="projectDetails" value='${$(
                "#projectCode"
            ).val()}'>
                            <input type='hidden' name='project_name[]' id='projectName${idx}' obj-key="projectDetails" value='${$(
                "#projectName"
            ).val()}'>
                            <input type='hidden' name='technology[]' id='technology${idx}' obj-key="projectDetails" value='${$(
                "#technology"
            ).val()}'>
                            <input type='hidden' name='duration[]' id='duration${idx}' obj-key="projectDetails" value='${$(
                "#duration"
            ).val()}'>
                            <input type='hidden' name='status[]' id='status${idx}' obj-key="projectDetails" value='${$(
                "#status"
            ).val()}'>
                    </div>
                `);

            $("#exampleModal2").modal("hide");
            if (!isEdit) {
                projectRows++;
            }
            deleteProject();
            createProjectDetailsDatatable();
            resetProjectDetailsForm();
        }
    });

    $("#btnShowPerformanceModal").click(function() {
        debugger;
        $("#exampleModal3").modal("show");
    });
    $("#btnAddPerformanceDetails").click(function() {
        if (
            isValidForm(
                "performanceDetailsFormId",
                validatorObjects["performanceDetailsForm"]
            )
        ) {
            let idx = 0;
            performanceDetailsTable.destroy(false);
            if (!isPerformanceEdit) {
                idx = performanceDetailsRows;
                $("#performanaceDetailsTb tbody").append(`
                    <tr id="performanceRow${idx}">
                        <td><input type='checkbox'></td>
                        <td>${$("#year").val()}</td>
                        <td>${$("#grade").val()}</td>
                        <td>${$("#comments").val()}</td>
                        <td><button type="button" class="btn btn-primary btnpencil performanceEdit btnEditPerformance" data-toggle="modal" performanceRowId="${idx}" performance-id=""><i class="fas fa-pen"></i></button></td>
                        <td><button id="btnRemovePerformance${idx}" class="btn btn-primary btntrasht btnRemovePerformance" performanceRowId="${idx}" performance-id=""><i class="fas fa-trash"></i></button></td>
                    </tr>
                `);

                let objectAdded = { rowIndex: idx };
                $("#performanceDetailsFormId")
                    .serializeArray()
                    .forEach((p) => {
                        objectAdded[p.name] = p.value;
                    });
                if (!employeeDetails.performanceDetails) {
                    employeeDetails.performanceDetails = [];
                }
                if (employeeDetails.performanceDetails) {
                    employeeDetails.performanceDetails.push(objectAdded);
                }
                console.log(
                    "employeeDetails.performanceDetails: ",
                    employeeDetails.performanceDetails
                );
                editPerformance();
                deletePerformanceRecord();
                //resetPerformanceDetailsForm();
                createPerformanceDetailsDatatable();
            } else {
                idx = selectedPerformanceRow;

                let objectEdited = {};
                $("#performanceDetailsFormId")
                    .serializeArray()
                    .forEach((p) => {
                        objectEdited[p.name] = p.value;
                    });

                let pdetails = [...employeeDetails.performanceDetails].map(
                    (pd) => {
                        if (pd.id == objectEdited.id) {
                            return {...pd, ...objectEdited };
                        } else {
                            return pd;
                        }
                    }
                );

                employeeDetails.performanceDetails = pdetails;

                $(`#performanceRow${idx}`).html("");
                $(`#performanceRow${idx}`).append(`
                    <td><input type='checkbox'></td>
                    <td>${$("#year").val()}</td>
                    <td>${$("#grade").val()}</td>
                    <td>${$("#comments").val()}</td>
                    <td><button type="file" class="btn btn-primary btnpencil performanceEdit btnEditPerformance" performanceRowId="${idx}" id="btnEditPerformance${idx}" record-id="${$(
                    "#performanceId"
                ).val()}"><i class="fas fa-pen"></i></button></td>
                    <td><button id="btnRemovePerformance${idx}" class="btn btn-primary btntrasht btnRemovePerformance" performance-id="${$(
                    "#performanceId"
                ).val()}" performanceRowId="${idx}"><i class="fas fa-trash"></i></button></td>
                `);
                editPerformance();
                $(`epdtl${idx}`).remove();
                isPerformanceEdit = false;
            }

            $("#employeePerformanceDetails").append(`
                    <div id="epdtl${idx}">
                        <input type='hidden' name='year[]' id='performanceYear${idx}' obj-key='performanceDetails' value='${$(
                "#year"
            ).val()}'>
                        <input type='hidden' name='grade[]' id='performanceGrade${idx}' obj-key='performanceDetails' value='${$(
                "#grade"
            ).val()}'>
                        <input type='hidden' name='comments[]' id='performanceComments${idx}' obj-key='performanceDetails' value='${$(
                "#comments"
            ).val()}'>
                    </div>
                `);

            $("#exampleModal3").modal("hide");
            createPerformanceDetailsDatatable();
            resetPerformanceDetailsForm();
            deletePerformanceRecord();
            if (!isEdit) {
                performanceDetailsRows++;
            }
        }
    });
    $("#btnCancelPerformanceDetails").click(function() {
        $("#exampleModal3").modal("hide");
        resetPerformanceDetailsForm();
        $("#main").addClass('modal-open');
    });
    $("#btnClosePerformanceModal").click(function() {
        $("#exampleModal3").modal("hide");
        resetPerformanceDetailsForm();
        $("#main").addClass('modal-open');
    });

    $("#btnSaveAndClose").click(function() {
        if (isValidForm("employeeFormID", validatorObjects["employeeForm"])) {
            Swal.fire({
                title: "Are you sure?",
                text: "You want to submit the employee details?",
                icon: "question",
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "Yes",
            }).then((result) => {
                console.log("result====>", result);
                if (result.isConfirmed) {
                    let formInputs = prepareDatasetFromForm("employeeFormID");
                    console.log("formInputs: ", formInputs);
                    saveEmployeeDetails(formInputs);
                    $("#exampleModal").modal("hide");
                    window.location = baseURL + '/dashboard';
                    location.reload();
                }
            });
        }
    });
    $("#btnSaveAndCreateNew").click(function() {
        if (isValidForm("employeeFormID", validatorObjects["employeeForm"])) {
            Swal.fire({
                title: "Are you sure?",
                text: "You want to submit the employee details?",
                icon: "question",
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "Yes",
            }).then((result) => {
                if (result.isConfirmed) {
                    let formInputs = prepareDatasetFromForm("employeeFormID");
                    console.log("formInputs: ", formInputs);
                    saveEmployeeDetails(formInputs);
                }
            });
        }
    });

    $("#btnUpdateEmployee").click(function() {
        if (isValidForm("employeeFormID", validatorObjects["employeeForm"])) {
            Swal.fire({
                title: "Are you sure?",
                text: "You want to update the employee details?",
                icon: "question",
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "Yes",
            }).then((result) => {
                if (result.isConfirmed) {
                    let empInputs = $("#employeeFormID").serializeArray();

                    let mainObj = {};
                    empInputs.forEach((obj) => {
                        if (!obj.name.match(/\[[^\]]*]/g)) {
                            mainObj[obj.name] = obj.value;
                        }
                    });

                    let formInputs = {...employeeDetails, ...mainObj };
                    console.log("formInputs: ", formInputs);
                    updateEmployeeDetails(formInputs);

                    resetEmployeeForm();
                    resetProjectDetailsForm();
                    resetPerformanceDetailsForm();
                    resetAllDataTables();

                    $("#saveButtonsDiv").show();
                    $("#updateButtonsDiv").hide();
                    $("#exampleModal").modal("hide");
                    window.location = baseURL + '/dashboard';
                    location.reload()
                }
            });
        }
    });

    $("form").each(function() {
        let inputs = $(this).find(":input");
        let formName = $(this).attr("name");
        Object.keys(inputs).forEach((key) => {
            let el = inputs[key];
            $(`#${el.id}`).blur(function() {
                validateField($(this), validatorObjects[formName]);
            });
        });
    });

    $(".btnRemoveEmployee").click(function() {
        let employeeId = $(this).attr("employee-id");

        Swal.fire({
            title: "Are you sure?",
            text: "You want to delete the employee?",
            icon: "success",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Yes",
        }).then((result) => {
            if (result.isConfirmed) {
                dAuthAxios
                    .delete(`/delete/employee/${employeeId}`)
                    .then((response) => {
                        Swal.fire({
                            title: "Done!",
                            text: "Employee record deleted successfully!",
                            icon: "success",
                            showCancelButton: false,
                            confirmButtonColor: "#3085d6",
                            cancelButtonColor: "#d33",
                            confirmButtonText: "Ok",
                        }).then((result) => {
                            window.location = "/dashboard";
                        });
                    });
            }
        });
    });

    /* -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= */

    $(".btnEdit").click(function() {
        isEdit = true;
        let recordId = $(this).attr("record-id");

        if (!recordId) {
            Swal.fire("Alert!", "Unable to fetch record!", "warning");
        } else {
            dAuthAxios
                .get(`/employee/${recordId}`)
                .then((response) => {
                    employeeDetails = response.data.data;
                    let employeeFormInputEls = $("#employeeFormID").find(
                        ":input"
                    );
                    Object.keys(employeeFormInputEls).forEach((key) => {
                        let el = employeeFormInputEls[key];
                        el.value = employeeDetails[el.name];
                    });
                    projectDetailsTable.destroy(false);
                    projectRows = employeeDetails.projectDetails.length;
                    employeeDetails.projectDetails.forEach((project, index) => {
                        $("#projectDetailsTb tbody").append(`
                            <tr id="projectRow${index}">
                                <td><input type="checkbox"></td>
                                <td>${project.project_code}</td>
                                <td>${project.project_name}</td>
                                <td>${project.technology}</td>
                                <td>${project.duration}</td>
                                <td>${project.status}</td>
                                <td><button type="button" class="btn btn-primary btnpencil editEmp btnEditProject" id="btnEditProject${index}" record-id="${project.id}" projectRowId="${index}"><i class="fas fa-pen"></i></button></td>
                                <td><button id="button" class="btn btn-primary btntrasht btnRemoveProject" data-toggle="tooltip" title="Delete Project" project-id="${project.id}" projectRowId="${index}"><i class="fas fa-trash"></i></button></td>
                            </tr>
                        `);
                    });

                    performanceDetailsTable.destroy(false);
                    employeeDetails.performanceDetails.forEach(
                        (performance, index) => {
                            $("#performanaceDetailsTb tbody").append(`
                            <tr id="performanceRow${index}">
                                <td><input type='checkbox'></td>
                                <td>${performance.year}</td>
                                <td>${performance.grade}</td>
                                <td>${performance.comments}</td>
                                <td><button type="file" class="btn btn-primary btnpencil performanceEdit btnEditPerformance" performanceRowId="${index}" id="btnEditPerformance${index}" record-id="${performance.id}"><i class="fas fa-pen"></i></button></td>
                                <td><button id="btnRemovePerformance${index}" class="btn btn-primary btntrasht btnRemovePerformance" performanceRowId="${index}" performance-id="${performance.id}"><i class="fas fa-trash"></i></button></td>
                            </tr>
                        `);
                        }
                    );

                    createProjectDetailsDatatable();
                    createPerformanceDetailsDatatable();
                    editProject();
                    editPerformance();
                    $("#saveButtonsDiv").hide();
                    $("#updateButtonsDiv").show();
                    deleteProject();
                    deletePerformanceRecord();

                    /*$(".btnEditProject").click(function () {
                        let projectFormInputEls = $("#addProjectFormId").find(
                            ":input"
                        );
                        selectedProjectRow = $(this).attr("projectRowId");
                        let projectId = $(this).attr("record-id");
                        let objectToEdit = {};
                        for (let obj of employeeDetails.projectDetails) {
                            if (obj.id == projectId) {
                                objectToEdit = { ...obj };
                            }
                        }
                        Object.keys(projectFormInputEls).forEach((key) => {
                            let el = projectFormInputEls[key];
                            el.value = objectToEdit[el.name];
                        });

                        $("#exampleModal2").modal("show");
                    });*/
                })
                .catch((e) => {
                    console.log(`Error: ${e.message}`);
                    Swal.fire({
                        title: "Something went wrong!",
                        text: "Unable to fetch employee details!",
                        showClass: {
                            popup: "animate__animated animate__rubberBand",
                        },
                        hideClass: {
                            popup: "animate__animated animate__backOutUp",
                        },
                        icon: "error",
                    });
                });
        }
        $("#exampleModal").modal("show");
    });
});

const deleteProject = () => {
    $(".btnRemoveProject").click(function() {
        let projectId = $(this).attr("project-id");
        let projectRowId = $(this).attr("projectRowId");
        if (projectId !== undefined && projectId !== "" && projectRowId != 0) {
            Swal.fire({
                    title: "Are you sure?",
                    text: "You want to delete this project?",
                    icon: "question",
                    showCancelButton: true,
                    confirmButtonColor: "#3085d6",
                    cancelButtonColor: "#d33",
                    confirmButtonText: "Yes",
                })
                .then((result) => {
                    if (result.isConfirmed) {
                        dAuthAxios
                            .delete(`/r/delete/projectdetails/${projectId}`)
                            .then((response) => {
                                Swal.fire({
                                    title: "Done!",
                                    text: "Project record deleted successfully!",
                                    icon: "success",
                                    showCancelButton: false,
                                    confirmButtonColor: "#3085d6",
                                    cancelButtonColor: "#d33",
                                    confirmButtonText: "Ok",
                                }).then((result) => {
                                    // removing project details
                                    let projectDetails = employeeDetails.projectDetails.map(
                                        (pd) => {
                                            if (pd.id === Number(projectId)) {
                                                return {
                                                    ...pd,
                                                    isDeleted: "Y",
                                                };
                                            } else {
                                                return pd;
                                            }
                                        }
                                    );

                                    employeeDetails.projectDetails = projectDetails;
                                    $(`#projectRow${projectRowId}`).remove();
                                });
                            });
                    }
                })
                .catch((e) => {
                    console.log("Something went wrong!");
                });
        } else {
            $(`#projectRow${projectRowId}`).remove();
        }
    });
};

const deletePerformanceRecord = () => {
    $(".btnRemovePerformance").click(function() {
        let performanceId = $(this).attr("performance-id");
        let performanceRowId = $(this).attr("performanceRowId");
        if (performanceId !== undefined && performanceId !== "") {
            Swal.fire({
                    title: "Are you sure?",
                    text: "You want to delete this performance record?",
                    icon: "question",
                    showCancelButton: true,
                    confirmButtonColor: "#3085d6",
                    cancelButtonColor: "#d33",
                    confirmButtonText: "Yes",
                })
                .then((result) => {
                    if (result.isConfirmed) {
                        dAuthAxios
                            .delete(
                                `/r/delete/performancedetails/${performanceId}`
                            )
                            .then((response) => {
                                Swal.fire({
                                    title: "Done!",
                                    text: "Performance record deleted successfully!",
                                    icon: "success",
                                    showCancelButton: false,
                                    confirmButtonColor: "#3085d6",
                                    cancelButtonColor: "#d33",
                                    confirmButtonText: "Ok",
                                }).then((result) => {
                                    // removing project details
                                    let performanceDetails = employeeDetails.performanceDetails.map(
                                        (pd) => {
                                            if (
                                                pd.id === Number(performanceId)
                                            ) {
                                                return {
                                                    ...pd,
                                                    isDeleted: "Y",
                                                };
                                            } else {
                                                return pd;
                                            }
                                        }
                                    );
                                    // hard reload and check nows
                                    employeeDetails.performanceDetails = performanceDetails;
                                    $(
                                        `#performanceRow${performanceRowId}`
                                    ).remove();
                                });
                            });
                    }
                })
                .catch((e) => {
                    console.log("Something went wrong!");
                });
        } else {
            $(`#performanceRow${performanceRowId}`).remove();
        }
    });
};

const editProject = () => {
    $(".btnEditProject").click(function() {
        let projectFormInputEls = $("#addProjectFormId").find(":input");
        selectedProjectRow = $(this).attr("projectRowId");
        let projectId = $(this).attr("record-id");
        let projectRowId = $(this).attr("projectRowId");
        let objectToEdit = {};
        for (let obj of employeeDetails.projectDetails) {
            if (projectId !== undefined && projectId !== "") {
                if (obj.id == projectId) {
                    objectToEdit = obj;
                }
            } else {
                if (obj.rowIndex == projectRowId) {
                    objectToEdit = obj;
                }
            }
        }
        Object.keys(projectFormInputEls).forEach((key) => {
            let el = projectFormInputEls[key];
            el.value = objectToEdit[el.name];
        });

        isProjectEdit = true;
        $("#exampleModal2").modal("show");
    });
};

const editPerformance = () => {
    $(".btnEditPerformance").click(function() {
        let performanceFormInputEls = $("#performanceDetailsFormId").find(
            ":input"
        );
        selectedPerformanceRow = $(this).attr("performanceRowId");
        let performanceId = $(this).attr("record-id");
        let objectToEdit = {};
        for (let obj of employeeDetails.performanceDetails) {
            console.log("performanceId: " + performanceId);
            if (performanceId !== undefined && performanceId !== "") {
                if (obj.id == performanceId) {
                    objectToEdit = obj;
                }
            } else {
                console.log(
                    "obj.rowIndex == selectedPerformanceRow: " +
                    (obj.rowIndex == selectedPerformanceRow)
                );
                if (obj.rowIndex == selectedPerformanceRow) {
                    objectToEdit = obj;
                }
            }
        }
        Object.keys(performanceFormInputEls).forEach((key) => {
            let el = performanceFormInputEls[key];
            console.log("objectToEdit: ", objectToEdit);
            el.value = objectToEdit[el.name];
        });

        isPerformanceEdit = true;
        $("#exampleModal3").modal("show");
    });
};

const saveEmployeeDetails = (requestBody) => {
    dAuthAxios
        .post("/save/employee", requestBody)
        .then((response) => {
            let responseBody = response.data;
            console.log("responseBody: ", responseBody);
            resetEmployeeForm();
            resetProjectDetailsForm();
            resetPerformanceDetailsForm();
            resetAllDataTables();
        })
        .catch((error) => {
            console.log(error.message);
        });
};

const updateEmployeeDetails = (requestBody) => {
    let emplId = $("#id").val();
    dAuthAxios
        .post(`/update/employee/${emplId}`, requestBody)
        .then((response) => {
            let responseBody = response.data;
            console.log("responseBody: ", responseBody);
            Swal.fire({
                title: "Done?",
                text: "Employee record updated successfully!",
                icon: "success",
                showCancelButton: false,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "Ok",
            }).then((result) => {
                if (result.isConfirmed) {
                    resetEmployeeForm();
                    resetProjectDetailsForm();
                    resetPerformanceDetailsForm();
                    resetAllDataTables();
                    $("#exampleModal").modal("hide");
                }
            });
        })
        .catch((error) => {
            console.log(error.message);
        });
};

const prepareDatasetFromForm = (formId) => {
    let inputs = $(`#${formId}`).find(":input");
    let formInputs = {};
    Object.keys(inputs).forEach((key) => {
        let el = inputs[key];
        let fieldName = el.name;
        if (fieldName !== undefined) {
            if (fieldName.match(/\[[^\]]*]/g)) {
                var values = $(`input[name="${fieldName}"]`)
                    .map(function() {
                        return this.value;
                    })
                    .get();
                let key = $(`#${el.id}`).attr("obj-key");
                if (formInputs[key] === undefined || formInputs[key] === null) {
                    formInputs[key] = [];
                    values.forEach((v) => {
                        formInputs[key].push({});
                    });
                }
                console.table(formInputs);
                values.forEach((val, index) => {
                    formInputs[key][index][fieldName.replace("[]", "")] = val;
                });
            } else {
                formInputs[fieldName] = el.value;
            }
        }
    });
    return formInputs;
};

const resetProjectDetailsForm = () => {
    $("#projectCode").val("");
    $("#projectName").val("");
    $("#technology").val("");
    $("#duration").val("");
    $("#status").val("");
};

const resetPerformanceDetailsForm = () => {
    $("#year").val("");
    $("#grade").val("");
    $("#comments").val("");
};

const resetAllDataTables = () => {
    projectRows = 0;
    performanceDetailsRows = 0;

    $("#employeeProjectDetails").html("");
    $("#employeePerformanceDetails").html("");
    projectDetailsTable.clear();
    performanceDetailsTable.clear();
    projectDetailsTable.destroy(false);
    performanceDetailsTable.destroy(false);
    createProjectDetailsDatatable();
    createPerformanceDetailsDatatable();
};

const resetEmployeeForm = () => {
    $("#empName").val("");
    $("#empDesign").val("");
    $("#empExp").val("");
    $("#empContact").val("");
};

const createProjectDetailsDatatable = () => {
    projectDetailsTable = $("#projectDetailsTb").DataTable({
        ajax: "",
        destroy: true,
        columnDefs: [{
            targets: 0,
            searchable: true,
            orderable: false,
            width: "1%",
            className: "dt-body-center",
            render: function(data, type, full, meta) {
                return '<input type="checkbox">';
            },
        }, ],
        order: [1, "asc"],
        rowCallback: function(row, data, dataIndex) {
            // Get row ID
            var rowId = data[0];
            // If row ID is in the list of selected row IDs
            /*if ($.inArray(rowId, rows_selected) !== -1) {
                $(row).find('input[type="checkbox"]').prop("checked", true);
                $(row).addClass("selected");
            }*/
        },
    });
};
const createPerformanceDetailsDatatable = () => {
    performanceDetailsTable = $("#performanaceDetailsTb").DataTable({
        ajax: "",
        destroy: true,
        columnDefs: [{
            targets: 0,
            searchable: true,
            orderable: false,
            width: "1%",
            className: "dt-body-center",
            render: function(data, type, full, meta) {
                return '<input type="checkbox">';
            },
        }, ],
        order: [1, "asc"],
        rowCallback: function(row, data, dataIndex) {
            //var rowId = data[0];
        },
    });
};
// datable code
function run() {
    var doc = new jsPDF("p", "mm", "letter");
    doc.fromHTML($("#dvData").get(0), 15, 15, {
        'width': 170
    }, function(a) {
        doc.save("Image.pdf");
    });
}
var is_required = "{{ __('messages.is_required') }}";
var Yes = "{{ __('messages.Yes') }}";
var Field = "{{ __('messages.Field') }}";
var Employee_Name = "{{ __('messages.Employee_Name') }}";
var Designation = "{{ __('messages.Designation') }}";
var Technology = "{{ __('messages.Technology') }}";
var Year = "{{ __('messages.Year') }}";
var Grade = "{{ _('messages.Grade') }}";
var Comments = "{{ __('messages.Comments') }}";
var Status = "{{ __('messages.Status') }}";
var Contact = "{{ __('messages.Contact') }}";
var Project_Code = "{{ __('messages.Project_Code') }}";
var Project_Name = "{{ __('messages.Project_Name') }}";
var Duration = "{{ __('messages.Duration') }}";
var Experience = "{{ __('messages.Experience') }}";
//alert
var Are_you_sure = "{{ __('messages.Are_you_sure') }}";
var You_want_to_submit_the_employee_details = "{{ __('messages.You_want_to_submit_the_employee_details') }}";
var You_want_to_update_the_employee_details = "{{ __('messages.You_want_to_update_the_employee_details') }}";
var You_want_to_delete_the_employee = "{{ __('messages.You_want_to_delete_the_employee') }}";
var Employee_record_deleted_successfully = "{{ __('messages.Employee_record_deleted_successfully') }}";
var You_want_to_delete_this_project = "{{ __('messages.You_want_to_delete_this_project') }}";
var Project_record_deleted_successfully = "{{ __('messages.Project_record_deleted_successfully') }}";
var Done = "{{ __('messages.Done') }}";
var Employee_record_deleted_successfully = "{{ __('messages.Employee_record_deleted_successfully') }}";

var Something_went_wrong = "{{ __('messages.Something_went_wrong') }}";
var Unable_to_fetch_employee_details = "{{ __('messages.Unable_to_fetch_employee_details') }}";
var Employee_record_updated_successfully = "{{ __('messages.Employee_record_updated_successfully') }}";

var Performance_record_deleted_successfully = "{{ __('messages.Performance_record_deleted_successfully') }}";
var You_want_to_delete_this_performance_record = "{{ __('messages.You_want_to_delete_this_performance_record') }}";
var Employee_record_updated_successfully = "{{ __('messages.Employee_record_updated_successfully') }}";

let projectRows = 0;
let performanceDetailsRows = 0;
let projectDetailsTable = null;
let performanceDetailsTable = null;
let isEdit = false;
let isPerformanceEdit = false;
let isProjectEdit = false;
let selectedProjectRow = 0;
let selectedPerformanceRow = 0;
let employeeDetails = {};




// $(document).ready(function() {
//     $('#table,#performanaceDetailsTb,#projectDetailsTb').DataTable({

//         destroy: true,
//         "order": [
//             [0, "desc"]
//         ],
//         "language": {
//             "sProcessing": "{{ __('messages.sSearch') }}..",
//             "sLengthMenu": "  {{ __('messages.Show_MENU_entries') }}",
//             "sZeroRecords": "{{ __('messages.No_data_available_in_table') }}",
//             "sInfo": " {{ __('messages.Shows_menu_entries') }}",
//             "sInfoEmpty": "{{ __('messages.Showing_0_to_0_of_0_entries') }}",

//             "sInfoPostFix": "",
//             "sSearch": "{{ __('messages.sSearch') }}:",
//             "sUrl": "",
//             "oPaginate": {
//                 "sFirst": "{{ __('messages.sFirst') }}",
//                 "sPrevious": "{{ __('messages.sPrevious') }}",
//                 "sNext": "{{ __('messages.sNext') }}",
//                 "sLast": "{{ __('messages.sLast') }}"
//             },
//             "copyTitle": "{{ __('messages.Copy_to_Clipboard') }}",
//             "csv": "{{ __('messages.CSV') }}",
//             "excel": "{{ __('messages.Excel') }}",
//             "pageLength": {
//                 "-1": "{{ __('messages.Show_all_rows') }}",
//                 "1": "{{ __('messages.Show_1_row') }}",
//                 "_": "{{ __('messages.Show_%d_rows') }}"
//             },
//             "pdf": "{{ __('messages.PDF') }}",
//             "print": "{{ __('messages.Print') }}"
//         }
//     });
// });