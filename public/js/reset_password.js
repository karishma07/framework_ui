$(document).ready(function () {
$("#togglePassword2").click(function() {
        if ($("#newPassword").attr("type") == "password") {
            $("#newPassword").attr("type", "text");
        } else {
            $("#newPassword").attr("type", "password");
        }
        //var passwordField = document.getElementById("password");
    });
    $("#togglePassword3").click(function() {
        if ($("#confirm_password").attr("type") == "password") {
            $("#confirm_password").attr("type", "text");
        } else {
            $("#confirm_password").attr("type", "password");
        }
        //var passwordField = document.getElementById("password");
    });
    $("#newPassword").keyup(function () {
        console.log("keyup");
        const passwordPattern = /^(?=.*[A-Z])(?=.*[a-z])(?=.*\d)(?=.*[^A-Za-z0-9]).{8,}$/;
        var password = $("#newPassword").val();
        if (password.length == 0) {
            $("#newPassword-validation").html(Password_required);
            if (passwordPattern.test(password)) {
                $("#newPassword-validation").html("");
            } else {
                $("#newPassword-validation").html(Password_valid);
            }
        }
    });

    $("#confirm_password").keyup(function () {
        console.log("keyup");
        const passwordPattern = /^(?=.*[A-Z])(?=.*[a-z])(?=.*\d)(?=.*[^A-Za-z0-9]).{8,}$/;
        var password = $("#confirm_password").val();
        if (password.length == 0) {
            $("#confirm_password-validation-validation").html(Confirm_Password_required);
            if (passwordPattern.test(password)) {
                $("#confirm_password-validation-validation").html("");
            } else {
                $("#confirm_password-validation-validation").html(Confirm_Password_valid);
            }
        }
    });
    $("#confirm_password").keyup(function () {
        var confirm_password = $("#confirm_password").val();
        var newPassword = $("#newPassword").val();
        if (newPassword != confirm_password) {
            $("#confirm_password-validation").html(Password_and_confirm_password_must_match);
        }
        else{
            $("#confirm_password-validation").html(" ");
        }
    });
    $("#btnResetPass").click(function () {
        var newPassword = $("#newPassword").val();
        var confirm_password = $("#confirm_password").val();
       // var confirm_password = $("#confirm_password").val();
        var qrStr = window.location.href;
		  console.log("qtr==>"+qrStr);
            qrStr = qrStr.split("/");
            var id = qrStr [qrStr.length -1];
			console.log("ID===>"+id);
        if (newPassword != "") {
            var reqData = {
                newPassword: newPassword,       
            };
            $.ajax({
                url: `${baseURL}'/api/resetForgotPassword`+id,
               //url: "http://localhost:8000/api/resetForgotPassword/"+id,
                method: "POST",
                data: reqData,
                type: "json",
                success: function (response, textStatus, jqXHR) {
                    console.log("token==>" + JSON.stringify(response));
                    //alert("success");
                    if (response != null) {
                        if (jqXHR.status == 200) {
                            //swal(Success, Password_reset_successfully, Ok);
                            //swal("Good job!", "You clicked the button!", "success")
                            Swal.fire({
                                title: Success,
                                text: Password_reset_successfully,
                                icon: "success",
                                confirmButtonText: Ok,
                            });
                            window.location.href = `${baseURL}`;                      
                        }
                    }
                },
                error: function (xhr, textStatus, err) {
                    // alert("err");
                    console.log("xhr: ", xhr);
                    console.log("Error while fetching the preview data: ", err);
                    if (xhr.status == 400) {
                        Swal.fire({
                            title: Error,
                            text: Invalid_Credentials,
                            icon: "error",
                            confirmButtonText: Ok,
                        });
                    } else if (xhr.status == 404) {
                        Swal.fire({
                            title: Error,
                            text: pls_chk_api_url,
                            icon: "error",
                            confirmButtonText: Ok,
                        });
                    } else if (xhr.status == 500) {
                        Swal.fire({
                            title: Error,
                            text: Something_Went_Wrong_Exception_Occured,
                            icon: "error",
                            confirmButtonText: Ok,
                        });
                    }
                },
            });
        } else {
            //alert("please eneter details!");
            $("#confirm_password-validation").html(Confirm_Password_required);
            $("#newPassword-validation").html(Password_required);
        }
    });
});

