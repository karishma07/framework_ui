$(document).ready(function() {
    var loggedInUserID = localStorage.getItem("loggedInUserId");
    //console.log("UserID==>" + loggedInUserID);
    $("#togglePassword").click(function() {
        if ($("#password").attr("type") == "password") {
            $("#password").attr("type", "text");
        } else {
            $("#password").attr("type", "password");
        }
        //var passwordField = document.getElementById("password");
    });
    $("#togglePassword2").click(function() {
        if ($("#confirmpass").attr("type") == "password") {
            $("#confirmpass").attr("type", "text");
        } else {
            $("#confirmpass").attr("type", "password");
        }
        //var passwordField = document.getElementById("password");
    });
    $(".otp_div").hide();
    $(".otp_verify").hide();
    $("#btnRegister").click(function() {
        $(".otp_div").show();
        $(".otp_verify").show();
        $("#btnRegister").hide();
        var username = $("#userName").val();
        var password = $("#password").val();
        var mobile = $("#mobile").val();
        var roleId = 1;
        var reqData = {
            userName: username,
            password: password,
            mobile: mobile,
            roleId: roleId,
        };
        $.ajax({
            url: `${baseURL}/api/otp`,
            method: "POST",
            data: reqData,
            type: "json",
            success: function(response, textStatus, jqXHR) {
                console.log("response==>" + JSON.stringify(response));
                if (response != null) {
                    if (jqXHR.status == 200) {
                        //window.location.href = `${baseURL}`;
                        console.log(response);
                        sessionStorage.setItem("sKey", response.key);
                    }
                }
            },
            error: function(xhr, textStatus, err) {
                console.log("xhr: ", xhr);
                console.log("Error while fetching the preview data: ", err);
                if (xhr.status == 404) {
                    Swal.fire({
                        title: Error,
                        text: pls_chk_api_url,
                        icon: "error",
                        confirmButtonText: Ok,
                    });
                } else if (xhr.status == 500) {
                    Swal.fire({
                        title: Error,
                        text: Something_Went_Wrong_Exception_Occured,
                        icon: "error",
                        confirmButtonText: Ok,
                    });
                }
            },
        });
    });

    $("#verifyOTP").click(function() {
        var username = $("#userName").val();
        var password = $("#password").val();
        var mobile = $("#mobile").val();
        var roleId = 1;
        let sKey = sessionStorage.getItem("sKey");
        let otp = $("#otp").val();
        var reqData = {
            userName: username,
            password: password,
            mobile: mobile,
            roleId: roleId,
        };
        if (otp === sKey) {
            $.ajax({
                url: `${baseURL}/api/register`,
                method: "POST",
                data: reqData,
                type: "json",
                success: function(response, textStatus, jqXHR) {
                    sessionStorage.removeItem("sKey");
                    console.log("response==>" + JSON.stringify(response));
                    if (response != null) {
                        if (jqXHR.status == 200) {
                            window.location.href = `${baseURL}`;
                            console.log(response);
                        }
                    }
                },
                error: function(xhr, textStatus, err) {
                    sessionStorage.removeItem("sKey");
                    console.log("xhr: ", xhr);
                    console.log("Error while fetching the preview data: ", err);
                    if (xhr.status == 404) {
                        Swal.fire({
                            title: Error,
                            text: pls_chk_api_url,
                            icon: "error",
                            confirmButtonText: Ok,
                        });
                    } else if (xhr.status == 500) {
                        Swal.fire({
                            title: Error,
                            text: Something_Went_Wrong_Exception_Occured,
                            icon: "error",
                            confirmButtonText: Ok,
                        });
                    }
                },
            });
        } else {
            sessionStorage.removeItem("sKey");
            Swal.fire({
                title: Error,
                text: OTP_Verification_Failed,
                icon: "error",
                confirmButtonText: Ok,
            });
        }
    });

    $("#userName").keyup(function() {
        var username = $("#userName").val();
        const userNamePattern = /^[A-Za-z]+.*$/;
        this.userNameValidationMsg = "";
        if (username.length == 0) {
            //this.userNameValidationMsg = "UserName is required !";
            $("#username-validation").html(Username_required);
            this.isUserNameValid = false;
        } else {
            if (userNamePattern.test(username)) {
                $("#username-validation").html("");
                this.isUserNameValid = true;
            } else {
                this.isUserNameValid = false;
                //this.userNameValidationMsg = "UserName should start with Alphabets !";
                $("#username-validation").html(
                    UserName_should_start_with_Alphabets
                );
            }
        }
    });


    $("#mobile").change(function(event) {
        let val = event.target.value;
        console.log("val==>", val);
        let mobile = $("#mobile").val();
        const mobilePattern = /^[0-9]+$/;
        if (!mobilePattern.test(val)) {
            mobile = mobile.substring(0, mobile.length - 1);
            event.target.value = mobile;
        }

        if (mobile.length == 0) {
            $("#mobile-validation").html(mobile_number_required);
        } else {
            if (mobilePattern.test(mobile)) {
                $("#mobile-validation").html("");
                return true;
            } else {
                $("#mobile-validation").html(mobile_number_required);
                return false;
            }
        }
    });
});

/* validateMobile(event) : void {
        let val = event.target.value;
        const mobilePattern = /^[0-9]+$/;
        if(!mobilePattern.test(val)){
          this.mobile = this.mobile.substring(0,(this.mobile.length-1));
          event.target.value = this.mobile;
        } 
        this.mobileValidationMsg = '';
        if(this.mobile.length == 0){
          this.mobileValidationMsg = "Mobile number is required !";
          this.isMobileValid = false;
        }
        else{
          if(mobilePattern.test(this.mobile)){
            this.isMobileValid = true;
          }else{
            this.isMobileValid = false;
            this.mobileValidationMsg = 'Mobile number is not valid !';
          }
        }
      }*/

function validateConfirmPassword() {
    var password = $("#password").val();
    var confirmPassword = $("#confirmPassword").val();
    ///this.confirmPasswordValidationMsg = '';
    if (password == confirmPassword) {
        $("#confirmPass-validation").html("");
        return true;
    } else {
        $("#confirmPass-validation").html(Password_not_match);
        return false;
    }
}