$(document).ready(function () {
    //validation for employee name
    $("#empName").keyup(function () {
        var username = $("#empName").val();
        const userNamePattern = /^[A-Za-z]+.*$/;
        this.userNameValidationMsg = "";
        if (username.length == 0) {
            //this.userNameValidationMsg = "UserName is required !";
            $("#empName-validation").html("Name is not valid !");
            this.isUserNameValid = false;
        } else {
            if (userNamePattern.test(username)) {
                $("#empName-validation").html("");
                this.isUserNameValid = true;
            } else {
                this.isUserNameValid = false;
                //this.userNameValidationMsg = "UserName should start with Alphabets !";
                $("empName-validation").html(
                    "UserName should start with Alphabets !"
                );
            }
        }
    });
    //for edit functionality

    //validation for employee name
    $("#empNameEdit").keyup(function () {
        var username = $("#empNameEdit").val();
        const userNamePattern = /^[A-Za-z]+.*$/;
        this.userNameValidationMsg = "";
        if (username.length == 0) {
            //this.userNameValidationMsg = "UserName is required !";
            $("#empNameEdit-validation").html("Name is not valid !");
            this.isUserNameValid = false;
        } else {
            if (userNamePattern.test(username)) {
                $("#empNameEdit-validation").html("");
                this.isUserNameValid = true;
            } else {
                this.isUserNameValid = false;
                //this.userNameValidationMsg = "UserName should start with Alphabets !";
                $("empNameEdit-validation").html(
                    "UserName should start with Alphabets !"
                );
            }
        }
    });

    //validation for employee Designation

    $("#empDesign").keyup(function () {
        var username = $("#empDesign").val();
        const userNamePattern = /^[A-Za-z]+.*$/;
        this.userNameValidationMsg = "";
        if (username.length == 0) {
            //this.userNameValidationMsg = "UserName is required !";
            $("#design-validation").html("Designation is not valid !");
            this.isUserNameValid = false;
        } else {
            if (userNamePattern.test(username)) {
                $("#design-validation").html("");
                this.isUserNameValid = true;
            } else {
                this.isUserNameValid = false;
                //this.userNameValidationMsg = "UserName should start with Alphabets !";
                $("design-validation").html("Designation is not valid !");
            }
        }
    });
    //for edit functionality validation for designation

    $("#empDesignEdit").keyup(function () {
        var username = $("#empDesignEdit").val();
        const userNamePattern = /^[A-Za-z]+.*$/;
        this.userNameValidationMsg = "";
        if (username.length == 0) {
            //this.userNameValidationMsg = "UserName is required !";
            $("#designEdit-validation").html("Designation is not valid !");
            this.isUserNameValid = false;
        } else {
            if (userNamePattern.test(username)) {
                $("#designEdit-validation").html("");
                this.isUserNameValid = true;
            } else {
                this.isUserNameValid = false;
                //this.userNameValidationMsg = "UserName should start with Alphabets !";
                $("designEdit-validation").html("Designation is not valid !");
            }
        }
    });
    //VALIDATION FOR CONTACT

    $("#empContact").keyup(function (event) {
        let val = event.target.value;
        console.log("val==>", val);
        let contact = $("#empContact").val();
        const contactPattern = /^[0-9]+$/;
        if (!contactPattern.test(val)) {
            contact = contact.substring(0, contact.length - 1);
            event.target.value = contact;
        }

        if (contact.length == 0) {
            $("#contact-validation").html("Contact is not valid!");
        } else {
            if (contactPattern.test(contact)) {
                $("#contact-validation").html("");
                return true;
            } else {
                tact;

                $("#contact-validation").html("Contact is not valid!");
                return false;
            }
        }
    });
    //validations for contact for edit functionality
    $("#empContactEdit").keyup(function (event) {
        let val = event.target.value;
        console.log("val==>", val);
        let contact = $("#empContactEdit").val();
        const contactPattern = /^[0-9]+$/;
        if (!contactPattern.test(val)) {
            contact = contact.substring(0, contact.length - 1);
            event.target.value = contact;
        }

        if (contact.length == 0) {
            $("#contactEdit-validation").html("Contact is not valid!");
        } else {
            if (contactPattern.test(contact)) {
                $("#contactEdit-validation").html("");
                return true;
            } else {
                tact;

                $("#contactEdit-validation").html("Contact is not valid!");
                return false;
            }
        }
    });

    //validation for expeirnce

    $("#empExp").keyup(function (event) {
        let val = event.target.value;
        console.log("val==>", val);
        let contact = $("#empExp").val();
        const contactPattern = /^[0-9]+$/;
        if (!contactPattern.test(val)) {
            contact = contact.substring(0, contact.length - 1);
            event.target.value = contact;
        }

        if (contact.length == 0) {
            $("#exp-validation").html("Experience is not valid!");
        } else {
            if (contactPattern.test(contact)) {
                $("#exp-validation").html("");
                return true;
            } else {
                tact;

                $("#exp-validation").html("Experience is not valid!");
                return false;
            }
        }
    });

    //validation for experience for edit functionality
    $("#empExpEdit").keyup(function (event) {
        let val = event.target.value;
        console.log("val==>", val);
        let contact = $("#empExpEdit").val();
        const contactPattern = /^[0-9]+$/;
        if (!contactPattern.test(val)) {
            contact = contact.substring(0, contact.length - 1);
            event.target.value = contact;
        }

        if (contact.length == 0) {
            $("#expEdit-validation").html("Experience is not valid!");
        } else {
            if (contactPattern.test(contact)) {
                $("#expEdit-validation").html("");
                return true;
            } else {
                tact;

                $("#expEdit-validation").html("Experience is not valid!");
                return false;
            }
        }
    });

    //validation for technology

    $("#technology").keyup(function () {
        var username = $("#technology").val();
        const userNamePattern = /^[A-Za-z]+.*$/;
        this.userNameValidationMsg = "";
        if (username.length == 0) {
            //this.userNameValidationMsg = "UserName is required !";
            $("#technology-validation").html("Technology is not valid !");
            this.isUserNameValid = false;
        } else {
            if (userNamePattern.test(username)) {
                $("#technology-validation").html("");
                this.isUserNameValid = true;
            } else {
                this.isUserNameValid = false;
                //this.userNameValidationMsg = "UserName should start with Alphabets !";
                $("technology-validation").html(
                    "Technology should start with Alphabets !"
                );
            }
        }
    });

    //validation for duration

    $("#duration").keyup(function (event) {
        let val = event.target.value;
        console.log("val==>", val);
        let contact = $("#duration").val();
        const contactPattern = /^[0-9]+$/;
        if (!contactPattern.test(val)) {
            contact = contact.substring(0, contact.length - 1);
            event.target.value = contact;
        }

        if (contact.length == 0) {
            $("#duration-validation").html("Duration is not valid!");
        } else {
            if (contactPattern.test(contact)) {
                $("#duration-validation").html("");
                return true;
            } else {
                tact;

                $("#duration-validation").html("Duration is not valid!");
                return false;
            }
        }
    });

    //validation for status

    $(function () {
        //project name
        $("#BtnSave").click(function () {
            var projectName = $("#projectName");
            if (projectName.val() == "") {
                //If the "Please Select" option is selected display error.
                $("#projectName-validation").html("Project Name is required!");
                return false;
            }
            $("#projectName-validation").html("");
            return true;
        });
        $("#BtnSave").click(function () {
            //project code
            var projectCode = $("#projectCode");
            if (projectCode.val() == "") {
                //If the "Please Select" option is selected display error.
                $("#projectCode-validation").html("Project code is required!");
                return false;
            }
            $("#projectCode-validation").html("");
            return true;
        });

        //grade
        $("#BtnSavePer").click(function () {
            $("#dtBasicExample3 tbody").append(`
                <tr>
                    <td><input type='checkbox'></td>
                    <td>${$("#year").val()}</td>
                    <td>${$("#grade").val()}</td>
                    <td>${$("#comments").val()}</td>
                    <td><button type="file" class="btn btn-primary btnpencil performanceEdit" data-toggle="modal"><i class="fas fa-pen"></i></button></td>
                    <td><button id="" class="btn btn-primary btntrasht"><i class="fas fa-trash"></i></button></td>
                </tr>
            `);
            $("#employeePerformanceDetails").append(`
                <div id="epdtl${performanceRows}">
                    <input type='hidden' name='year[]' id='performanceYear${performanceRows}' obj-key='performanceDetails' value='${$("#year").val()}'>
                    <input type='hidden' name='grade[]' id='performanceGrade${performanceRows}' obj-key='performanceDetails' value='${$("#grade").val()}'>
                    <input type='hidden' name='comments[]' id='performanceComments${performanceRows}' obj-key='performanceDetails' value='${$("#comments").val()}'>
                </div>
            `);

            var grade = $("#grade");
            if (grade.val() == "") {
                //If the "Please Select" option is selected display error.
                $("#grade-validation").html("Grade is required!");
                return false;
            }
            $("#grade-validation").html("");
            return true;
        });
        $("#BtnSave").click(function () {
            //project code
            var projectCode = $("#projectCode");
            if (projectCode.val() == "") {
                //If the "Please Select" option is selected display error.
                $("#projectCode-validation").html("Project code is required!");
                return false;
            }
            $("#projectCode-validation").html("");
            return true;
        });
        $("#grade").change(function () {
            $("#grade-validation").html("");
        });

        $("#status").change(function () {
            $("#status-validation").html("");
        });

        $("#projectCode").change(function () {
            $("#projectCode-validation").html("");
        });
        $("#projectName").change(function () {
            $("#projectName-validation").html("");
        });

        //validation for edit functionality

        //validation for technology

        $("#technologyEdit").keyup(function () {
            var username = $("#technologyEdit").val();
            const userNamePattern = /^[A-Za-z]+.*$/;
            this.userNameValidationMsg = "";
            if (username.length == 0) {
                //this.userNameValidationMsg = "UserName is required !";
                $("#technologyEdit-validation").html(
                    "Technology is not valid !"
                );
                this.isUserNameValid = false;
            } else {
                if (userNamePattern.test(username)) {
                    $("#technologyEdit-validation").html("");
                    this.isUserNameValid = true;
                } else {
                    this.isUserNameValid = false;
                    //this.userNameValidationMsg = "UserName should start with Alphabets !";
                    $("technologyEdit-validation").html(
                        "Technology should start with Alphabets !"
                    );
                }
            }
        });

        //validation for duration

        $("#durationEdit").keyup(function (event) {
            let val = event.target.value;
            console.log("val==>", val);
            let contact = $("#durationEdit").val();
            const contactPattern = /^[0-9]+$/;
            if (!contactPattern.test(val)) {
                contact = contact.substring(0, contact.length - 1);
                event.target.value = contact;
            }

            if (contact.length == 0) {
                $("#durationEdit-validation").html("Duration is not valid!");
            } else {
                if (contactPattern.test(contact)) {
                    $("#durationEdit-validation").html("");
                    return true;
                } else {
                    tact;

                    $("#durationEdit-validation").html(
                        "Duration is not valid!"
                    );
                    return false;
                }
            }
        });

        $(function () {
            //project name
            $("#BtnSaveEdit").click(function () {
                var projectName = $("#projectNameEdit");
                if (projectName.val() == "") {
                    //If the "Please Select" option is selected display error.
                    $("#projectNameEdit-validation").html(
                        "Project Name is required!"
                    );
                    return false;
                }
                $("#projectNameEdit-validation").html("");
                return true;
            });
            $("#BtnSaveEdit").click(function () {
                //project code
                var projectCode = $("#projectCode");
                if (projectCode.val() == "") {
                    //If the "Please Select" option is selected display error.
                    $("#projectCodeEdit-validation").html(
                        "Project code is required!"
                    );
                    return false;
                }
                $("#projectCodeEdit-validation").html("");
                return true;
            });

            //grade
            $("#BtnSavePerEdit").click(function () {
                var grade = $("#gradeEdit");
                if (grade.val() == "") {
                    //If the "Please Select" option is selected display error.
                    $("#gradeEdit-validation").html("Grade is required!");
                    return false;
                }
                $("#gradeEdit-validation").html("");
                return true;
            });
            $("#BtnSaveEdit").click(function () {
                //project code
                var projectCode = $("#projectCodeEdit");
                if (projectCode.val() == "") {
                    //If the "Please Select" option is selected display error.
                    $("#projectCodeEdit-validation").html(
                        "Project code is required!"
                    );
                    return false;
                }
                $("#projectCodeEdit-validation").html("");
                return true;
            });

            $("#gradeEdit").change(function () {
                $("#gradeEdit-validation").html("");
            });

            $("#statusEdit").change(function () {
                $("#statusEdit-validation").html("");
            });

            $("#projectCodeEdit").change(function () {
                $("#projectCodeEdit-validation").html("");
            });
            $("#projectNameEdit").change(function () {
                $("#projectNameEdit-validation").html("");
            });
        });
        //edit function end here
        //for status
        /*$("#BtnSave").click(function () {
            var status = $("#status");
            if (status.val() == "") {
                //If the "Please Select" option is selected display error.
                $("#status-validation").html("Status is required!");
                return false;
            }
            $("#status-validation").html("");
            return true;
        });*/

        //for status edit
        $("#BtnSaveEdit").click(function () {
            var status = $("#statusEdit");
            if (status.val() == "") {
                //If the "Please Select" option is selected display error.
                $("#statusEdit-validation").html("Status is required!");
                return false;
            }
            $("#statusEdit-validation").html("");
            return true;
        });
        //validation for performace form

        /*$("#BtnSave").click(function () {
            let val = event.target.value;
            console.log("val==>", val);
            let contact = $("#duration").val();
            const contactPattern = /^[0-9]+$/;
            if (!contactPattern.test(val)) {
                contact = contact.substring(0, contact.length - 1);
                event.target.value = contact;
            }

            if (contact.length == 0) {
                $("#duration-validation").html("Duration required!");
            } else {
                if (contactPattern.test(contact)) {
                    $("#duration-validation").html("");
                    return true;
                } else {
                    tact;

                    $("#duration-validation").html("Duration is required!");
                    return false;
                }
            }
        });*/

        $("#BtnSaveEdit").click(function () {
            var technology = $("#technologyEdit").val();
            const userNamePattern = /^[A-Za-z]+.*$/;
            this.userNameValidationMsg = "";
            if (technology.length == 0) {
                //this.userNameValidationMsg = "UserName is required !";
                $("#technologyEdit-validation").html("Technology is required!");
                this.isUserNameValid = false;
            } else {
                if (userNamePattern.test(technology)) {
                    $("#technologyEdit-validation").html("");
                    this.isUserNameValid = true;
                } else {
                    this.isUserNameValid = false;
                    //this.userNameValidationMsg = "UserName should start with Alphabets !";
                    $("technologyEdit-validation").html(
                        "Technology should start with Alphabets !"
                    );
                }
            }
        });

        $("#BtnSaveEdit").click(function () {
            let val = event.target.value;
            console.log("val==>", val);
            let contact = $("#durationEdit").val();
            const contactPattern = /^[0-9]+$/;
            if (!contactPattern.test(val)) {
                contact = contact.substring(0, contact.length - 1);
                event.target.value = contact;
            }

            if (contact.length == 0) {
                $("#durationEdit-validation").html("Duration required!");
            } else {
                if (contactPattern.test(contact)) {
                    $("#durationEdit-validation").html("");
                    return true;
                } else {
                    tact;

                    $("#durationEdit-validation").html("Duration is required!");
                    return false;
                }
            }
        });

        $("#btnAddProject").click(function () {
            $("#exampleModal2").modal("show");
        });

        $("#BtnSave").click(function () {
            // Validate form
            /*if (isValid) {

            }*/

            // If condition starts
            var username = $("#technology").val();
            $("#projectDetailsTb tbody").append(`
                <tr>
                    <td><input type='checkbox'></td>
                    <td>${$("#projectCode option:selected").text()}</td>
                    <td>${$("#projectName option:selected").text()}</td>
                    <td>${$("#technology").val()}</td>
                    <td>${$("#duration").val()}</td>
                    <td>${$("#status").val()}</td>
                    <td>
                        Edit
                    </td>
                    <td>Delete</td>
                </tr>
            `);

            $(`#employeeProjectDetails`).append(`
                <div id='epdDiv${projectRows}'>
                    <input type='hidden' name='projectCode[]' id='projectCode${projectRows}' obj-key="projectDetails" value='${$("#projectCode").val()}'>
                        <input type='hidden' name='projectName[]' id='projectName${projectRows}' obj-key="projectDetails" value='${$("#projectName").val()}'>
                        <input type='hidden' name='technology[]' id='technology${projectRows}' obj-key="projectDetails" value='${$("#technology").val()}'>
                        <input type='hidden' name='duration[]' id='duration${projectRows}' obj-key="projectDetails" value='${$("#duration").val()}'>
                        <input type='hidden' name='status[]' id='status${projectRows}' obj-key="projectDetails" value='${$("#status").val()}'>
                </div>
            `);

            projectRows++;
            // If condition ends

            $("#exampleModal2").modal("hide");

            const userNamePattern = /^[A-Za-z]+.*$/;
            this.userNameValidationMsg = "";
            if (username.length == 0) {
                //this.userNameValidationMsg = "UserName is required !";
                $("#technology-validation").html("Technology is required!");
                this.isUserNameValid = false;
            } else {
                if (userNamePattern.test(username)) {
                    $("#technology-validation").html("");
                    this.isUserNameValid = true;
                } else {
                    this.isUserNameValid = false;
                    //this.userNameValidationMsg = "UserName should start with Alphabets !";
                    $("technology-validation").html(
                        "Technology should start with Alphabets !"
                    );
                }
            }
        });
    });

    $("#saveCloseBtn").click(function () {
        let inputs = $("#employeeFormID").find(":input");
        let formInputs = {};
        Object.keys(inputs).forEach((key) => {
            let el = inputs[key];
            let fieldName = el.name;
            if (fieldName !== undefined) {
                if (fieldName.match(/\[[^\]]*]/g)) {
                    var values = $(`input[name="${fieldName}"]`)
                        .map(function () {
                            return this.value;
                        })
                        .get();
                    let key = $(`#${el.id}`).attr("obj-key");
                    if (
                        formInputs[key] === undefined ||
                        formInputs[key] === null
                    ) {
                        formInputs[key] = [];
                        values.forEach((v) => {
                            formInputs[key].push({});
                        });
                    }
                    console.table(formInputs);
                    values.forEach((val, index) => {
                        formInputs[key][index][
                            fieldName.replace("[]", "")
                        ] = val;
                    });
                } else {
                    formInputs[fieldName] = el.value;
                }
            }
        });

        console.log(":: formInputs :: ", formInputs);

        /*$.ajax({
            url: "",
            method: "post",
            data: formInputs,
            success: function (response) {},
            error: function (err, err_text, xhr) {},
        });*/

        let projectFormInputs = $("#employeeFormID").serializeArray();
        console.log("projectFormInputs: ", projectFormInputs);
        let projectDetails = [];

        let rawData = {};

        projectFormInputs.forEach((record) => {
            if (rawData[record.name.replace("[]", "")]) {
                let arr = [...rawData[record.name.replace("[]", "")]];
                arr.push(record.value);
                rawData[record.name.replace("[]", "")] = arr;
            } else {
                let arr = [record.value];
                rawData[record.name.replace("[]", "")] = arr;
            }
        });

        Object.keys(rawData).forEach((fieldName) => {});

        var empName = $("#empName").val();
        const empNamePattern = /^[A-Za-z]+.*$/;
        this.empNameValidationMsg = "";
        if (empName.length == 0) {
            //this.userNameValidationMsg = "UserName is required !";
            $("#empName-validation").html("Name is required!");
            this.isempNameValid = false;
        } else {
            if (empNamePattern.test(empName)) {
                $("#empName-validation").html("");
                this.isempNameValid = true;
            } else {
                this.isempNameValid = false;
                //this.userNameValidationMsg = "UserName should start with Alphabets !";
                $("empName-validation").html(
                    "UserName should start with Alphabets !"
                );
            }
        }

        var empDesign = $("#empDesign").val();
        const empDesignPattern = /^[A-Za-z]+.*$/;
        this.empDesignValidationMsg = "";
        if (empDesign.length == 0) {
            //this.userNameValidationMsg = "UserName is required !";
            $("#design-validation").html("Designation is required!");
            this.isempDesignValid = false;
        } else {
            if (empDesignPattern.test(empDesign)) {
                $("#design-validation").html("");
                this.isempDesignValid = true;
            } else {
                this.isempDesignValid = false;
                //this.userNameValidationMsg = "UserName should start with Alphabets !";
                $("design-validation").html("Designation is required!");
            }
        }
        let val1 = event.target.value;
        console.log("val==>", val1);
        let contact = $("#empContact").val();
        const contactPattern = /^[0-9]+$/;
        if (!contactPattern.test(val1)) {
            contact = contact.substring(0, contact.length - 1);
            event.target.value = contact;
        }

        if (contact.length == 0) {
            $("#contact-validation").html("Contact is required!");
        } else {
            if (contactPattern.test(contact)) {
                $("#contact-validation").html("");
                return true;
            } else {
                tact;

                $("#contact-validation").html("Contact is required!");
                return false;
            }
        }

        let val2 = event.target.value;
        console.log("val==>", val2);
        let contactExp = $("#empExp").val();
        const contactExpPattern = /^[0-9]+$/;
        if (!contactExpPattern.test(val2)) {
            contactExp = contactExp.substring(0, contactExp.length - 1);
            event.target.value = contactExp;
        }

        if (contactExp.length == 0) {
            $("#exp-validation").html("Experience is required!");
        } else {
            if (contactPattern.test(contact)) {
                $("#exp-validation").html("");
                return true;
            } else {
                tact;

                $("#exp-validation").html("Experience is required!");
                return false;
            }
        }
    });
    //validation for edit functionality

    $("#saveCloseBtnEdit").click(function () {
        var empName = $("#empNameEdit").val();
        const empNamePattern = /^[A-Za-z]+.*$/;
        this.empNameValidationMsg = "";
        if (empName.length == 0) {
            //this.userNameValidationMsg = "UserName is required !";
            $("#empNameEdit-validation").html("Name is required!");
            this.isempNameValid = false;
        } else {
            if (empNamePattern.test(empName)) {
                $("#empNameEdit-validation").html("");
                this.isempNameValid = true;
            } else {
                this.isempNameValid = false;
                //this.userNameValidationMsg = "UserName should start with Alphabets !";
                $("empNameEdit-validation").html(
                    "UserName should start with Alphabets !"
                );
            }
        }

        var empDesign = $("#empDesignEdit").val();
        const empDesignPattern = /^[A-Za-z]+.*$/;
        this.empDesignValidationMsg = "";
        if (empDesign.length == 0) {
            //this.userNameValidationMsg = "UserName is required !";
            $("#designEdit-validation").html("Designation is required!");
            this.isempDesignValid = false;
        } else {
            if (empDesignPattern.test(empDesign)) {
                $("#designEdit-validation").html("");
                this.isempDesignValid = true;
            } else {
                this.isempDesignValid = false;
                //this.userNameValidationMsg = "UserName should start with Alphabets !";
                $("designEdit-validation").html("Designation is required!");
            }
        }
        let val1 = event.target.value;
        console.log("val==>", val1);
        let contact = $("#empContactEdit").val();
        const contactPattern = /^[0-9]+$/;
        if (!contactPattern.test(val1)) {
            contact = contact.substring(0, contact.length - 1);
            event.target.value = contact;
        }

        if (contact.length == 0) {
            $("#contactEdit-validation").html("Contact is required!");
        } else {
            if (contactPattern.test(contact)) {
                $("#contactEdit-validation").html("");
                return true;
            } else {
                tact;

                $("#contactEdit-validation").html("Contact is required!");
                return false;
            }
        }

        let val2 = event.target.value;
        console.log("val==>", val2);
        let contactExpEdit = $("#empExpEdit").val();
        const contactExpEditPattern = /^[0-9]+$/;
        if (!contactExpEditPattern.test(val2)) {
            contactExpEdit = contactExpEdit.substring(0, contact.length - 1);
            event.target.value = contactExpEdit;
        }

        if (contactExpEdit.length == 0) {
            $("#expEdit-validation").html("Experience is required!");
        } else {
            if (contactPattern.test(contact)) {
                $("#expEdit-validation").html("");
                return true;
            } else {
                tact;

                $("#expEdit-validation").html("Experience is required!");
                return false;
            }
        }
    });

    $("#saveNewBtn").click(function () {
        var empName = $("#empName").val();
        const empNamePattern = /^[A-Za-z]+.*$/;
        this.empNameValidationMsg = "";
        if (empName.length == 0) {
            //this.userNameValidationMsg = "UserName is required !";
            $("#empName-validation").html("Name is is required!");
            this.isempNameValid = false;
        } else {
            if (empNamePattern.test(empName)) {
                $("#empName-validation").html("");
                this.isempNameValid = true;
            } else {
                this.isempNameValid = false;
                //this.userNameValidationMsg = "UserName should start with Alphabets !";
                $("empName-validation").html(
                    "UserName should start with Alphabets !"
                );
            }
        }

        var empDesign = $("#empDesign").val();
        const empDesignPattern = /^[A-Za-z]+.*$/;
        this.empDesignValidationMsg = "";
        if (empDesign.length == 0) {
            //this.userNameValidationMsg = "UserName is required !";
            $("#design-validation").html("Designation is required!");
            this.isempDesignValid = false;
        } else {
            if (empDesignPattern.test(empDesign)) {
                $("#design-validation").html("");
                this.isempDesignValid = true;
            } else {
                this.isempDesignValid = false;
                //this.userNameValidationMsg = "UserName should start with Alphabets !";
                $("design-validation").html("Designation is required!");
            }
        }

        let valcontact = event.target.value;
        console.log("val==>", val);
        let contact = $("#empContact").val();
        const contactPattern = /^[0-9]+$/;
        if (!contactPattern.test(valcontact)) {
            contact = contact.substring(0, contact.length - 1);
            event.target.value = contact;
        }

        if (contact.length == 0) {
            $("#contact-validation").html("Contact is required!");
        } else {
            if (contactPattern.test(contact)) {
                $("#contact-validation").html("");
                return true;
            } else {
                tact;

                $("#contact-validation").html("Contact is required!");
                return false;
            }
        }

        let valExp = event.target.value;
        console.log("val==>", valExp);
        let contactExp = $("#empExp").val();
        const contactExpPattern = /^[0-9]+$/;
        if (!contactExpPattern.test(valExp)) {
            contactExp = contactExp.substring(0, contactExp.length - 1);
            event.target.value = contactExp;
        }

        if (contactExp.length == 0) {
            $("#exp-validation").html("Experience is required!");
        } else {
            if (contactPattern.test(contactExp)) {
                $("#exp-validation").html("");
                return true;
            } else {
                tact;

                $("#exp-validation").html("Experience is required!");
                return false;
            }
        }
    });
    //validation for edit functionality

    $("#saveNewBtnEdit").click(function () {
        var empName = $("#empNameEdit").val();
        const empNamePattern = /^[A-Za-z]+.*$/;
        this.empNameValidationMsg = "";
        if (empName.length == 0) {
            //this.userNameValidationMsg = "UserName is required !";
            $("#empNameEdit-validation").html("Name is is required!");
            this.isempNameValid = false;
        } else {
            if (empNamePattern.test(empName)) {
                $("#empNameEdit-validation").html("");
                this.isempNameValid = true;
            } else {
                this.isempNameValid = false;
                //this.userNameValidationMsg = "UserName should start with Alphabets !";
                $("empNameEdit-validation").html(
                    "UserName should start with Alphabets !"
                );
            }
        }

        var empDesign = $("#empDesignEdit").val();
        const empDesignPattern = /^[A-Za-z]+.*$/;
        this.empDesignValidationMsg = "";
        if (empDesign.length == 0) {
            //this.userNameValidationMsg = "UserName is required !";
            $("#designEdit-validation").html("Designation is required!");
            this.isempDesignValid = false;
        } else {
            if (empDesignPattern.test(empDesign)) {
                $("#designEdit-validation").html("");
                this.isempDesignValid = true;
            } else {
                this.isempDesignValid = false;
                //this.userNameValidationMsg = "UserName should start with Alphabets !";
                $("designEdit-validation").html("Designation is required!");
            }
        }

        let valcontactEdit = event.target.value;
        console.log("val==>", valcontactEdit);
        let contact = $("#empContactEdit").val();
        const contactPattern = /^[0-9]+$/;
        if (!contactPattern.test(valcontactEdit)) {
            contact = contact.substring(0, contact.length - 1);
            event.target.value = contact;
        }

        if (contact.length == 0) {
            $("#contactEdit-validation").html("Contact is required!");
        } else {
            if (contactPattern.test(contact)) {
                $("#contactEdit-validation").html("");
                return true;
            } else {
                tact;

                $("#contactEdit-validation").html("Contact is required!");
                return false;
            }
        }

        let valExp = event.target.value;
        console.log("val==>", valExp);
        let contactExpEdit = $("#empExpEdit").val();
        const contactExpEditPattern = /^[0-9]+$/;
        if (!contactExpEditPattern.test(valExp)) {
            contactExpEdit = contactExpEdit.substring(
                0,
                contactExpEdit.length - 1
            );
            event.target.value = contactExpEdit;
        }

        if (contactExpEdit.length == 0) {
            $("#expEdit-validation").html("Experience is required!");
        } else {
            if (contactExpEditPattern.test(contactExpEdit)) {
                $("#expEdit-validation").html("");
                return true;
            } else {
                tact;

                $("#expEdit-validation").html("Experience is required!");
                return false;
            }
        }
    });

    //validation for year

    $("#year").keyup(function (event) {
        let val = event.target.value;
        console.log("val==>", val);
        let year = $("#year").val();
        const yearPattern = /^[0-9]+$/;
        if (!yearPattern.test(val)) {
            year = year.substring(0, year.length - 1);
            event.target.value = year;
        }

        if (year.length == 0) {
            $("#year-validation").html("year is not valid!");
        } else {
            if (yearPattern.test(year)) {
                $("#year-validation").html("");
                return true;
            } else {
                tact;

                $("#year-validation").html("year is not valid!");
                return false;
            }
        }
    });
    //validation forcomments

    $("#comments").keyup(function () {
        var comments = $("#comments").val();
        const commentsPattern = /^[A-Za-z]+.*$/;
        this.commentsValidationMsg = "";
        if (comments.length == 0) {
            //this.userNameValidationMsg = "UserName is required !";
            $("#comments-validation").html("comments is not valid !");
            this.iscommentsValid = false;
        } else {
            if (commentsPattern.test(comments)) {
                $("#comments-validation").html("");
                this.iscommentsValid = true;
            } else {
                this.iscommentsValid = false;
                //this.userNameValidationMsg = "UserName should start with Alphabets !";
                $("comments-validation").html(
                    "comments should start with Alphabets !"
                );
            }
        }
    });

    //edit

    //validation for year

    $("#yearEdit").keyup(function (event) {
        let val = event.target.value;
        console.log("val==>", val);
        let year = $("#yearEdit").val();
        const yearPattern = /^[0-9]+$/;
        if (!yearPattern.test(val)) {
            year = year.substring(0, year.length - 1);
            event.target.value = year;
        }

        if (year.length == 0) {
            $("#yearEdit-validation").html("year is not valid!");
        } else {
            if (yearPattern.test(year)) {
                $("#yearEdit-validation").html("");
                return true;
            } else {
                tact;

                $("#yearEdit-validation").html("year is not valid!");
                return false;
            }
        }
    });
    //validation forcomments

    $("#commentsEdit").keyup(function () {
        var comments = $("#commentsEdit").val();
        const commentsPattern = /^[A-Za-z]+.*$/;
        this.commentsValidationMsg = "";
        if (comments.length == 0) {
            //this.userNameValidationMsg = "UserName is required !";
            $("#commentsEdit-validation").html("comments is not valid !");
            this.iscommentsValid = false;
        } else {
            if (commentsPattern.test(comments)) {
                $("#commentsEdit-validation").html("");
                this.iscommentsValid = true;
            } else {
                this.iscommentsValid = false;
                //this.userNameValidationMsg = "UserName should start with Alphabets !";
                $("commentsEdit-validation").html(
                    "comments should start with Alphabets !"
                );
            }
        }
    });
    /*$("#BtnSavePer").click(function () {
        let val = event.target.value;
        console.log("val==>", val);
        let year = $("#year").val();
        const yearPattern = /^[0-9]+$/;
        if (!yearPattern.test(val)) {
            year = year.substring(0, year.length - 1);
            event.target.value = year;
        }

        if (year.length == 0) {
            $("#year-validation").html("year is required!");
        } else {
            if (yearPattern.test(year)) {
                $("#year-validation").html("");
                return true;
            } else {
                tact;

                $("#year-validation").html("year is required!");
                return false;
            }
        }

        var comments = $("#comments").val();
        const commentsPattern = /^[A-Za-z]+.*$/;
        this.commentsValidationMsg = "";
        if (comments.length == 0) {
            //this.userNameValidationMsg = "UserName is required !";
            $("#comments-validation").html("comments is not valid !");
            this.iscommentsValid = false;
        } else {
            if (commentsPattern.test(comments)) {
                $("#comments-validation").html("");
                this.iscommentsValid = true;
            } else {
                this.iscommentsValid = false;
                //this.userNameValidationMsg = "UserName should start with Alphabets !";
                $("comments-validation").html(
                    "comments should start with Alphabets !"
                );
            }
        }
    });*/
    //validation for edit functionality
    $("#BtnSavePerEdit").click(function () {
        let val = event.target.value;
        console.log("val==>", val);
        let year = $("#yearEdit").val();
        const yearPattern = /^[0-9]+$/;
        if (!yearPattern.test(val)) {
            year = year.substring(0, year.length - 1);
            event.target.value = year;
        }

        if (year.length == 0) {
            $("#yearEdit-validation").html("year is required!");
        } else {
            if (yearPattern.test(year)) {
                $("#yearEdit-validation").html("");
                return true;
            } else {
                tact;

                $("#yearEdit-validation").html("year is required!");
                return false;
            }
        }

        var comments = $("#commentsEdit").val();
        const commentsPattern = /^[A-Za-z]+.*$/;
        this.commentsValidationMsg = "";
        if (comments.length == 0) {
            //this.userNameValidationMsg = "UserName is required !";
            $("#commentsEdit-validation").html("comments is required !");
            this.iscommentsValid = false;
        } else {
            if (commentsPattern.test(comments)) {
                $("#commentsEdit-validation").html("");
                this.iscommentsValid = true;
            } else {
                this.iscommentsValid = false;
                //this.userNameValidationMsg = "UserName should start with Alphabets !";
                $("commentsEdit-validation").html(
                    "comments should start with Alphabets !"
                );
            }
        }
    });

    //for edit row functionality
    $(".editEmp").click(function () {
        $("#empModal").modal("show");
    });
    $(".performanceEdit").click(function () {
        $("#editPer").modal("show");
    });

    $(".projectEdit").click(function () {
        $("#editProject").modal("show");
    });
    $(".projectEditBtn").click(function () {
        $("#exampleModal2").modal("show");
    });

    $(".perEditBtn").click(function () {
        $("#exampleModal3").modal("show");
    });

    //for close modal
    //for edit row functionality
    $(".cancleEmpModal").click(function () {
        $("#editProject").modal("hide");
    });
    $(".cancleEditPer").click(function () {
        $("#editPer").modal("hide");
    });

    $(".cancleEditPer").click(function () {
        $("#editProject").modal("hide");
    });
    $(".cancleBtnproject").click(function () {
        $("#exampleModal2").modal("hide");
    });
    $(".cancleBtnMain").click(function () {
        $("#exampleModal").modal("hide");
    });
    $(".cancleEditMain").click(function () {
        $("#empModal").modal("hide");
    });
    $(".BtnCanclePer").click(function () {
        $("#exampleModal3").modal("hide");
    });

    //added by pruthvi

    // Array holding selected row IDs
    var rows_selected = [];
    var table = $("#performanaceDetailsTb").DataTable({
        ajax: "",
        columnDefs: [
            {
                targets: 0,
                searchable: false,
                orderable: false,
                width: "1%",
                className: "dt-body-center",
                render: function (data, type, full, meta) {
                    return '<input type="checkbox">';
                },
            },
        ],
        order: [1, "asc"],
        rowCallback: function (row, data, dataIndex) {
            // Get row ID
            var rowId = data[0];

            // If row ID is in the list of selected row IDs
            if ($.inArray(rowId, rows_selected) !== -1) {
                $(row).find('input[type="checkbox"]').prop("checked", true);
                $(row).addClass("selected");
            }
        },
    });

    // Handle click on checkbox
    $("#performanaceDetailsTb tbody").on(
        "click",
        'input[type="checkbox"]',
        function (e) {
            // Update state of "Select all" control
            updateDataTableSelectAllCtrl(table);

            // Prevent click event from propagating to parent
            e.stopPropagation();
        }
    );

    // Handle click on table cells with checkboxes

    // Handle click on "Select all" control
    $('thead input[name="select_all"]', table.table().container()).on(
        "click",
        function (e) {
            if (this.checked) {
                $(
                    '#performanaceDetailsTb tbody input[type="checkbox"]:not(:checked)'
                ).trigger("click");
            } else {
                $(
                    '#performanaceDetailsTb tbody input[type="checkbox"]:checked'
                ).trigger("click");
            }

            // Prevent click event from propagating to parent
            e.stopPropagation();
        }
    );

    // Handle table draw event
    table.on("draw", function () {
        // Update state of "Select all" control
        updateDataTableSelectAllCtrl(table);
    });

    // Handle form submission event
    $("#frm-performanaceDetailsTb").on("submit", function (e) {
        var form = this;

        // Iterate over all selected checkboxes
        $.each(rows_selected, function (index, rowId) {
            // Create a hidden element
            $(form).append(
                $("<input>")
                    .attr("type", "hidden")
                    .attr("name", "id[]")
                    .val(rowId)
            );
        });
        // Output form data to a console
        $("#performanaceDetailsTb-console").text($(form).serialize());
        console.log("Form submission", $(form).serialize());

        // Remove added elements
        $('input[name="id[]"]', form).remove();

        // Prevent actual form submission
        e.preventDefault();
    });

    //for project deatils

    // Array holding selected row IDs
    var rows_selected = [];
    var table = $("#projectDetailsTb").DataTable({
        ajax: "",
        columnDefs: [
            {
                targets: 0,
                searchable: false,
                orderable: false,
                width: "1%",
                className: "dt-body-center",
                render: function (data, type, full, meta) {
                    return '<input type="checkbox">';
                },
            },
        ],
        order: [1, "asc"],
        rowCallback: function (row, data, dataIndex) {
            // Get row ID
            var rowId = data[0];

            // If row ID is in the list of selected row IDs
            if ($.inArray(rowId, rows_selected) !== -1) {
                $(row).find('input[type="checkbox"]').prop("checked", true);
                $(row).addClass("selected");
            }
        },
    });

    // Handle click on checkbox
    $("#performanaceDetailsTb tbody").on(
        "click",
        'input[type="checkbox"]',
        function (e) {
            // Update state of "Select all" control
            updateDataTableSelectAllCtrl(table);

            // Prevent click event from propagating to parent
            e.stopPropagation();
        }
    );

    // Handle click on table cells with checkboxes

    // Handle click on "Select all" control
    $('thead input[name="select_all"]', table.table().container()).on(
        "click",
        function (e) {
            if (this.checked) {
                $(
                    '#performanaceDetailsTb tbody input[type="checkbox"]:not(:checked)'
                ).trigger("click");
            } else {
                $(
                    '#performanaceDetailsTb tbody input[type="checkbox"]:checked'
                ).trigger("click");
            }

            // Prevent click event from propagating to parent
            e.stopPropagation();
        }
    );

    //for edit project deatails

    // Array holding selected row IDs
    var rows_selected = [];
    var table = $("#dtBasicExample2Edit").DataTable({
        ajax: "",
        columnDefs: [
            {
                targets: 0,
                searchable: false,
                orderable: false,
                width: "1%",
                className: "dt-body-center",
                render: function (data, type, full, meta) {
                    return '<input type="checkbox">';
                },
            },
        ],
        order: [1, "asc"],
        rowCallback: function (row, data, dataIndex) {
            // Get row ID
            var rowId = data[0];

            // If row ID is in the list of selected row IDs
            if ($.inArray(rowId, rows_selected) !== -1) {
                $(row).find('input[type="checkbox"]').prop("checked", true);
                $(row).addClass("selected");
            }
        },
    });

    // Handle click on checkbox
    $("#dtBasicExample2Edit tbody").on(
        "click",
        'input[type="checkbox"]',
        function (e) {
            // Update state of "Select all" control
            updateDataTableSelectAllCtrl(table);

            // Prevent click event from propagating to parent
            e.stopPropagation();
        }
    );

    // Handle click on table cells with checkboxes

    // Handle click on "Select all" control
    $('thead input[name="select_all"]', table.table().container()).on(
        "click",
        function (e) {
            if (this.checked) {
                $(
                    '#dtBasicExample2Edit tbody input[type="checkbox"]:not(:checked)'
                ).trigger("click");
            } else {
                $(
                    '#dtBasicExample2Edit tbody input[type="checkbox"]:checked'
                ).trigger("click");
            }

            // Prevent click event from propagating to parent
            e.stopPropagation();
        }
    );

    // Handle table draw event
    table.on("draw", function () {
        // Update state of "Select all" control
        updateDataTableSelectAllCtrl(table);
    });

    // Handle form submission event
    $("#frm-dtBasicExample2Edit").on("submit", function (e) {
        var form = this;

        // Iterate over all selected checkboxes
        $.each(rows_selected, function (index, rowId) {
            // Create a hidden element
            $(form).append(
                $("<input>")
                    .attr("type", "hidden")
                    .attr("name", "id[]")
                    .val(rowId)
            );
        });
        // Output form data to a console
        $("#performanaceDetailsTb-console").text($(form).serialize());
        console.log("Form submission", $(form).serialize());

        // Remove added elements
        $('input[name="id[]"]', form).remove();

        // Prevent actual form submission
        e.preventDefault();
    });

    //for edit performance deatails

    // Array holding selected row IDs
    var rows_selected = [];
    var table = $("#dtBasicExample3").DataTable({
        ajax: "",
        columnDefs: [
            {
                targets: 0,
                searchable: false,
                orderable: false,
                width: "1%",
                className: "dt-body-center",
                render: function (data, type, full, meta) {
                    return '<input type="checkbox">';
                },
            },
        ],
        order: [1, "asc"],
        rowCallback: function (row, data, dataIndex) {
            // Get row ID
            var rowId = data[0];

            // If row ID is in the list of selected row IDs
            if ($.inArray(rowId, rows_selected) !== -1) {
                $(row).find('input[type="checkbox"]').prop("checked", true);
                $(row).addClass("selected");
            }
        },
    });

    // Handle click on checkbox
    $("#dtBasicExample3 tbody").on(
        "click",
        'input[type="checkbox"]',
        function (e) {
            // Update state of "Select all" control
            updateDataTableSelectAllCtrl(table);

            // Prevent click event from propagating to parent
            e.stopPropagation();
        }
    );

    // Handle click on table cells with checkboxes

    // Handle click on "Select all" control
    $('thead input[name="select_all"]', table.table().container()).on(
        "click",
        function (e) {
            if (this.checked) {
                $(
                    '#dtBasicExample3 tbody input[type="checkbox"]:not(:checked)'
                ).trigger("click");
            } else {
                $(
                    '#dtBasicExample3 tbody input[type="checkbox"]:checked'
                ).trigger("click");
            }

            // Prevent click event from propagating to parent
            e.stopPropagation();
        }
    );

    // Handle table draw event
    table.on("draw", function () {
        // Update state of "Select all" control
        updateDataTableSelectAllCtrl(table);
    });

    // Handle form submission event
    $("#frm-dtBasicExample3").on("submit", function (e) {
        var form = this;

        // Iterate over all selected checkboxes
        $.each(rows_selected, function (index, rowId) {
            // Create a hidden element
            $(form).append(
                $("<input>")
                    .attr("type", "hidden")
                    .attr("name", "id[]")
                    .val(rowId)
            );
        });
        // Output form data to a console
        $("#dtBasicExample3-console").text($(form).serialize());
        console.log("Form submission", $(form).serialize());

        // Remove added elements
        $('input[name="id[]"]', form).remove();

        // Prevent actual form submission
        e.preventDefault();
    });

    // get selected row of parent table
    // display selected row data in text input

    var table = document.getElementById("table"),
        rIndex;

    for (var i = 1; i < table.rows.length; i++) {
        table.rows[i].onclick = function () {
            rIndex = this.rowIndex;
            console.log(rIndex);

            document.getElementById(
                "empNameEdit"
            ).value = this.cells[1].innerHTML;
            document.getElementById(
                "empDesignEdit"
            ).value = this.cells[2].innerHTML;
            document.getElementById(
                "empExpEdit"
            ).value = this.cells[3].innerHTML;
            document.getElementById(
                "empContactEdit"
            ).value = this.cells[4].innerHTML;
        };
    }

    // get selected row of project table for edit functionality
    // display selected row data in text input

    var table = document.getElementById("dtBasicExample2Edit"),
        rIndex;

    for (var i = 1; i < table.rows.length; i++) {
        table.rows[i].onclick = function () {
            rIndex = this.rowIndex;
            document.getElementById(
                "projectCodeEdit"
            ).value = this.cells[1].innerHTML;
            document.getElementById(
                "projectCodeEdit"
            ).value = this.cells[1].innerHTML;
            document.getElementById(
                "projectNameEdit"
            ).value = this.cells[2].innerHTML;
            document.getElementById(
                "technologyEdit"
            ).value = this.cells[3].innerHTML;
            document.getElementById(
                "durationEdit"
            ).value = this.cells[4].innerHTML;
            document.getElementById(
                "statusEdit"
            ).value = this.cells[5].innerHTML;
        };
    }

    // get selected row of performance table for edit functionality
    // display selected row data in text input

    var table = document.getElementById("dtBasicExample3"),
        rIndex;

    for (var i = 1; i < table.rows.length; i++) {
        table.rows[i].onclick = function () {
            rIndex = this.rowIndex;
            document.getElementById("yearEdit").value = this.cells[1].innerHTML;
            document.getElementById(
                "gradeEdit"
            ).value = this.cells[2].innerHTML;
            document.getElementById(
                "commentsEdit"
            ).value = this.cells[3].innerHTML;
        };
    }
    // display selected row data in text input

    var table = document.getElementById("projectDetailsTb"),
        rIndex;

    for (var i = 1; i < table.rows.length; i++) {
        table.rows[i].onclick = function () {
            rIndex = this.rowIndex;
            document.getElementById(
                "projectCode"
            ).value = this.cells[1].innerHTML;
            document.getElementById(
                "projectCode"
            ).value = this.cells[1].innerHTML;
            document.getElementById(
                "projectName"
            ).value = this.cells[2].innerHTML;
            document.getElementById(
                "technology"
            ).value = this.cells[3].innerHTML;
            document.getElementById("duration").value = this.cells[4].innerHTML;
            document.getElementById("status").value = this.cells[5].innerHTML;
        };
    }
    // display selected row data in text input

    var table = document.getElementById("performanaceDetailsTb"),
        rIndex;

    for (var i = 1; i < table.rows.length; i++) {
        table.rows[i].onclick = function () {
            rIndex = this.rowIndex;
            document.getElementById("year").value = this.cells[1].innerHTML;
            document.getElementById("grade").value = this.cells[2].innerHTML;
            document.getElementById("comments").value = this.cells[3].innerHTML;
        };
    }
});

$(document).ready(function () {
    $("#exportTable").click(function (e) {
        var a = document.createElement("a");
        var data_type = "data:application/vnd.ms-excel;charset=utf-8";
        var table_html = $("#table")[0].outerHTML;
        table_html = table_html.replace(/<tfoot[\s\S.]*tfoot>/gim, "");
        var css_html =
            "<style>td {border: 0.5pt solid #c0c0c0} .tRight { text-align:right} .tLeft { text-align:left} </style>";
        a.href =
            data_type +
            "," +
            encodeURIComponent(
                "<html><head>" +
                    css_html +
                    "</" +
                    "head><body>" +
                    table_html +
                    "</body></html>"
            );
        a.download = "Employee_table.xls";
        a.click();
        e.preventDefault();
    });

    $("#exportProject").click(function (e) {
        var a = document.createElement("a");
        var data_type = "data:application/vnd.ms-excel;charset=utf-8";
        var table_html = $("#projectDetailsTb")[0].outerHTML;
        table_html = table_html.replace(/<tfoot[\s\S.]*tfoot>/gim, "");
        var css_html =
            "<style>td {border: 0.5pt solid #c0c0c0} .tRight { text-align:right} .tLeft { text-align:left} </style>";
        a.href =
            data_type +
            "," +
            encodeURIComponent(
                "<html><head>" +
                    css_html +
                    "</" +
                    "head><body>" +
                    table_html +
                    "</body></html>"
            );
        a.download = "ProjectDetails_table.xls";
        a.click();
        e.preventDefault();
    });

    $("#exportPer").click(function (e) {
        var a = document.createElement("a");
        var data_type = "data:application/vnd.ms-excel;charset=utf-8";
        var table_html = $("#performanaceDetailsTb")[0].outerHTML;
        table_html = table_html.replace(/<tfoot[\s\S.]*tfoot>/gim, "");
        var css_html =
            "<style>td {border: 0.5pt solid #c0c0c0} .tRight { text-align:right} .tLeft { text-align:left} </style>";
        a.href =
            data_type +
            "," +
            encodeURIComponent(
                "<html><head>" +
                    css_html +
                    "</" +
                    "head><body>" +
                    table_html +
                    "</body></html>"
            );
        a.download = "PerformanceDetails_table.xls";
        a.click();
        e.preventDefault();
    });
    $("#exportEditPer").click(function (e) {
        var a = document.createElement("a");
        var data_type = "data:application/vnd.ms-excel;charset=utf-8";
        var table_html = $("#dtBasicExample3")[0].outerHTML;
        table_html = table_html.replace(/<tfoot[\s\S.]*tfoot>/gim, "");
        var css_html =
            "<style>td {border: 0.5pt solid #c0c0c0} .tRight { text-align:right} .tLeft { text-align:left} </style>";
        a.href =
            data_type +
            "," +
            encodeURIComponent(
                "<html><head>" +
                    css_html +
                    "</" +
                    "head><body>" +
                    table_html +
                    "</body></html>"
            );
        a.download = "PerformanceDetails_table.xls";
        a.click();
        e.preventDefault();
    });

    $("#exportEditProject").click(function (e) {
        var a = document.createElement("a");
        var data_type = "data:application/vnd.ms-excel;charset=utf-8";
        var table_html = $("#dtBasicExample2Edit")[0].outerHTML;
        table_html = table_html.replace(/<tfoot[\s\S.]*tfoot>/gim, "");
        var css_html =
            "<style>td {border: 0.5pt solid #c0c0c0} .tRight { text-align:right} .tLeft { text-align:left} </style>";
        a.href =
            data_type +
            "," +
            encodeURIComponent(
                "<html><head>" +
                    css_html +
                    "</" +
                    "head><body>" +
                    table_html +
                    "</body></html>"
            );
        a.download = "ProjectDetails_table.xls";
        a.click();
        e.preventDefault();
    });
});
