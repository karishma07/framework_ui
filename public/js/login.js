$(document).ready(function () {
    $(".form-group .form-control").blur(function() {
        if ($(this).val() != "") {
            $(this).siblings(".form-label").addClass("active");
        } else {
            $(this).siblings(".form-label").removeClass("active");
        }
    });
    document.getElementById("btnLogin").disabled = true;
    $("#btnLogin").addClass("not-allowed");
    var loggedInUserID = localStorage.getItem("loggedInUserId");
    // console.log("UserID==>" + loggedInUserID);
    $("#togglePassword").click(function () {
        if ($("#password").attr("type") == "password") {
            $("#password").attr("type", "text");
        } else {
            $("#password").attr("type", "password");
        }
        //var passwordField = document.getElementById("password");
    });
    $("#userName").keyup(function () {
        console.log("keyup");
        const userNamePattern = /^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+$/;
        var username = $("#userName").val();
        if (username.length == 0) {
            $("#username-validation").html(Username_required);
            document.getElementById("btnLogin").disabled = true;
            $("#btnLogin").addClass("not-allowed");
        } else {
            if (userNamePattern.test(username)) {
                $("#username-validation").html("");
                $("#btnLogin").css("cursor", "pointer!important");
            } else {
                $("#username-validation").html(Username_contain);
            }
        }
    });
    $("#password").keyup(function () {
        const passwordPattern = /^(?=.*[A-Z])(?=.*[a-z])(?=.*\d)(?=.*[^A-Za-z0-9]).{8,}$/;
        var password = $("#password").val();
        var username = $("#userName").val();
        if (username != "" && password != "") {
            $("#btnLogin").removeClass("not-allowed");
            document.getElementById("btnLogin").disabled = false;
        }
        if (password.length == 0) {
            $("#password-validation").html(Password_required);
            document.getElementById("btnLogin").disabled = true;
            $("#btnLogin").addClass("not-allowed");
        }
            else {
                $("#password-validation").html("");
                $("#btnLogin").css("cursor", "pointer!important");
            }
        
    });
    $("#btnLogin").click(function () {
        var username = $("#userName").val();
        var password = $("#password").val();
        if (username != "" && password != "") {
            var reqData = {
                username: username,
                password: password,
            };
            document.getElementById("btnLogin").disabled = false;
            $("#btnLogin").removeClass("not-allowed");
            $.ajax({
                url: `${baseURL}/api/login/user`,
                method: "POST",
                data: reqData,
                type: "json",
                success: function (response, textStatus, jqXHR) {
                    console.log("token==>" + JSON.stringify(response));
                    //alert("success");
                    if (response != null) {
                        if (jqXHR.status == 200) {
                            if ($("#checkbox2").is(":checked")) {
                                localStorage.setItem(
                                    "loggedInUserId",
                                    response.data.id
                                );
                            } else {
                                localStorage.loggedInUserId = "";
                            }
                            sessionStorage.clear();
                            sessionStorage.setItem(
                                "access_token",
                                response.access_token
                            );
                            sessionStorage.setItem("isAuthenticated", "TRUE");
                            sessionStorage.setItem(
                                "loggedInUserId",
                                response.data.id
                            );
                            window.location.href = `${baseURL}/dashboard`;
                        }
                    }
                },
                error: function (xhr, textStatus, err) {
                    // alert("err");
                    console.log("xhr: ", xhr);
                    console.log("Error while fetching the preview data: ", err);
                    if (xhr.status == 400) {
                        Swal.fire({
                            title: Error,
                            text: Invalid_Credentials,
                            icon: "error",
                            confirmButtonText: Ok,
                        });
                    } else if (xhr.status == 404) {
                        Swal.fire({
                            title: Error,
                            text: pls_chk_api_url,
                            icon: "error",
                            confirmButtonText: Ok,
                        });
                    } else if (xhr.status == 500) {
                        Swal.fire({
                            title: Error,
                            text: Something_Went_Wrong_Exception_Occured,
                            icon: "error",
                            confirmButtonText: Ok,
                        });
                    }
                },
            });
        } else {
         
            //alert("please eneter details!");
            $("#username-validation").html(Username_required);
            $("#password-validation").html(Password_required);
            document.getElementById("btnLogin").disabled = true;
            $("#btnLogin").addClass("not-allowed");
            

        }
    });
});
function logout() {
    window.location.href = `${baseURL}`;
    sessionStorage.clear();
}
