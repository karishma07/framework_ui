
$(document).ready(function() {
     
    $("#exportTable").click(function(e) {
        var a = document.createElement('a');
        var data_type = 'data:application/vnd.ms-excel;charset=utf-8';
        var table_html = $('#table')[0].outerHTML;
        table_html = table_html.replace(/<tfoot[\s\S.]*tfoot>/gmi, '');
        var css_html =
            '<style>td {border: 0.5pt solid #c0c0c0} .tRight { text-align:right} .tLeft { text-align:left} </style>';
        a.href = data_type + ',' + encodeURIComponent('<html><head>' + css_html + '</' + 'head><body>' +
            table_html + '</body></html>');
        a.download = 'Manage_roles_table.xls';
        a.click();
        e.preventDefault();
    });
});