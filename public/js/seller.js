
$(document).ready(function() {
    //validation for employee name
    $("#sellerName").keyup(function () {
        var username = $("#sellerName").val();
        const userNamePattern = /^[A-Za-z]+.*$/;
        this.userNameValidationMsg = "";
        if (username.length == 0) {
            //this.userNameValidationMsg = "UserName is required !";
            $("#sellerName-validation").html("Name is not valid !");
            this.isUserNameValid = false;
        } else {
            if (userNamePattern.test(username)) {
                $("#sellerName-validation").html("");
                this.isUserNameValid = true;
            } else {
                this.isUserNameValid = false;
                //this.userNameValidationMsg = "UserName should start with Alphabets !";
                $("sellerName-validation").html("UserName should start with Alphabets !");
            }
        }
    });
    //for edit functionality
    
    
    //validation for employee name
    $("#sellerNameEdit").keyup(function () {
        var username = $("#sellerNameEdit").val();
        const userNamePattern = /^[A-Za-z]+.*$/;
        this.userNameValidationMsg = "";
        if (username.length == 0) {
            //this.userNameValidationMsg = "UserName is required !";
            $("#sellerNameEdit-validation").html("Name is not valid !");
            this.isUserNameValid = false;
        } else {
            if (userNamePattern.test(username)) {
                $("#sellerNameEdit-validation").html("");
                this.isUserNameValid = true;
            } else {
                this.isUserNameValid = false;
                //this.userNameValidationMsg = "UserName should start with Alphabets !";
                $("sellerNameEdit-validation").html("UserName should start with Alphabets !");
            }
        }
    });
    
    
    //VALIDATION FOR CONTACT
    
    $("#sellerContact").keyup(function (event) {
        let val = event.target.value;
        console.log("val==>", val);
        let contact = $("#sellerContact").val();
        const contactPattern = /^[0-9]+$/;
        if (!contactPattern.test(val)) {
            contact = contact.substring(0, contact.length - 1);
            event.target.value = contact;
        }
    
        if (contact.length == 0) {
            $("#sellerContact-validation").html("Contact is not valid!");
        } else {
            if (contactPattern.test(contact)) {
                $("#sellerContact-validation").html("");
                return true;
            } else {tact
    
                $("#sellerContact-validation").html("Contact is not valid!");
                return false;
            }
        }
    });
    //validations for contact for edit functionality
    $("#sellerContactEdit").keyup(function (event) {
        let val = event.target.value;
        console.log("val==>", val);
        let contact = $("#sellerContactEdit").val();
        const contactPattern = /^[0-9]+$/;
        if (!contactPattern.test(val)) {
            contact = contact.substring(0, contact.length - 1);
            event.target.value = contact;
        }
    
        if (contact.length == 0) {
            $("#sellerContactEdit-validation").html("Contact is not valid!");
        } else {
            if (contactPattern.test(contact)) {
                $("#sellerContactEdit-validation").html("");
                return true;
            } else {tact
    
                $("#sellerContactEdit-validation").html("Contact is not valid!");
                return false;
            }
        }
    });
    
    $("#sale").keyup(function () {
        var username = $("#sale").val();
        const userNamePattern = /^[A-Za-z]+.*$/;
        this.userNameValidationMsg = "";
        if (username.length == 0) {
            //this.userNameValidationMsg = "UserName is required !";
            $("#sale-validation").html("Name is not valid !");
            this.isUserNameValid = false;
        } else {
            if (userNamePattern.test(username)) {
                $("#sale-validation").html("");
                this.isUserNameValid = true;
            } else {
                this.isUserNameValid = false;
                //this.userNameValidationMsg = "UserName should start with Alphabets !";
                $("sale-validation").html("UserName should start with Alphabets !");
            }
        }
    });
    $("#productNameSale").keyup(function () {
        var username = $("#productNameSale").val();
        const userNamePattern = /^[A-Za-z]+.*$/;
        this.userNameValidationMsg = "";
        if (username.length == 0) {
            //this.userNameValidationMsg = "UserName is required !";
            $("#productNameSale-validation").html("Product Name is not valid !");
            this.isUserNameValid = false;
        } else {
            if (userNamePattern.test(username)) {
                $("#productNameSale-validation").html("");
                this.isUserNameValid = true;
            } else {
                this.isUserNameValid = false;
                //this.userNameValidationMsg = "UserName should start with Alphabets !";
                $("productNameSale-validation").html("UserName should start with Alphabets !");
            }
        }
    });
    
    //validation for status
    
    $(function () {
    
        //project name
    $("#BtnSave").click(function () {
    
        var projectName = $("#productName");
        if ((projectName.val() == "")) {
            //If the "Please Select" option is selected display error.
            $("#productName-validation").html("Product Name is required!");
            return false;
        }
        $("#productName-validation").html("");
        return true;
        });
        $("#BtnSave").click(function () {
            //project code
            var projectCode = $("#product_category");
            if ((projectCode.val() == "")) {
                //If the "Please Select" option is selected display error.
                $("#product_category-validation").html("Product category  is required!");
                return false;
            }
            $("#product_category-validation").html("");
            return true;
    
            
        });
    
    
        //grade
        $("#BtnSavePer").click(function () {
    
            var grade = $("#sale");
            if ((grade.val() == "")) {
                //If the "Please Select" option is selected display error.
                $("#sale-validation").html("Sale is required!");
                return false;
            }
            $("#sale-validation").html("");
            return true;
            });

            //grade
        $("#BtnSavePer").click(function () {
    
            var grade = $("#productName");
            if ((grade.val() == "")) {
                //If the "Please Select" option is selected display error.
                $("#productName-validation").html("Product Name is required!");
                return false;
            }
            $("#productName-validation").html("");
            return true;
            });
            $("#BtnSave").click(function () {
                //project code
                var projectCode = $("#projectCode");
                if ((projectCode.val() == "")) {
                    //If the "Please Select" option is selected display error.
                    $("#projectCode-validation").html("Project code is required!");
                    return false;
                }
                $("#projectCode-validation").html("");
                return true;
        
                
            });

            $("#productName").keyup(function () {
                var username = $("#productName").val();
                const userNamePattern = /^[A-Za-z]+.*$/;
                this.userNameValidationMsg = "";
                if (username.length == 0) {
                    //this.userNameValidationMsg = "UserName is required !";
                    $("#productName-validation").html("Product Name is not valid !");
                    this.isUserNameValid = false;
                } else {
                    if (userNamePattern.test(username)) {
                        $("#productName-validation").html("");
                        this.isUserNameValid = true;
                    } else {
                        this.isUserNameValid = false;
                        //this.userNameValidationMsg = "UserName should start with Alphabets !";
                        $("productName-validation").html("UserName should start with Alphabets !");
                    }
                }
            });

              //validation for edit functionality
    
 
    
    $(function () {
    
        //project name
    $("#BtnSaveEdit").click(function () {
    
        var projectName = $("#productNameEditSale");
        if ((projectName.val() == "")) {
            //If the "Please Select" option is selected display error.
            $("#productNameEditSale-validation").html("Product Name is required!");
            return false;
        }
        $("#productNameEditSale-validation").html("");
        return true;
        });
        $("#BtnSaveEdit").click(function () {
            //project code
            var projectCode = $("#productCategoryEdit");
            if ((projectCode.val() == "")) {
                //If the "Please Select" option is selected display error.
                $("#productCategoryEdit-validation").html("Product category is required!");
                return false;
            }
            $("#productCategoryEdit-validation").html("");
            return true;
    
            
        });
    
              //grade
        $("#BtnSavePerEdit").click(function () {
    
            var grade = $("#gradeEdit");
            if ((grade.val() == "")) {
                //If the "Please Select" option is selected display error.
                $("#gradeEdit-validation").html("Grade is required!");
                return false;
            }
            $("#gradeEdit-validation").html("");
            return true;
            });
            $("#BtnSaveEdit").click(function () {
                //project code
                var projectCode = $("#projectCodeEdit");
                if ((projectCode.val() == "")) {
                    //If the "Please Select" option is selected display error.
                    $("#projectCodeEdit-validation").html("Project code is required!");
                    return false;
                }
                $("#projectCodeEdit-validation").html("");
                return true;
        
                
            });
    
              $('#product_category').change(function() {
                $("#product_category-validation").html("");
              });
    
              $('#productCategoryEdit').change(function() {
                $("#productCategoryEdit-validation").html("");
              });
    
              $('#projectCodeEdit').change(function() {
                $("#projectCodeEdit-validation").html("");
              });
              $('#projectNameEdit').change(function() {
                $("#projectNameEdit-validation").html("");
              });
            });
              //edit function end here

    
  

    
        
    });
    $("#saveCloseBtn").click(function () {
        var username = $("#sellerName").val();
        const userNamePattern = /^[A-Za-z]+.*$/;
        this.userNameValidationMsg = "";
        if (username.length == 0) {
            //this.userNameValidationMsg = "UserName is required !";
            $("#sellerName-validation").html("Name is not valid !");
            this.isUserNameValid = false;
        } else {
            if (userNamePattern.test(username)) {
                $("#sellerName-validation").html("");
                this.isUserNameValid = true;
            } else {
                this.isUserNameValid = false;
                //this.userNameValidationMsg = "UserName should start with Alphabets !";
                $("sellerName-validation").html("UserName should start with Alphabets !");
            }
        }
        let val = event.target.value;
        console.log("val==>", val);
        let contact = $("#sellerContact").val();
        const contactPattern = /^[0-9]+$/;
        if (!contactPattern.test(val)) {
            contact = contact.substring(0, contact.length - 1);
            event.target.value = contact;
        }
    
        if (contact.length == 0) {
            $("#sellerContact-validation").html("Contact is not valid!");
        } else {
            if (contactPattern.test(contact)) {
                $("#sellerContact-validation").html("");
                return true;
            } else {tact
    
                $("#sellerContact-validation").html("Contact is not valid!");
                return false;
            }
        }
    
    });
    $("#saveNewBtn").click(function () {
        var username = $("#sellerName").val();
        const userNamePattern = /^[A-Za-z]+.*$/;
        this.userNameValidationMsg = "";
        if (username.length == 0) {
            //this.userNameValidationMsg = "UserName is required !";
            $("#sellerName-validation").html("Name is not valid !");
            this.isUserNameValid = false;
        } else {
            if (userNamePattern.test(username)) {
                $("#sellerName-validation").html("");
                this.isUserNameValid = true;
            } else {
                this.isUserNameValid = false;
                //this.userNameValidationMsg = "UserName should start with Alphabets !";
                $("sellerName-validation").html("UserName should start with Alphabets !");
            }
        }
        let val = event.target.value;
        console.log("val==>", val);
        let contact = $("#sellerContact").val();
        const contactPattern = /^[0-9]+$/;
        if (!contactPattern.test(val)) {
            contact = contact.substring(0, contact.length - 1);
            event.target.value = contact;
        }
    
        if (contact.length == 0) {
            $("#sellerContact-validation").html("Contact is not valid!");
        } else {
            if (contactPattern.test(contact)) {
                $("#sellerContact-validation").html("");
                return true;
            } else {tact
    
                $("#sellerContact-validation").html("Contact is not valid!");
                return false;
            }
        }
    
    });
    
    //validation for edit functionality
    
    $("#saveCloseBtnEdit").click(function () {
        var username = $("#sellerNameEdit").val();
        const userNamePattern = /^[A-Za-z]+.*$/;
        this.userNameValidationMsg = "";
        if (username.length == 0) {
            //this.userNameValidationMsg = "UserName is required !";
            $("#sellerNameEdit-validation").html("Name is not valid !");
            this.isUserNameValid = false;
        } else {
            if (userNamePattern.test(username)) {
                $("#sellerNameEdit-validation").html("");
                this.isUserNameValid = true;
            } else {
                this.isUserNameValid = false;
                //this.userNameValidationMsg = "UserName should start with Alphabets !";
                $("sellerNameEdit-validation").html("UserName should start with Alphabets !");
            }
        }
        let val = event.target.value;
        console.log("val==>", val);
        let contact = $("#sellerContact").val();
        const contactPattern = /^[0-9]+$/;
        if (!contactPattern.test(val)) {
            contact = contact.substring(0, contact.length - 1);
            event.target.value = contact;
        }
    
        if (contact.length == 0) {
            $("#sellerContactEdit-validation").html("Contact is not valid!");
        } else {
            if (contactPattern.test(contact)) {
                $("#sellerContactEdit-validation").html("");
                return true;
            } else {tact
    
                $("#sellerContactEdit-validation").html("Contact is not valid!");
                return false;
            }
        }
    
    });
    
    
    //validation for edit functionality
     //validation for edit functionality
    
     $("#saveNewBtnEdit").click(function () {
        var username = $("#sellerNameEdit").val();
        const userNamePattern = /^[A-Za-z]+.*$/;
        this.userNameValidationMsg = "";
        if (username.length == 0) {
            //this.userNameValidationMsg = "UserName is required !";
            $("#sellerNameEdit-validation").html("Name is not valid !");
            this.isUserNameValid = false;
        } else {
            if (userNamePattern.test(username)) {
                $("#sellerNameEdit-validation").html("");
                this.isUserNameValid = true;
            } else {
                this.isUserNameValid = false;
                //this.userNameValidationMsg = "UserName should start with Alphabets !";
                $("sellerNameEdit-validation").html("UserName should start with Alphabets !");
            }
        }
        let val = event.target.value;
        console.log("val==>", val);
        let contact = $("#sellerContact").val();
        const contactPattern = /^[0-9]+$/;
        if (!contactPattern.test(val)) {
            contact = contact.substring(0, contact.length - 1);
            event.target.value = contact;
        }
    
        if (contact.length == 0) {
            $("#sellerContactEdit-validation").html("Contact is not valid!");
        } else {
            if (contactPattern.test(contact)) {
                $("#sellerContactEdit-validation").html("");
                return true;
            } else {tact
    
                $("#sellerContactEdit-validation").html("Contact is not valid!");
                return false;
            }
        }
    
    });
    
    
    
    //validation for year
    
    $("#year").keyup(function (event) {
        let val = event.target.value;
        console.log("val==>", val);
        let year = $("#year").val();
        const yearPattern = /^[0-9]+$/;
        if (!yearPattern.test(val)) {
            year = year.substring(0, year.length - 1);
            event.target.value = year;
        }
    
        if (year.length == 0) {
            $("#year-validation").html("year is not valid!");
        } else {
            if (yearPattern.test(year)) {
                $("#year-validation").html("");
                return true;
            } else {tact
    
                $("#year-validation").html("year is not valid!");
                return false;
            }
        }
    });
    //validation forcomments
    
    $("#comments").keyup(function () {
        var comments = $("#comments").val();
        const commentsPattern = /^[A-Za-z]+.*$/;
        this.commentsValidationMsg = "";
        if (comments.length == 0) {
            //this.userNameValidationMsg = "UserName is required !";
            $("#comments-validation").html("comments is not valid !");
            this.iscommentsValid = false;
        } else {
            if (commentsPattern.test(comments)) {
                $("#comments-validation").html("");
                this.iscommentsValid = true;
            } else {
                this.iscommentsValid = false;
                //this.userNameValidationMsg = "UserName should start with Alphabets !";
                $("comments-validation").html("comments should start with Alphabets !");
            }
        }
    });
    
    //edit
    
    //validation for year
    
    $("#yearEdit").keyup(function (event) {
        let val = event.target.value;
        console.log("val==>", val);
        let year = $("#yearEdit").val();
        const yearPattern = /^[0-9]+$/;
        if (!yearPattern.test(val)) {
            year = year.substring(0, year.length - 1);
            event.target.value = year;
        }
    
        if (year.length == 0) {
            $("#yearEdit-validation").html("year is not valid!");
        } else {
            if (yearPattern.test(year)) {
                $("#yearEdit-validation").html("");
                return true;
            } else {tact
    
                $("#yearEdit-validation").html("year is not valid!");
                return false;
            }
        }
    });
    //validation forcomments
    
    $("#commentsEdit").keyup(function () {
        var comments = $("#commentsEdit").val();
        const commentsPattern = /^[A-Za-z]+.*$/;
        this.commentsValidationMsg = "";
        if (comments.length == 0) {
            //this.userNameValidationMsg = "UserName is required !";
            $("#commentsEdit-validation").html("comments is not valid !");
            this.iscommentsValid = false;
        } else {
            if (commentsPattern.test(comments)) {
                $("#commentsEdit-validation").html("");
                this.iscommentsValid = true;
            } else {
                this.iscommentsValid = false;
                //this.userNameValidationMsg = "UserName should start with Alphabets !";
                $("commentsEdit-validation").html("comments should start with Alphabets !");
            }
        }
    });
    $("#BtnSavePer").click(function () {
        let val = event.target.value;
        console.log("val==>", val);
        let year = $("#year").val();
        const yearPattern = /^[0-9]+$/;
        if (!yearPattern.test(val)) {
            year = year.substring(0, year.length - 1);
            event.target.value = year;
        }
    
        if (year.length == 0) {
            $("#year-validation").html("year is required!");
        } else {
            if (yearPattern.test(year)) {
                $("#year-validation").html("");
                return true;
            } else {tact
    
                $("#year-validation").html("year is required!");
                return false;
            }
        }
    
        var comments = $("#sale").val();
        const commentsPattern = /^[A-Za-z]+.*$/;
        this.commentsValidationMsg = "";
        if (comments.length == 0) {
            //this.userNameValidationMsg = "UserName is required !";
            $("#sale-validation").html("sale is not valid !");
            this.iscommentsValid = false;
        } else {
            if (commentsPattern.test(comments)) {
                $("#sale-validation").html("");
                this.iscommentsValid = true;
            } else {
                this.iscommentsValid = false;
                //this.userNameValidationMsg = "UserName should start with Alphabets !";
                $("sale-validation").html("sale should start with Alphabets !");
            }
        }
    });
        $("#BtnSavePer").click(function () {
        
        var productName = $("#productNameSale").val();
        const productNamePattern = /^[A-Za-z]+.*$/;
        this.productNameValidationMsg = "";
        if (productName.length == 0) {
            //this.userNameValidationMsg = "UserName is required !";
            $("#productNameSale-validation").html("Product Name required !");
            this.isproductNameValid = false;
        } else {
            if (productNamePattern.test(productName)) {
                $("#productNameSale-validation").html("");
                this.isproductNameValid = true;
            } else {
                this.isproductNameValid = false;
                //this.userNameValidationMsg = "UserName should start with Alphabets !";
                $("productNameSale-validation").html("sale should start with Alphabets !");
            }
        }
    });

    
    //validation for edit functionality
    $("#BtnSavePerEdit").click(function () {
        let val = event.target.value;
        console.log("val==>", val);
        let year = $("#yearEdit").val();
        const yearPattern = /^[0-9]+$/;
        if (!yearPattern.test(val)) {
            year = year.substring(0, year.length - 1);
            event.target.value = year;
        }
    
        if (year.length == 0) {
            $("#yearEdit-validation").html("year is required!");
        } else {
            if (yearPattern.test(year)) {
                $("#yearEdit-validation").html("");
                return true;
            } else {tact
    
                $("#yearEdit-validation").html("year is required!");
                return false;
            }
        }
                                   
        var productNameSaleEdit = $("#productNameSaleEdit").val();
        const productNameSaleEditPattern = /^[A-Za-z]+.*$/;
        this.commentsValidationMsg = "";
        if (productNameSaleEdit.length == 0) {
            //this.userNameValidationMsg = "UserName is required !";
            $("#productNameSaleEdit-validation").html("Product Name is required !");
            this.iscommentsValid = false;
        } else {
            if (productNameSaleEditPattern.test(productNameSaleEdit)) {
                $("#productNameSaleEdit-validation").html("");
                this.iscommentsValid = true;
            } else {
                this.iscommentsValid = false;
                //this.userNameValidationMsg = "UserName should start with Alphabets !";
                $("productNameSaleEdit-validation").html("comments should start with Alphabets !");
            }
        }

        var comments1 = $("#saleEdit").val();
        const commentsPattern1 = /^[A-Za-z]+.*$/;
        this.commentsValidationMsg = "";
        if (comments1.length == 0) {
            //this.userNameValidationMsg = "UserName is required !";
            $("#saleEdit-validation").html("sale is required !");
            this.iscommentsValid = false;
        } else {
            if (commentsPattern1.test(comments1)) {
                $("#saleEdit-validation").html("");
                this.iscommentsValid = true;
            } else {
                this.iscommentsValid = false;
                //this.userNameValidationMsg = "UserName should start with Alphabets !";
                $("saleEdit-validation").html("sale should start with Alphabets !");
            }
        }
    });
    
    
    $("#productNameEditSale").keyup(function () {
    var productNameSaleEdit = $("#productNameEditSale").val();
    const productNameSaleEditPattern = /^[A-Za-z]+.*$/;
    this.commentsValidationMsg = "";
    if (productNameSaleEdit.length == 0) {
        //this.userNameValidationMsg = "UserName is required !";
        $("#productNameEditSale-validation").html("Product Name is required !");
        this.iscommentsValid = false;
    } else {
        if (productNameSaleEditPattern.test(productNameSaleEdit)) {
            $("#productNameEditSale-validation").html("");
            this.iscommentsValid = true;
        } else {
            this.iscommentsValid = false;
            //this.userNameValidationMsg = "UserName should start with Alphabets !";
            $("productNameEditSale-validation").html("comments should start with Alphabets !");
        }
    }
});

$("#productNameSaleEdit").keyup(function () {
    var productNameSaleEdit = $("#productNameEditSale").val();
    const productNameSaleEditPattern = /^[A-Za-z]+.*$/;
    this.commentsValidationMsg = "";
    if (productNameSaleEdit.length == 0) {
        //this.userNameValidationMsg = "UserName is required !";
        $("#productNameSaleEdit-validation").html("Product Name is required !");
        this.iscommentsValid = false;
    } else {
        if (productNameSaleEditPattern.test(productNameSaleEdit)) {
            $("#productNameSaleEdit-validation").html("");
            this.iscommentsValid = true;
        } else {
            this.iscommentsValid = false;
            //this.userNameValidationMsg = "UserName should start with Alphabets !";
            $("productNameSaleEdit-validation").html("comments should start with Alphabets !");
        }
    }
});
$("#saleEdit").keyup(function () {
    var productNameSaleEdit = $("#saleEdit").val();
    const productNameSaleEditPattern = /^[A-Za-z]+.*$/;
    this.commentsValidationMsg = "";
    if (productNameSaleEdit.length == 0) {
        //this.userNameValidationMsg = "UserName is required !";
        $("#saleEdit-validation").html("Product Name is required !");
        this.iscommentsValid = false;
    } else {
        if (productNameSaleEditPattern.test(productNameSaleEdit)) {
            $("#saleEdit-validation").html("");
            this.iscommentsValid = true;
        } else {
            this.iscommentsValid = false;
            //this.userNameValidationMsg = "UserName should start with Alphabets !";
            $("saleEdit-validation").html("comments should start with Alphabets !");
        }
    }
});
    //for edit row functionality
    $(".editEmp").click(function () {
        $('#empModal').modal('show');
    });
    $(".performanceEdit").click(function () {
        $('#editPer').modal('show');
    });
    
    $(".projectEdit").click(function () {
        $('#editProject').modal('show');
    });
    $(".projectEditBtn").click(function () {
        $('#exampleModal2').modal('show');
    });
    
    $(".perEditBtn").click(function () {
        $('#exampleModal3').modal('show');
    });
    
    
    //for close modal
    //for edit row functionality
    $(".cancleEmpModal").click(function () {
        $('#editProject').modal('hide');
    });
    $(".cancleEditPer").click(function () {
        $('#editPer').modal('hide');
    });
    
    $(".cancleEditPer").click(function () {
        $('#editProject').modal('hide');
    });
    $(".cancleBtnproject").click(function () {
        $('#exampleModal2').modal('hide');
    });
    $(".cancleBtnMain").click(function () {
        $('#exampleModal').modal('hide');
    });
    $(".cancleEditMain").click(function () {
        $('#empModal').modal('hide');
    });
    $(".BtnCanclePer").click(function () {
        $('#exampleModal3').modal('hide');
    });
    
    //added by pruthvi
    
    // Array holding selected row IDs
    var rows_selected = [];
    var table = $('#performanaceDetailsTb').DataTable({
       'ajax': '',
       'columnDefs': [{
          'targets': 0,
          'searchable':false,
          'orderable':false,
          'width':'1%',
          'className': 'dt-body-center',
          'render': function (data, type, full, meta){
              return '<input type="checkbox">';
          }
       }],
       'order': [1, 'asc'],
       'rowCallback': function(row, data, dataIndex){
          // Get row ID
          var rowId = data[0];
    
          // If row ID is in the list of selected row IDs
          if($.inArray(rowId, rows_selected) !== -1){
             $(row).find('input[type="checkbox"]').prop('checked', true);
             $(row).addClass('selected');
          }
       }
    });
    
    // Handle click on checkbox
    $('#performanaceDetailsTb tbody').on('click', 'input[type="checkbox"]', function(e){
      
       // Update state of "Select all" control
       updateDataTableSelectAllCtrl(table);
    
       // Prevent click event from propagating to parent
       e.stopPropagation();
    });
    
    // Handle click on table cells with checkboxes
    
    // Handle click on "Select all" control
    $('thead input[name="select_all"]', table.table().container()).on('click', function(e){
       if(this.checked){
          $('#performanaceDetailsTb tbody input[type="checkbox"]:not(:checked)').trigger('click');
       } else {
          $('#performanaceDetailsTb tbody input[type="checkbox"]:checked').trigger('click');
       }
    
       // Prevent click event from propagating to parent
       e.stopPropagation();
    });
    
    // Handle table draw event
    table.on('draw', function(){
       // Update state of "Select all" control
       updateDataTableSelectAllCtrl(table);
    });
     
    // Handle form submission event 
    $('#frm-performanaceDetailsTb').on('submit', function(e){
       var form = this;
    
       // Iterate over all selected checkboxes
       $.each(rows_selected, function(index, rowId){
          // Create a hidden element 
          $(form).append(
              $('<input>')
                 .attr('type', 'hidden')
                 .attr('name', 'id[]')
                 .val(rowId)
          );
       });
       // Output form data to a console     
       $('#performanaceDetailsTb-console').text($(form).serialize());
       console.log("Form submission", $(form).serialize());
        
       // Remove added elements
       $('input[name="id\[\]"]', form).remove();
        
       // Prevent actual form submission
       e.preventDefault();
    });
    
    //for project deatils
    
    // Array holding selected row IDs
    var rows_selected = [];
    var table = $('#projectDetailsTb').DataTable({
       'ajax': '',
       'columnDefs': [{
          'targets': 0,
          'searchable':false,
          'orderable':false,
          'width':'1%',
          'className': 'dt-body-center',
          'render': function (data, type, full, meta){
              return '<input type="checkbox">';
          }
       }],
       'order': [1, 'asc'],
       'rowCallback': function(row, data, dataIndex){
          // Get row ID
          var rowId = data[0];
    
          // If row ID is in the list of selected row IDs
          if($.inArray(rowId, rows_selected) !== -1){
             $(row).find('input[type="checkbox"]').prop('checked', true);
             $(row).addClass('selected');
          }
       }
    });
    
    // Handle click on checkbox
    $('#performanaceDetailsTb tbody').on('click', 'input[type="checkbox"]', function(e){
      
       // Update state of "Select all" control
       updateDataTableSelectAllCtrl(table);
    
       // Prevent click event from propagating to parent
       e.stopPropagation();
    });
    
    // Handle click on table cells with checkboxes
    
    // Handle click on "Select all" control
    $('thead input[name="select_all"]', table.table().container()).on('click', function(e){
       if(this.checked){
          $('#performanaceDetailsTb tbody input[type="checkbox"]:not(:checked)').trigger('click');
       } else {
          $('#performanaceDetailsTb tbody input[type="checkbox"]:checked').trigger('click');
       }
    
       // Prevent click event from propagating to parent
       e.stopPropagation();
    });
    
    
    //for edit project deatails
    
    // Array holding selected row IDs
    var rows_selected = [];
    var table = $('#dtBasicExample2Edit').DataTable({
       'ajax': '',
       'columnDefs': [{
          'targets': 0,
          'searchable':false,
          'orderable':false,
          'width':'1%',
          'className': 'dt-body-center',
          'render': function (data, type, full, meta){
              return '<input type="checkbox">';
          }
       }],
       'order': [1, 'asc'],
       'rowCallback': function(row, data, dataIndex){
          // Get row ID
          var rowId = data[0];
    
          // If row ID is in the list of selected row IDs
          if($.inArray(rowId, rows_selected) !== -1){
             $(row).find('input[type="checkbox"]').prop('checked', true);
             $(row).addClass('selected');
          }
       }
    });
    
    // Handle click on checkbox
    $('#dtBasicExample2Edit tbody').on('click', 'input[type="checkbox"]', function(e){
      
       // Update state of "Select all" control
       updateDataTableSelectAllCtrl(table);
    
       // Prevent click event from propagating to parent
       e.stopPropagation();
    });
    
    // Handle click on table cells with checkboxes
    
    // Handle click on "Select all" control
    $('thead input[name="select_all"]', table.table().container()).on('click', function(e){
       if(this.checked){
          $('#dtBasicExample2Edit tbody input[type="checkbox"]:not(:checked)').trigger('click');
       } else {
          $('#dtBasicExample2Edit tbody input[type="checkbox"]:checked').trigger('click');
       }
    
       // Prevent click event from propagating to parent
       e.stopPropagation();
    });
    
    
    // Handle table draw event
    table.on('draw', function(){
       // Update state of "Select all" control
       updateDataTableSelectAllCtrl(table);
    });
     
    // Handle form submission event 
    $('#frm-dtBasicExample2Edit').on('submit', function(e){
       var form = this;
    
       // Iterate over all selected checkboxes
       $.each(rows_selected, function(index, rowId){
          // Create a hidden element 
          $(form).append(
              $('<input>')
                 .attr('type', 'hidden')
                 .attr('name', 'id[]')
                 .val(rowId)
          );
       });
       // Output form data to a console     
       $('#performanaceDetailsTb-console').text($(form).serialize());
       console.log("Form submission", $(form).serialize());
        
       // Remove added elements
       $('input[name="id\[\]"]', form).remove();
        
       // Prevent actual form submission
       e.preventDefault();
    });
    
    //for edit performance deatails
    
    // Array holding selected row IDs
    var rows_selected = [];
    var table = $('#dtBasicExample3').DataTable({
       'ajax': '',
       'columnDefs': [{
          'targets': 0,
          'searchable':false,
          'orderable':false,
          'width':'1%',
          'className': 'dt-body-center',
          'render': function (data, type, full, meta){
              return '<input type="checkbox">';
          }
       }],
       'order': [1, 'asc'],
       'rowCallback': function(row, data, dataIndex){
          // Get row ID
          var rowId = data[0];
    
          // If row ID is in the list of selected row IDs
          if($.inArray(rowId, rows_selected) !== -1){
             $(row).find('input[type="checkbox"]').prop('checked', true);
             $(row).addClass('selected');
          }
       }
    });
    
    // Handle click on checkbox
    $('#dtBasicExample3 tbody').on('click', 'input[type="checkbox"]', function(e){
      
       // Update state of "Select all" control
       updateDataTableSelectAllCtrl(table);
    
       // Prevent click event from propagating to parent
       e.stopPropagation();
    });
    
    // Handle click on table cells with checkboxes
    
    // Handle click on "Select all" control
    $('thead input[name="select_all"]', table.table().container()).on('click', function(e){
       if(this.checked){
          $('#dtBasicExample3 tbody input[type="checkbox"]:not(:checked)').trigger('click');
       } else {
          $('#dtBasicExample3 tbody input[type="checkbox"]:checked').trigger('click');
       }
    
       // Prevent click event from propagating to parent
       e.stopPropagation();
    });
    
    
    // Handle table draw event
    table.on('draw', function(){
       // Update state of "Select all" control
       updateDataTableSelectAllCtrl(table);
    });
     
    // Handle form submission event 
    $('#frm-dtBasicExample3').on('submit', function(e){
       var form = this;
    
       // Iterate over all selected checkboxes
       $.each(rows_selected, function(index, rowId){
          // Create a hidden element 
          $(form).append(
              $('<input>')
                 .attr('type', 'hidden')
                 .attr('name', 'id[]')
                 .val(rowId)
          );
       });
       // Output form data to a console     
       $('#dtBasicExample3-console').text($(form).serialize());
       console.log("Form submission", $(form).serialize());
        
       // Remove added elements
       $('input[name="id\[\]"]', form).remove();
        
       // Prevent actual form submission
       e.preventDefault();
    });
   
    });

    $(document).ready(function() {
        $("#exportTable").click(function(e) {
            var a = document.createElement('a');
            var data_type = 'data:application/vnd.ms-excel;charset=utf-8';
            var table_html = $('#table')[0].outerHTML;
            table_html = table_html.replace(/<tfoot[\s\S.]*tfoot>/gmi, '');
            var css_html =
                '<style>td {border: 0.5pt solid #c0c0c0} .tRight { text-align:right} .tLeft { text-align:left} </style>';
            a.href = data_type + ',' + encodeURIComponent('<html><head>' + css_html + '</' + 'head><body>' +
                table_html + '</body></html>');
            a.download = 'Seller_table.xls';
            a.click();
            e.preventDefault();
        });
    
        $("#exportProject").click(function(e) {
            var a = document.createElement('a');
            var data_type = 'data:application/vnd.ms-excel;charset=utf-8';
            var table_html = $('#projectDetailsTb')[0].outerHTML;
            table_html = table_html.replace(/<tfoot[\s\S.]*tfoot>/gmi, '');
            var css_html =
                '<style>td {border: 0.5pt solid #c0c0c0} .tRight { text-align:right} .tLeft { text-align:left} </style>';
            a.href = data_type + ',' + encodeURIComponent('<html><head>' + css_html + '</' + 'head><body>' +
                table_html + '</body></html>');
            a.download = 'ProductDetails_table.xls';
            a.click();
            e.preventDefault();
        });
    
    
        $("#exportPer").click(function(e) {
            var a = document.createElement('a');
            var data_type = 'data:application/vnd.ms-excel;charset=utf-8';
            var table_html = $('#performanaceDetailsTb')[0].outerHTML;
            table_html = table_html.replace(/<tfoot[\s\S.]*tfoot>/gmi, '');
            var css_html =
                '<style>td {border: 0.5pt solid #c0c0c0} .tRight { text-align:right} .tLeft { text-align:left} </style>';
            a.href = data_type + ',' + encodeURIComponent('<html><head>' + css_html + '</' + 'head><body>' +
                table_html + '</body></html>');
            a.download = 'Product_Sale_Details_table.xls';
            a.click();
            e.preventDefault();
        });
        $("#exportEditPer").click(function(e) {
            var a = document.createElement('a');
            var data_type = 'data:application/vnd.ms-excel;charset=utf-8';
            var table_html = $('#dtBasicExample3')[0].outerHTML;
            table_html = table_html.replace(/<tfoot[\s\S.]*tfoot>/gmi, '');
            var css_html =
                '<style>td {border: 0.5pt solid #c0c0c0} .tRight { text-align:right} .tLeft { text-align:left} </style>';
            a.href = data_type + ',' + encodeURIComponent('<html><head>' + css_html + '</' + 'head><body>' +
                table_html + '</body></html>');
            a.download = 'Product_Sale_Details_table.xls';
            a.click();
            e.preventDefault();
        });
    
        $("#exportEditProject").click(function(e) {
            var a = document.createElement('a');
            var data_type = 'data:application/vnd.ms-excel;charset=utf-8';
            var table_html = $('#dtBasicExample2Edit')[0].outerHTML;
            table_html = table_html.replace(/<tfoot[\s\S.]*tfoot>/gmi, '');
            var css_html =
                '<style>td {border: 0.5pt solid #c0c0c0} .tRight { text-align:right} .tLeft { text-align:left} </style>';
            a.href = data_type + ',' + encodeURIComponent('<html><head>' + css_html + '</' + 'head><body>' +
                table_html + '</body></html>');
            a.download = 'ProductDetails_table.xls';
            a.click();
            e.preventDefault();
        });
    

        // get selected row of parent table
                // display selected row data in text input
                
                var table = document.getElementById("table"),rIndex;
                
                for(var i = 1; i < table.rows.length; i++)
                {
                    table.rows[i].onclick = function()
                    {
                        rIndex = this.rowIndex;
                        console.log(rIndex);
                        
                        document.getElementById("sellerNameEdit").value = this.cells[1].innerHTML;
                        document.getElementById("sellerContactEdit").value = this.cells[2].innerHTML;
                    };
                }

                //display selected row data in text input
                
                var table = document.getElementById("dtBasicExample2Edit"),rIndex;
                
                for(var i = 1; i < table.rows.length; i++)
                {
                    table.rows[i].onclick = function()
                    {
                        
                        rIndex = this.rowIndex;
                        document.getElementById("productNameEditSale").value = this.cells[1].innerHTML; 
                        document.getElementById("productCategoryEdit").value = this.cells[2].innerHTML;
                       
                    };
                }

                 // display selected row data in text input
                
                 var table = document.getElementById("dtBasicExample3"),rIndex;
                
                 for(var i = 1; i < table.rows.length; i++)
                 {
                     table.rows[i].onclick = function()
                     {
                         
                         rIndex = this.rowIndex;
                         document.getElementById("productNameSaleEdit").value = this.cells[1].innerHTML; 
                         document.getElementById("yearEdit").value = this.cells[2].innerHTML;
                         document.getElementById("saleEdit").value = this.cells[3].innerHTML;
                        
                     };
                 }


                  // display selected row data in text input
                
      var table = document.getElementById("projectDetailsTb"),rIndex;
                
      for(var i = 1; i < table.rows.length; i++)
      {
          table.rows[i].onclick = function()
          {
              
              rIndex = this.rowIndex;
              document.getElementById("productName").value = this.cells[1].innerHTML; 
              document.getElementById("product_category").value = this.cells[1].innerHTML;
          };
      }
                 // display selected row data in text input
                
                 var table = document.getElementById("performanaceDetailsTb"),rIndex;
                
                 for(var i = 1; i < table.rows.length; i++)
                 {
                     table.rows[i].onclick = function()
                     {
                         
                         rIndex = this.rowIndex;
                         document.getElementById("productNameSale").value = this.cells[1].innerHTML; 
                         document.getElementById("year").value = this.cells[2].innerHTML;
                         document.getElementById("sale").value = this.cells[3].innerHTML;
                        
                     };
                 }

    });
    
