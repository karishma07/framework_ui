$(document).ready(function () {
    $("#togglePassword").click(function () {
        if ($("#password").attr("type") == "password") {
            $("#password").attr("type", "text");
        } else {
            $("#password").attr("type", "password");
        }
        //var passwordField = document.getElementById("password");
    });
    $("#togglePassword1").click(function () {
        if ($("#confirm_password").attr("type") == "password") {
            $("#confirm_password").attr("type", "text");
        } else {
            $("#confirm_password").attr("type", "password");
        }
        //var passwordField = document.getElementById("password");
    });
    var loggedInUserID = localStorage.getItem("loggedInUserId");
    //console.log("UserID==>" + loggedInUserID);
   

    $(".otp_div").hide();
    $(".otp_verify").hide();
    document.getElementById("btnRegister").disabled = true;
     $("#btnRegister").addClass("not-allowed");
    var username = $("#userName").val();
    var password = $("#password").val();
    var mobile = $("#mobile").val();

    $("#btnRegister").click(function () {
        // if (
        //     isValidForm(
        //         "registerFormID",
        //         validatorObjects["validatedForm"]
        //     )
        // ) {
        //     console.log(response);
        //     sessionStorage.setItem("sKey", response.key);
        // }
        $(".otp_div").show();
        /*$("#otp").blur(function () {
            validateField($(this), validatorObjects["validatedForm"]);
        });*/
        $(".otp_verify").show();
        $("#btnRegister").hide();
        var username = $("#userName").val();
        var password = $("#password").val();
        var mobile = $("#mobile").val();
        var confirm_password = $("#confirm_password").val();
        var roleId = 1;
        if (username != "" && password != "" && mobile != "" && confirm_password != "" ) {
            document.getElementById("btnRegister").disabled = false;
            $("#btnRegister").removeClass("not-allowed");
        var reqData = {
            userName: username,
            password: password,
            mobile: mobile,
            roleId: roleId,
        };
       
        $.ajax({
            url: `${baseURL}/api/otp`,
            method: "POST",
            data: reqData,
            type: "json",
            success: function (response, textStatus, jqXHR) {
                console.log("response==>" + JSON.stringify(response));
                if (response != null) {
                    if (jqXHR.status == 200) {
                        //window.location.href = `${baseURL}`;
                        if (
                            isValidForm(
                                "registerFormID",
                                validatorObjects["validatedForm"]
                            )
                        ) {
                            console.log(response);
                            sessionStorage.setItem("sKey", response.key);
                        }
                    }
                }
            },
            error: function (xhr, textStatus, err) {
                console.log("xhr: ", xhr);
                console.log("Error while fetching the preview data: ", err);
                if (xhr.status == 404) {
                    Swal.fire({
                        title: "Error!",
                        text: "Please check API URL!",
                        icon: "error",
                        confirmButtonText: "OK",
                    });
                } else if (xhr.status == 500) {
                    Swal.fire({
                        title: "Error!",
                        text: "Something Went Wrong Exception Occured!",
                        icon: "error",
                        confirmButtonText: "OK",
                    });
                }
            },
        });
    } else {
         
        document.getElementById("btnRegister").disabled = true;
        $("#btnRegister").addClass("not-allowed");
        

    }
    });

    $("#verifyOTP").click(function () {
        if ($("#otp").val() === "" || $("#otp").val() === undefined) {
            Swal.fire({
                title: "Unable to proceed!",
                text: "Please enter OTP",
                icon: "error",
                confirmButtonText: "OK",
            });
        } else {
            var username = $("#userName").val();
            var password = $("#password").val();
            var mobile = $("#mobile").val();
            var confirm_password = $("#confirm_password").val();
            var roleId = 1;
            let sKey = sessionStorage.getItem("sKey");
            let otp = $("#otp").val();
       
            var reqData = {
                userName: username,
                password: password,
                mobile: mobile,
                roleId: roleId,
            };
            if (otp === sKey) {
                $.ajax({
                    url: `${baseURL}/api/register`,
                    method: "POST",
                    data: reqData,
                    type: "json",
                    success: function (response, textStatus, jqXHR) {
                        sessionStorage.removeItem("sKey");
                        console.log("response==>" + JSON.stringify(response));
                        if (response != null) {
                            if (jqXHR.status == 200) {
                                Swal.fire({
                                    title: "Success!",
                                    text: "Successfully Registered!",
                                    icon: "success",
                                    confirmButtonText: "OK",
                                }).then((result) => {
                                    if (result.isConfirmed) {
                                        window.location.href = `${baseURL}`;
                                    }
                                });

                                console.log(response);
                            }
                        }
                    },
                    error: function (xhr, textStatus, err) {
                        sessionStorage.removeItem("sKey");
                        console.log("xhr: ", xhr);
                        console.log(
                            "Error while fetching the preview data: ",
                            err
                        );
                        if (xhr.status == 404) {
                            Swal.fire({
                                title: "Error!",
                                text: "Please check API URL!",
                                icon: "error",
                                confirmButtonText: "OK",
                            });
                        } else if (xhr.status == 500) {
                            Swal.fire({
                                title: "Error!",
                                text: "Something Went Wrong Exception Occured!",
                                icon: "error",
                                confirmButtonText: "OK",
                            });
                        }
                    },
                });
            } else {
                sessionStorage.removeItem("sKey");
                Swal.fire({
                    title: "Error!",
                    text: "OTP Verification Failed!",
                    icon: "error",
                    confirmButtonText: "OK",
                });
            }
        }
    });

    $("#userName").keyup(function () {

        var username = $("#userName").val();
        const userNamePattern = /^[A-Za-z]+.*$/;
        this.userNameValidationMsg = "";
        if (username.length == 0) {
            //this.userNameValidationMsg = "UserName is required !";
            $("#username-validation").html("UserName is required");
            $("#btnRegister").addClass("not-allowed");
            document.getElementById("btnRegister").disabled = true;
            this.isUserNameValid = false;
        } else {
            if (userNamePattern.test(username)) {
                $("#username-validation").html("");
                this.isUserNameValid = true;
            } else {
                this.isUserNameValid = false;
                //this.userNameValidationMsg = "UserName should start with Alphabets !";
                $("#username-validation").html(
                    "UserName should start with Alphabets !"
                );
                $("#btnRegister").addClass("not-allowed");
                document.getElementById("btnRegister").disabled = true;
            }
        }
    });

    /*$("#password").keyup(function () {
        const passwordPattern = /^(?=.*[A-Z])(?=.*[a-z])(?=.*\d)(?=.*[^A-Za-z0-9]).{8,}$/;
        var confirmPassword = $("#confirmpass").val();
        var password = $("#password").val();

        if (password.length == 0) {
            this.passwordValidationMsg = "Password is required !";
            $("#password-validation").html("Password is required !");
        } else {
            if (passwordPattern.test(password)) {
                this.isPasswordValid = true;
            } else {
                $("#password-validation").html("Password is not valid !");
            }
        }
        if (confirmPassword != null && confirmPassword.length > 0) {
            this.validateConfirmPassword();
        }
    });*/
    $("#mobile").change(function (event) {
        let val = event.target.value;
        console.log("val==>", val);
        let mobile = $("#mobile").val();
        var username = $("#userName").val();
        var password = $("#password").val();
        if (username != "" && password != "" && mobile != null) {
            $("#btnRegister").removeClass("not-allowed");
            document.getElementById("btnRegister").disabled = false;
        }
        const mobilePattern = /^[0-9]+$/;
        if (!mobilePattern.test(val)) {
            mobile = mobile.substring(0, mobile.length - 1);
            event.target.value = mobile;
        }

        if (mobile.length == 0) {
            $("#mobile-validation").html("Mobile number is required !");
            $("#btnRegister").addClass("not-allowed");
            document.getElementById("btnRegister").disabled = true;
        } else {
            if (mobilePattern.test(mobile)) {
                $("#mobile-validation").html("");
                return true;
            } else {
                $("#mobile-validation").html("Mobile number is required !");
                $("#btnRegister").addClass("not-allowed");
                document.getElementById("btnRegister").disabled = true;
                return false;
            }
        }
    });

    $("form").each(function () {
        let inputs = $(this).find(":input");
        let formName = $(this).attr("name");
        Object.keys(inputs).forEach((key) => {
            let el = inputs[key];
            if (!(el.id instanceof HTMLElement)) {
                if (el.id !== "" && el.id !== undefined) {
                    $(`#${el.id}`).blur(function () {
                        validateField($(this), validatorObjects[formName]);
                    });
                }
            }
        });
    });
});

/* validateMobile(event) : void {
        let val = event.target.value;
        const mobilePattern = /^[0-9]+$/;
        if(!mobilePattern.test(val)){
          this.mobile = this.mobile.substring(0,(this.mobile.length-1));
          event.target.value = this.mobile;
        } 
        this.mobileValidationMsg = '';
        if(this.mobile.length == 0){
          this.mobileValidationMsg = "Mobile number is required !";
          this.isMobileValid = false;
        }
        else{
          if(mobilePattern.test(this.mobile)){
            this.isMobileValid = true;
          }else{
            this.isMobileValid = false;
            this.mobileValidationMsg = 'Mobile number is not valid !';
          }
        }
      }*/

function validateConfirmPassword() {
    var password = $("#password").val();
    var confirmPassword = $("#confirmPassword").val();
    ///this.confirmPasswordValidationMsg = '';
    if (password == confirmPassword) {
        $("#confirmPass-validation").html("");
        document.getElementById("btnRegister").disabled = flase;
        $("#btnRegister").removeClass("not-allowed");
        return true;
    } else {
        $("#confirmPass-validation").html("Password does not match !");
        document.getElementById("btnRegister").disabled = true;
        $("#btnRegister").addClass("not-allowed");
        return false;
    }
}
