
$(document).ready(function () {
	// for active label
	 $(".form-group .form-control").blur(function() {
        if ($(this).val() != "") {
            $(this).siblings(".form-label").addClass("active");
        } else {
            $(this).siblings(".form-label").removeClass("active");
        }
    });
    var loggedInUserID = localStorage.getItem("loggedInUserId");
    // console.log("UserID==>" + loggedInUserID);
    $("#username").keyup(function () {
        console.log("keyup");
        const userNamePattern = /^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+$/;
        var username = $("#username").val();
        if (username.length == 0) {
            $("#username-validation").html(Email_is_required);
        } else {
            if (userNamePattern.test(username)) {
                $("#username-validation").html("");
            } else {
                $("#username-validation").html(Email_contain);
            }
        }
    });
    $("#btnForgotPass").click(function () {
        var username = $("#username").val();
        if (username != "") {
            var reqData = {
                username: username,
               
            };
            $.ajax({
                
                  url: `${baseURL}'/api/sendResetLink`,
             //  url: `http://localhost:8000/api/sendResetLink`,
                method: "POST",
                data: reqData,
                type: "json",
                success: function (response, textStatus, jqXHR) {
                    console.log("token==>" + JSON.stringify(response));
                    //alert("success");
                    if (response != null) {
                        if (jqXHR.status == 200) {
                            Swal.fire({
                                title: Success,
                                text: Password_link_sent_successfully,
                                icon: "success",
                                confirmButtonText: Ok,
                            });
                            window.location.href = `${baseURL}`;
                        }
                    }
                },
                error: function (xhr, textStatus, err) {
                    // alert("err");
                    console.log("xhr: ", xhr);
                    console.log("Error while fetching the preview data: ", err);
                    if (xhr.status == 400) {
                        Swal.fire({
                            title: Error,
                            text: Email_invalid,
                            icon: "error",
                            confirmButtonText: Ok,
                        });
                    } else if (xhr.status == 404) {
                        Swal.fire({
                            title: Error,
                            text: pls_chk_api_url,
                            icon: "error",
                            confirmButtonText: Ok,
                        });
                    } else if (xhr.status == 500) {
                        Swal.fire({
                            title: Error,
                            text: Something_Went_Wrong_Exception_Occured,
                            icon: "error",
                            confirmButtonText: Ok,
                        });
                    }
                },
            });
        } else {
            //alert("please eneter details!");
            $("#username-validation").html(Email_is_required);
            //$("#password-validation").html("Password is required !");
        }
    });
});
