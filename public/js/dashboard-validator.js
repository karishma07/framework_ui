const validatorObjects = {
    employeeForm: {
        name: {
            required: true,
            fieldText: "Employee Name",
        },
        designation: {
            required: true,
            fieldText: "Designation",
        },
        experience: {
            required: true,
            fieldText: "Experience",
        },
        contact: {
            required: true,
            fieldText: "Contact",
        },
    },
    addProjectForm: {
        project_code: {
            required: true,
            fieldText: "Project Code",
        },
        project_name: {
            required: true,
            fieldText: "Project Name",
        },
        technology: {
            required: true,
            fieldText: "Technology",
        },
        duration: {
            required: true,
            fieldText: "Duration",
            isNumber: true,
        },
        status: {
            required: true,
            fieldText: "Status",
        },
    },
    performanceDetailsForm: {
        year: {
            required: true,
            fieldText: "Year",
        },
        grade: {
            required: true,
            fieldText: "Grade",
        },
        comments: {
            required: true,
            fieldText: "Comments",
        },
    },
};
