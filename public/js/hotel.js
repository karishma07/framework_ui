
$(document).ready(function() {
    $("#HotelName").keyup(function () {
        var username = $("#HotelName").val();
        const userNamePattern = /^[A-Za-z]+.*$/;
        this.userNameValidationMsg = "";
        if (username.length == 0) {
            //this.userNameValidationMsg = "UserName is required !";
            $("#HotelName-validation").html("Name is not valid !");
            this.isUserNameValid = false;
        } else {
            if (userNamePattern.test(username)) {
                $("#HotelName-validation").html("");
                this.isUserNameValid = true;
            } else {
                this.isUserNameValid = false;
                //this.userNameValidationMsg = "UserName should start with Alphabets !";
                $("HotelName-validation").html("UserName should start with Alphabets !");
            }
        }
    });

    $("#HotelCategory").keyup(function () {
        var username = $("#HotelCategory").val();
        const userNamePattern = /^[A-Za-z]+.*$/;
        this.userNameValidationMsg = "";
        if (username.length == 0) {
            //this.userNameValidationMsg = "UserName is required !";
            $("#HotelCategory-validation").html("category is not valid !");
            this.isUserNameValid = false;
        } else {
            if (userNamePattern.test(username)) {
                $("#HotelCategory-validation").html("");
                this.isUserNameValid = true;
            } else {
                this.isUserNameValid = false;
                //this.userNameValidationMsg = "UserName should start with Alphabets !";
                $("HotelCategory-validation").html("UserName should start with Alphabets !");
            }
        }
    });

    $("#place").keyup(function () {
        var username = $("#place").val();
        const userNamePattern = /^[A-Za-z]+.*$/;
        this.userNameValidationMsg = "";
        if (username.length == 0) {
            //this.userNameValidationMsg = "UserName is required !";
            $("#place-validation").html("Place is not valid !");
            this.isUserNameValid = false;
        } else {
            if (userNamePattern.test(username)) {
                $("#place-validation").html("");
                this.isUserNameValid = true;
            } else {
                this.isUserNameValid = false;
                //this.userNameValidationMsg = "UserName should start with Alphabets !";
                $("place-validation").html("UserName should start with Alphabets !");
            }
        }
    });

    $("#HotelContact").keyup(function (event) {
        let val = event.target.value;
        console.log("val==>", val);
        let contact = $("#HotelContact").val();
        const contactPattern = /^[0-9]+$/;
        if (!contactPattern.test(val)) {
            contact = contact.substring(0, contact.length - 1);
            event.target.value = contact;
        }
    
        if (contact.length == 0) {
            $("#HotelContact-validation").html("Contact is not valid!");
        } else {
            if (contactPattern.test(contact)) {
                $("#HotelContact-validation").html("");
                return true;
            } else {tact
    
                $("#HotelContact-validation").html("Contact is not valid!");
                return false;
            }
        }
    });



       //for edit row functionality

       $("#HotelNameEdit").keyup(function () {
        var username = $("#HotelNameEdit").val();
        const userNamePattern = /^[A-Za-z]+.*$/;
        this.userNameValidationMsg = "";
        if (username.length == 0) {
            //this.userNameValidationMsg = "UserName is required !";
            $("#HotelNameEdit-validation").html("Name is not valid !");
            this.isUserNameValid = false;
        } else {
            if (userNamePattern.test(username)) {
                $("#HotelNameEdit-validation").html("");
                this.isUserNameValid = true;
            } else {
                this.isUserNameValid = false;
                //this.userNameValidationMsg = "UserName should start with Alphabets !";
                $("HotelName-validation").html("UserName should start with Alphabets !");
            }
        }
    });

    $("#HotelCategoryEdit").keyup(function () {
        var username = $("#HotelCategoryEdit").val();
        const userNamePattern = /^[A-Za-z]+.*$/;
        this.userNameValidationMsg = "";
        if (username.length == 0) {
            //this.userNameValidationMsg = "UserName is required !";
            $("#HotelCategoryEdit-validation").html("category is not valid !");
            this.isUserNameValid = false;
        } else {
            if (userNamePattern.test(username)) {
                $("#HotelCategoryEdit-validation").html("");
                this.isUserNameValid = true;
            } else {
                this.isUserNameValid = false;
                //this.userNameValidationMsg = "UserName should start with Alphabets !";
                $("HotelCategoryEdit-validation").html("UserName should start with Alphabets !");
            }
        }
    });

    $("#placeEdit").keyup(function () {
        var username = $("#place").val();
        const userNamePattern = /^[A-Za-z]+.*$/;
        this.userNameValidationMsg = "";
        if (username.length == 0) {
            //this.userNameValidationMsg = "UserName is required !";
            $("#placeEdit-validation").html("Place is not valid !");
            this.isUserNameValid = false;
        } else {
            if (userNamePattern.test(username)) {
                $("#placeEdit-validation").html("");
                this.isUserNameValid = true;
            } else {
                this.isUserNameValid = false;
                //this.userNameValidationMsg = "UserName should start with Alphabets !";
                $("placeEdit-validation").html("UserName should start with Alphabets !");
            }
        }
    });

    $("#HotelContactEdit").keyup(function (event) {
        let val = event.target.value;
        console.log("val==>", val);
        let contact = $("#HotelContactEdit").val();
        const contactPattern = /^[0-9]+$/;
        if (!contactPattern.test(val)) {
            contact = contact.substring(0, contact.length - 1);
            event.target.value = contact;
        }
    
        if (contact.length == 0) {
            $("#HotelContactEdit-validation").html("Contact is not valid!");
        } else {
            if (contactPattern.test(contact)) {
                $("#HotelContactEdit-validation").html("");
                return true;
            } else {tact
    
                $("#HotelContactEdit-validation").html("Contact is not valid!");
                return false;
            }
        }
    });
       $(".editEmp").click(function () {
        $('#empModal').modal('show');
    });


    $("#saveCloseBtn").click(function () {
        var username = $("#HotelName").val();
        const userNamePattern = /^[A-Za-z]+.*$/;
        this.userNameValidationMsg = "";
        if (username.length == 0) {
            //this.userNameValidationMsg = "UserName is required !";
            $("#HotelName-validation").html("Name is not valid !");
            this.isUserNameValid = false;
        } else {
            if (userNamePattern.test(username)) {
                $("#HotelName-validation").html("");
                this.isUserNameValid = true;
            } else {
                this.isUserNameValid = false;
                //this.userNameValidationMsg = "UserName should start with Alphabets !";
                $("HotelName-validation").html("UserName should start with Alphabets !");
            }
        }
        let val = event.target.value;
        console.log("val==>", val);
        let contact = $("#HotelContact").val();
        const contactPattern = /^[0-9]+$/;
        if (!contactPattern.test(val)) {
            contact = contact.substring(0, contact.length - 1);
            event.target.value = contact;
        }
    
        if (contact.length == 0) {
            $("#HotelContact-validation").html("Contact is not valid!");
        } else {
            if (contactPattern.test(contact)) {
                $("#HotelContact-validation").html("");
                return true;
            } else {tact
    
                $("#HotelContact-validation").html("Contact is not valid!");
                return false;
            }
        }
    
        var username1 = $("#HotelCategory").val();
        const userNamePattern1 = /^[A-Za-z]+.*$/;
        this.userNameValidationMsg = "";
        if (username1.length == 0) {
            //this.userNameValidationMsg = "UserName is required !";
            $("#HotelCategory-validation").html("category is not valid !");
            this.isUserNameValid = false;
        } else {
            if (userNamePattern1.test(username1)) {
                $("#HotelCategory-validation").html("");
                this.isUserNameValid = true;
            } else {
                this.isUserNameValid = false;
                //this.userNameValidationMsg = "UserName should start with Alphabets !";
                $("HotelCategory-validation").html("UserName should start with Alphabets !");
            }
        }

        var place = $("#place").val();
        const placePattern = /^[A-Za-z]+.*$/;
        this.userNameValidationMsg = "";
        if (place.length == 0) {
            //this.userNameValidationMsg = "UserName is required !";
            $("#place-validation").html("Place is not valid !");
            this.isUserNameValid = false;
        } else {
            if (placePattern.test(place)) {
                $("#place-validation").html("");
                this.isUserNameValid = true;
            } else {
                this.isUserNameValid = false;
                //this.userNameValidationMsg = "UserName should start with Alphabets !";
                $("place-validation").html("UserName should start with Alphabets !");
            }
        }

    });
    $("#saveNewBtn").click(function () {
        var username = $("#HotelName").val();
        const userNamePattern = /^[A-Za-z]+.*$/;
        this.userNameValidationMsg = "";
        if (username.length == 0) {
            //this.userNameValidationMsg = "UserName is required !";
            $("#HotelName-validation").html("Name is not valid !");
            this.isUserNameValid = false;
        } else {
            if (userNamePattern.test(username)) {
                $("#HotelName-validation").html("");
                this.isUserNameValid = true;
            } else {
                this.isUserNameValid = false;
                //this.userNameValidationMsg = "UserName should start with Alphabets !";
                $("HotelName-validation").html("UserName should start with Alphabets !");
            }
        }
        let val = event.target.value;
        console.log("val==>", val);
        let contact = $("#HotelContact").val();
        const contactPattern = /^[0-9]+$/;
        if (!contactPattern.test(val)) {
            contact = contact.substring(0, contact.length - 1);
            event.target.value = contact;
        }
    
        if (contact.length == 0) {
            $("#HotelContact-validation").html("Contact is not valid!");
        } else {
            if (contactPattern.test(contact)) {
                $("#HotelContact-validation").html("");
                return true;
            } else {tact
    
                $("#HotelContact-validation").html("Contact is not valid!");
                return false;
            }
        }
    
        var username1 = $("#HotelCategory").val();
        const userNamePattern1 = /^[A-Za-z]+.*$/;
        this.userNameValidationMsg = "";
        if (username1.length == 0) {
            //this.userNameValidationMsg = "UserName is required !";
            $("#HotelCategory-validation").html("category is not valid !");
            this.isUserNameValid = false;
        } else {
            if (userNamePattern1.test(username1)) {
                $("#HotelCategory-validation").html("");
                this.isUserNameValid = true;
            } else {
                this.isUserNameValid = false;
                //this.userNameValidationMsg = "UserName should start with Alphabets !";
                $("HotelCategory-validation").html("UserName should start with Alphabets !");
            }
        }

        var place = $("#place").val();
        const placePattern = /^[A-Za-z]+.*$/;
        this.userNameValidationMsg = "";
        if (place.length == 0) {
            //this.userNameValidationMsg = "UserName is required !";
            $("#place-validation").html("Place is not valid !");
            this.isUserNameValid = false;
        } else {
            if (placePattern.test(place)) {
                $("#place-validation").html("");
                this.isUserNameValid = true;
            } else {
                this.isUserNameValid = false;
                //this.userNameValidationMsg = "UserName should start with Alphabets !";
                $("place-validation").html("UserName should start with Alphabets !");
            }
        }

    });


    //edit

    $("#saveCloseBtnEdit").click(function () {
        var username = $("#HotelNameEdit").val();
        const userNamePattern = /^[A-Za-z]+.*$/;
        this.userNameValidationMsg = "";
        if (username.length == 0) {
            //this.userNameValidationMsg = "UserName is required !";
            $("#HotelNameEdit-validation").html("Name is not valid !");
            this.isUserNameValid = false;
        } else {
            if (userNamePattern.test(username)) {
                $("#HotelNameEdit-validation").html("");
                this.isUserNameValid = true;
            } else {
                this.isUserNameValid = false;
                //this.userNameValidationMsg = "UserName should start with Alphabets !";
                $("HotelNameEdit-validation").html("UserName should start with Alphabets !");
            }
        }
        let val = event.target.value;
        console.log("val==>", val);
        let contact = $("#HotelContactEdit").val();
        const contactPattern = /^[0-9]+$/;
        if (!contactPattern.test(val)) {
            contact = contact.substring(0, contact.length - 1);
            event.target.value = contact;
        }
    
        if (contact.length == 0) {
            $("#HotelContactEdit-validation").html("Contact is not valid!");
        } else {
            if (contactPattern.test(contact)) {
                $("#HotelContactEdit-validation").html("");
                return true;
            } else {tact
    
                $("#HotelContactEdit-validation").html("Contact is not valid!");
                return false;
            }
        }
    
        var username1 = $("#HotelCategoryEdit").val();
        const userNamePattern1 = /^[A-Za-z]+.*$/;
        this.userNameValidationMsg = "";
        if (username1.length == 0) {
            //this.userNameValidationMsg = "UserName is required !";
            $("#HotelCategoryEdit-validation").html("category is not valid !");
            this.isUserNameValid = false;
        } else {
            if (userNamePattern1.test(username1)) {
                $("#HotelCategoryEdit-validation").html("");
                this.isUserNameValid = true;
            } else {
                this.isUserNameValid = false;
                //this.userNameValidationMsg = "UserName should start with Alphabets !";
                $("HotelCategoryEdit-validation").html("UserName should start with Alphabets !");
            }
        }

        var place = $("#placeEdit").val();
        const placePattern = /^[A-Za-z]+.*$/;
        this.userNameValidationMsg = "";
        if (place.length == 0) {
            //this.userNameValidationMsg = "UserName is required !";
            $("#placeEdit-validation").html("Place is not valid !");
            this.isUserNameValid = false;
        } else {
            if (placePattern.test(place)) {
                $("#placeEdit-validation").html("");
                this.isUserNameValid = true;
            } else {
                this.isUserNameValid = false;
                //this.userNameValidationMsg = "UserName should start with Alphabets !";
                $("placeEdit-validation").html("UserName should start with Alphabets !");
            }
        }

    });
    $("#saveNewBtnEdit").click(function () {
        var username = $("#HotelNameEdit").val();
        const userNamePattern = /^[A-Za-z]+.*$/;
        this.userNameValidationMsg = "";
        if (username.length == 0) {
            //this.userNameValidationMsg = "UserName is required !";
            $("#HotelNameEdit-validation").html("Name is not valid !");
            this.isUserNameValid = false;
        } else {
            if (userNamePattern.test(username)) {
                $("#HotelNameEdit-validation").html("");
                this.isUserNameValid = true;
            } else {
                this.isUserNameValid = false;
                //this.userNameValidationMsg = "UserName should start with Alphabets !";
                $("HotelNameEdit-validation").html("UserName should start with Alphabets !");
            }
        }
        let val = event.target.value;
        console.log("val==>", val);
        let contact = $("#HotelContactEdit").val();
        const contactPattern = /^[0-9]+$/;
        if (!contactPattern.test(val)) {
            contact = contact.substring(0, contact.length - 1);
            event.target.value = contact;
        }
    
        if (contact.length == 0) {
            $("#HotelContactEdit-validation").html("Contact is not valid!");
        } else {
            if (contactPattern.test(contact)) {
                $("#HotelContactEdit-validation").html("");
                return true;
            } else {tact
    
                $("#HotelContactEdit-validation").html("Contact is not valid!");
                return false;
            }
        }
    
        var username1 = $("#HotelCategoryEdit").val();
        const userNamePattern1 = /^[A-Za-z]+.*$/;
        this.userNameValidationMsg = "";
        if (username1.length == 0) {
            //this.userNameValidationMsg = "UserName is required !";
            $("#HotelCategoryEdit-validation").html("category is not valid !");
            this.isUserNameValid = false;
        } else {
            if (userNamePattern1.test(username1)) {
                $("#HotelCategoryEdit-validation").html("");
                this.isUserNameValid = true;
            } else {
                this.isUserNameValid = false;
                //this.userNameValidationMsg = "UserName should start with Alphabets !";
                $("HotelCategoryEdit-validation").html("UserName should start with Alphabets !");
            }
        }

        var place = $("#placeEdit").val();
        const placePattern = /^[A-Za-z]+.*$/;
        this.userNameValidationMsg = "";
        if (place.length == 0) {
            //this.userNameValidationMsg = "UserName is required !";
            $("#placeEdit-validation").html("Place is not valid !");
            this.isUserNameValid = false;
        } else {
            if (placePattern.test(place)) {
                $("#placeEdit-validation").html("");
                this.isUserNameValid = true;
            } else {
                this.isUserNameValid = false;
                //this.userNameValidationMsg = "UserName should start with Alphabets !";
                $("placeEdit-validation").html("UserName should start with Alphabets !");
            }
        }

    });


//for close modal
    //for edit row functionality
    $(".cancleEmpModal").click(function () {
        $('#editProject').modal('hide');
    });
    $(".cancleEditPer").click(function () {
        $('#editPer').modal('hide');
    });
    
    $(".cancleEditPer").click(function () {
        $('#editProject').modal('hide');
    });
    $(".cancleBtnproject").click(function () {
        $('#exampleModal2').modal('hide');
    });
    $(".cancleBtnMain").click(function () {
        $('#exampleModal').modal('hide');
    });
    $(".cancleEditMain").click(function () {
        $('#empModal').modal('hide');
    });
    $(".BtnCanclePer").click(function () {
        $('#exampleModal3').modal('hide');
    });
    
    $("#exportTable").click(function(e) {
        var a = document.createElement('a');
        var data_type = 'data:application/vnd.ms-excel;charset=utf-8';
        var table_html = $('#table')[0].outerHTML;
        table_html = table_html.replace(/<tfoot[\s\S.]*tfoot>/gmi, '');
        var css_html =
            '<style>td {border: 0.5pt solid #c0c0c0} .tRight { text-align:right} .tLeft { text-align:left} </style>';
        a.href = data_type + ',' + encodeURIComponent('<html><head>' + css_html + '</' + 'head><body>' +
            table_html + '</body></html>');
        a.download = 'HotelDetails_table.xls';
        a.click();
        e.preventDefault();
    });

     // display selected row data in text input
                
     var table = document.getElementById("table"),rIndex;
                
     for(var i = 1; i < table.rows.length; i++)
     {
         table.rows[i].onclick = function()
         {
             rIndex = this.rowIndex;
             console.log(rIndex);
             
             document.getElementById("HotelNameEdit").value = this.cells[1].innerHTML;
             document.getElementById("HotelContactEdit").value = this.cells[2].innerHTML;
             document.getElementById("HotelCategoryEdit").value = this.cells[3].innerHTML;
             document.getElementById("placeEdit").value = this.cells[4].innerHTML;
         };
     }
});
