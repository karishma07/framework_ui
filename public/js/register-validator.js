const validatorObjects = {
    validatedForm: {
        username: {
            required: true,
            fieldText: "Username",
        },
        password: {
            required: true,
            fieldText: "Password",
        },
        confirm_password: {
            required: true,
            fieldText: " Confirm Password",
        },
        mobile: {
            required: true,
            fieldText: "Mobile",
        },
        otp: {
            required: true,
            fieldText: "OTP",
        },
    },
};
