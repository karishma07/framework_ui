<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\ForgetpasswordController;
use App\Http\Controllers\ResetPasswordController;
use App\Http\Controllers\EmployeeController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//localization routs
Route::get('locale/{locale}', function($locale){
    Session()->put('locale',$locale);
    return redirect()->back();
    });
    
/*Route::get('/', function () {
    return view('welcome');
});*/
//forget password  functinality
Route::get('/forgetPassword', [ForgetpasswordController::class, 'index']);

//reset password  functinality
Route::get('/resetPassword/{id}', [ResetPasswordController::class, 'index']);


Route::get('/signup', [LoginController::class, 'signup']);

Route::get('/', [LoginController::class, 'index']);
//Route::get('/login', [LoginController::class, 'login']);
Route::get('/logout', [LoginController::class, 'logout']);

//for dashboard view
Route::get('/dashboard', [DashboardController::class, 'dashboard']);
Route::get('/delete/employee/{empId}', [DashboardController::class, 'deleteEmployee']);

//for employee 
Route::get('/employee', [EmployeeController::class, 'index']);
Route::get('/edit/{id}', [EmployeeController::class, 'EditEmployee']);

//import functionality

Route::get('/import', [EmployeeController::class, 'import']);
Route::post('/uploadFile', [EmployeeController::class, 'uploadFile']);


//upload functionality

Route::get('/upload', [EmployeeController::class, 'upload']);

//upload functionality

Route::get('/upload-document', [EmployeeController::class, 'uploadDocument']);

//upload functionality

Route::get('/upload-image', [EmployeeController::class, 'uploadImage']);




Route::get('/pass', [DashboardController::class, 'pass']);